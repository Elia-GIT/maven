package bpij.main.java.csv;

import bpij.main.java.model.InputData;
import bpij.main.java.utils.Constants;
import bpij.main.java.utils.UtilsAPI;
import bpij.main.java.utils.UtilsRapportRun;
import org.apache.log4j.Logger;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class CsvReaderWriter {
    private static final Logger LOGGER = Logger.getLogger(CsvReaderWriter.class);

    public List<InputData> readInputDataList(String csvPath) {
        if (Constants.IS_COMMAND_LINE_VERSION && Constants.IS_API_VERSION) {
            return UtilsAPI.getLoginsList(LOGGER);
        } else {
            List<InputData> inputDataList = new ArrayList<>();
            File file = new File(csvPath);

            try (FileReader reader = new FileReader(file)) {
                char[] chars = new char[(int) file.length()];
                reader.read(chars);

                String[] lines = new String(chars).split("\\r?\\n");
                for (int k = 0; k < lines.length; k++) {
                    String currLine = lines[k];

                    if (k == 0) {
                        inputDataList.add(new InputData(currLine));
                    } else {
                        String[] cells = currLine.split(";");
                        String siret = cells[0];
                        String nom = "";
                        String prenom = "";
                        String password = "";

                        if (cells.length >= 4) {
                            nom = cells[1];
                            prenom = cells[2];
                            password = cells[3];
                        }

                        String companyName = null;
                        if (cells.length > 4) {
                            companyName = cells[4];
                        }

                        String employeeCustomName = null;

                        if (cells.length > 5) {
                            employeeCustomName = cells[5];
                        }
                        if (cells.length > 6) {
                            employeeCustomName = employeeCustomName + ";" + cells[6];
                        }
                        if (cells.length > 7) {
                            employeeCustomName = employeeCustomName + ";" + cells[7];
                        }

                        LOGGER.info("INPUTDATALIST size = " + inputDataList.size());
                        inputDataList.add(new InputData(siret, nom, prenom, password, currLine, companyName, employeeCustomName));
                    }

                }
            } catch (Exception e) {
                LOGGER.error("ERROR - readInputDataList. path: " + csvPath, e);
            }

            return inputDataList;
        }
    }

    public void writeCSV(InputData inputData, String filename) {
        try (PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(filename, true)))) {
            String line = inputData.getCompanyID() + ";" + inputData.getLastname()
                    + ";" + inputData.getFirstname() + ";" + inputData.getPassword()
                    + ";" + inputData.getCompanyCustomName()
                    + ";" + UtilsRapportRun.getTypeCompte(inputData.getStatusLogin(), false);

            out.println(line);
        } catch (IOException e) {
            LOGGER.error("ERROR - writeCSV. filename: " + filename, e);
        }
    }

    public void writeHeaderLine(String line, String filename) {
        try (PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(filename, false)))) {
            out.println(line + ";Resulat SIRET");
        } catch (IOException e) {
            LOGGER.error("ERROR - writeCSV. filename: " + filename, e);
        }
    }
}

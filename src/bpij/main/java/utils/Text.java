package bpij.main.java.utils;

class Text {
    static String cpamList = "CPAM de l'AIN\n" +
            "CPAM de l'AISNE\n" +
            "CPAM de l'ALLIER\n" +
            "CPAM des ALPES de HAUTE-PROVENCE\n" +
            "CPAM des HAUTES-ALPES\n" +
            "CPAM des ALPES MARITIMES\n" +
            "CPAM de l'ARDECHE\n" +
            "CPAM des ARDENNES\n" +
            "CPAM de l'ARIÈGE\n" +
            "CPAM de l'AUBE\n" +
            "CPAM de l'AUDE\n" +
            "CPAM de l'AVEYRON\n" +
            "CPAM des BOUCHES du RHÔNE\n" +
            "CPAM du CALVADOS\n" +
            "CPAM du CANTAL\n" +
            "CPAM de CHARENTE\n" +
            "CPAM de CHARENTE-MARITIME\n" +
            "CPAM du CHER\n" +
            "CPAM de la CORREZE\n" +
            "MFP\n" +
            "HFP\n" +
            "ENIM\n" +
            "CPAM de la Corse du Sud\n" +
            "CPAM de la Haute-Corse\n" +
            "CPAM de la CÔTE d'OR\n" +
            "CPAM des CÔTES d'ARMOR\n" +
            "CANSSM\n" +
            "CPAM de la CREUSE\n" +
            "CPAM de DORDOGNE\n" +
            "CPAM du DOUBS\n" +
            "CPAM de la DROME\n" +
            "CPAM de l'EURE\n" +
            "CPAM de l'EURE et LOIR\n" +
            "CPAM du FINISTERE\n" +
            "CPAM du GARD\n" +
            "CPAM de HAUTE-GARONNE\n" +
            "CPAM du GERS\n" +
            "CPAM de GIRONDE\n" +
            "CPAM de l'HERAULT\n" +
            "CPAM d'ILLE et VILAINE\n" +
            "CPAM de l'INDRE\n" +
            "CPAM d'INDRE et LOIRE\n" +
            "CPAM de l'ISERE\n" +
            "CPAM du JURA\n" +
            "CPAM des LANDES\n" +
            "CPAM du LOIR et CHER\n" +
            "CPAM de la LOIRE\n" +
            "CPAM de HAUTE-LOIRE\n" +
            "CPAM de LOIRE-ATLANTIQUE\n" +
            "CPAM du LOIRET\n" +
            "MNH\n" +
            "CPAM du LOT\n" +
            "CPAM du LOT ET GARONNE\n" +
            "CCSS de LOZERE\n" +
            "CPAM de MAINE-ET-LOIRE\n" +
            "CPAM de la MANCHE\n" +
            "CPAM de la MARNE\n" +
            "LMG\n" +
            "CPAM de HAUTE-MARNE\n" +
            "CPAM de la MAYENNE\n" +
            "CPAM DE MEURTHE-ET-MOSELLE\n" +
            "CPAM de la MEUSE\n" +
            "CPAM du MORBIHAN\n" +
            "CPAM de MOSELLE\n" +
            "CPAM de la NIEVRE\n" +
            "CPAM des FLANDRES-DUNKERQUE-ARMENTIERES\n" +
            "CPAM de LILLE-DOUAI\n" +
            "CPAM de ROUBAIX-TOURCOING\n" +
            "CPAM du HAINAUT\n" +
            "CPAM de l'OISE\n" +
            "CPAM de l'ORNE\n" +
            "CPAM de la COTE d'OPALE\n" +
            "CPAM de l'ARTOIS\n" +
            "CPAM du PUY de DOME\n" +
            "CPAM de BAYONNE\n" +
            "CPAM de PAU\n" +
            "CPAM des HAUTES-PYRÉNÉES\n" +
            "CPAM des PYRÉNÉES-ORIENTALES\n" +
            "CPAM du BAS-RHIN\n" +
            "CPAM du HAUT-RHIN\n" +
            "CPAM du RHONE\n" +
            "CPAM de HAUTE-SAÔNE\n" +
            "CPAM de SAÔNE et LOIRE\n" +
            "CPAM de la SARTHE\n" +
            "CPAM de SAVOIE\n" +
            "CPAM de HAUTE-SAVOIE\n" +
            "CPAM de PARIS\n" +
            "CPAM du HAVRE\n" +
            "CPAM ROUEN-ELBEUF-DIEPPE-SEINE-MARITIME\n" +
            "CPAM de SEINE-et-MARNE\n" +
            "CPAM des YVELINES\n" +
            "CPAM des DEUX-SEVRES\n" +
            "CPAM de la SOMME\n" +
            "CPAM du TARN\n" +
            "CPAM de TARN et GARONNE\n" +
            "CPAM du VAR\n" +
            "CPAM du VAUCLUSE\n" +
            "CPAM de VENDÉE\n" +
            "CPAM de la VIENNE\n" +
            "CPAM de la HAUTE-VIENNE\n" +
            "CPAM des VOSGES\n" +
            "CPAM de l'YONNE\n" +
            "CPAM du TERRITOIRE de BELFORT\n" +
            "CPAM de l'ESSONNE\n" +
            "CPAM des Hauts-de-Seine\n" +
            "CPAM de la SEINE-SAINT-DENIS\n" +
            "CPAM du VAL de MARNE\n" +
            "CPAM du VAL d'OISE\n" +
            "Caisse Générale de Sécurité Sociale de la GUADELOUPE\n" +
            "Caisse Générale de Sécurité Sociale de la MARTINIQUE\n" +
            "Caisse Générale de Sécurité Sociale de GUYANE\n" +
            "Caisse Générale de Sécurité Sociale de REUNION\n" +
            "Caisse de Sécurité Sociale de MAYOTTE\n" +
            "CRPCEN\n";

    static String cpamCode = "11\n" +
            "21\n" +
            "31\n" +
            "41\n" +
            "51\n" +
            "61\n" +
            "72\n" +
            "81\n" +
            "91\n" +
            "101\n" +
            "111\n" +
            "121\n" +
            "131\n" +
            "141\n" +
            "151\n" +
            "161\n" +
            "171\n" +
            "181\n" +
            "191\n" +
            "194\n" +
            "196\n" +
            "200\n" +
            "201\n" +
            "202\n" +
            "211\n" +
            "221\n" +
            "225\n" +
            "231\n" +
            "241\n" +
            "251\n" +
            "261\n" +
            "271\n" +
            "281\n" +
            "291\n" +
            "301\n" +
            "311\n" +
            "321\n" +
            "331\n" +
            "342\n" +
            "351\n" +
            "361\n" +
            "371\n" +
            "381\n" +
            "391\n" +
            "401\n" +
            "411\n" +
            "422\n" +
            "431\n" +
            "441\n" +
            "451\n" +
            "459\n" +
            "461\n" +
            "471\n" +
            "481\n" +
            "491\n" +
            "501\n" +
            "511\n" +
            "512\n" +
            "521\n" +
            "531\n" +
            "542\n" +
            "551\n" +
            "561\n" +
            "571\n" +
            "581\n" +
            "594\n" +
            "595\n" +
            "597\n" +
            "599\n" +
            "601\n" +
            "611\n" +
            "623\n" +
            "624\n" +
            "631\n" +
            "641\n" +
            "642\n" +
            "651\n" +
            "661\n" +
            "673\n" +
            "682\n" +
            "691\n" +
            "701\n" +
            "711\n" +
            "721\n" +
            "731\n" +
            "741\n" +
            "751\n" +
            "763\n" +
            "764\n" +
            "771\n" +
            "781\n" +
            "791\n" +
            "801\n" +
            "811\n" +
            "821\n" +
            "831\n" +
            "841\n" +
            "851\n" +
            "861\n" +
            "871\n" +
            "881\n" +
            "891\n" +
            "901\n" +
            "911\n" +
            "921\n" +
            "931\n" +
            "941\n" +
            "951\n" +
            "971\n" +
            "972\n" +
            "973\n" +
            "974\n" +
            "976\n" +
            "980\n";
}

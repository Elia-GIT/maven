package bpij.main.java.utils;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class UtilsLicenseLinkedIn {
    public static String getId(org.apache.log4j.Logger logger) {
        try (BufferedReader br = new BufferedReader(new FileReader(Constants.LINKEDIN_DEMO_FILE))) {
            int count = 0;
            for (String line; (line = br.readLine()) != null; ) {
                count++;

                if (count == 1) {
                    return line;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return "";
    }
}

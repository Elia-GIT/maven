package bpij.main.java.utils;

import java.util.HashMap;
import java.util.List;

public class UtilsRapportRun {
    public static String writeRappportRun(HashMap<Integer, List<String>> h, long totalTime, long averageTime, int nbCas) {
        StringBuilder ret = new StringBuilder();
        ret.append("\n");
        ret.append("### RAPPORT EXECUTION ###\n");
        ret.append("Durée D'extraction Moyenne par Siret : ").append(averageTime).append(" sec\n\n");

        for (int i = 0; i < nbCas; i++) {
            ret.append(getTypeCompte(i, true)).append(" : ").append(h.get(i).size()).append("\n");
        }

        ret.append("### FIN DU RAPPORT EXECUTION ###\n");
        return ret.toString();
    }

    public static String getTypeCompte(int i, boolean isPluriel) {
        String compte = "Compte";
        if (isPluriel) {
            compte = compte + "s";
        }

        switch (i) {
            case 9:
                return compte + " avec blocage 5min car échecs 3 tentatives";
            case 8:
                return compte + " erreur net-entreprises nous ne pouvons répondre à votre demande";
            case 0:
                return compte + " avec BPIJ mais erreur de parcours";
            case 1:
                return compte + " sans accès à BPIJ";
            case 2:
                return compte + " avec mot de passe échoué";
            case 3:
                return compte + " sans BPIJ à télécharger";
            case 4:
                return compte + " avec accès BPIJ mais téléchargements échoués";
            case 5:
                return compte + " trop de BPIJ sur la période";
            case 6:
                return compte + " SIRET en particulier à définir pour télécharger des BPIJs";
            case 7:
                return compte + " avec accès BPIJ et téléchargements réussis";
        }

        return "";
    }
}

package bpij.main.java.utils;

import bpij.main.java.bot.NetEntreprisesBot;
import bpij.main.java.model.InputData;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.*;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

public class UtilsAPI {
    public static void setLoginStatus(String siret, String statut, Logger LOGGER) {
        final JsonObject in = new JsonObject();
        in.addProperty("siret", siret);
        in.addProperty("statusBpij", statut);

        final RequestEntity<String> entity = RequestEntity
                .post(null)
                .header("X-ApiKey", Constants.API_KEY)
                .header("X-ApiSecret", Constants.API_SECRET)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .body(in.toString());

        final JsonObject out =
                new RestTemplate().postForObject(
                        Constants.API_PREFIX +
                                Constants.API_HOST +
                                ":" + Constants.API_PORT +
                                "/api/bpij/admin/logins/status", entity, JsonObject.class);
        LOGGER.info("[" + siret + "] Réponse API setLoginStatus " + out);
    }
    public static class MyJob implements Callable<String> {

        @Override
        public String call() {
            final HttpHeaders headers = new HttpHeaders();
            headers.set("X-ApiKey", Constants.API_KEY);
            headers.set("X-ApiSecret", Constants.API_SECRET);

            final HttpEntity<Void> entity = new HttpEntity<>(headers);
            final ResponseEntity<JsonObject> out = new RestTemplate().exchange(
                    Constants.API_PREFIX + Constants.API_HOST
                            + ":" + Constants.API_PORT
                            + "/api/atmp/admin/logins",
                    HttpMethod.GET, entity, JsonObject.class);
            return out.getBody().toString();
        }
    }

    public static List<InputData> getLoginsList(Logger LOGGER) {
        Future<String> control
                = Executors.newSingleThreadExecutor().submit(new MyJob());
        String result = "";

        try {
            result = control.get(5, TimeUnit.SECONDS);
        } catch (TimeoutException ex) {
            LOGGER.info("--> timer except");
            control.cancel(true);
        } catch (InterruptedException ex) {
            LOGGER.info("réponse API getLoginsList Interr--> " + result);
            return new ArrayList<>();
        } catch (ExecutionException ex) {
            LOGGER.info("réponse API getLoginsList Exec --> Erreur clé API " + result);
            return new ArrayList<>();
        }

        JSONObject json;
        JSONArray arr = null;

        try {
            json = new JSONObject(result);
            arr = json.getJSONArray("logins");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        LOGGER.info("réponse API getLoginsList --> " + "200 OK");

        List<InputData> ret = new ArrayList<>();
        ret.add(new InputData(""));
        for (int i = 0; i < arr.length(); i++) {
            try {
                String siret = arr.getJSONObject(i).getString("siret");
                String nom = arr.getJSONObject(i).getString("nom");
                String prenom = arr.getJSONObject(i).getString("prenom");
                String motDePasse = arr.getJSONObject(i).getString("motDePasse");
                ret.add(new InputData(siret, nom, prenom, motDePasse, "", "", null));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return ret;
    }
    public static List<InputData> getLoginsList() {
        final HttpHeaders headers = new HttpHeaders();
        headers.set("X-ApiKey", Constants.API_KEY);
        headers.set("X-ApiSecret", Constants.API_SECRET);

        final HttpEntity<Void> entity = new HttpEntity<>(headers);
        final ResponseEntity<JsonObject> out = new RestTemplate().exchange(
                Constants.API_PREFIX + Constants.API_HOST
                        + ":" + Constants.API_PORT
                        + "/api/bpij/admin/logins",
                HttpMethod.GET, entity, JsonObject.class);

        JSONObject json;
        JSONArray arr = null;

        try {
            json = new JSONObject(out.getBody().toString());
            arr = json.getJSONArray("logins");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        List<InputData> ret = new ArrayList<>();
        ret.add(new InputData(""));
        for (int i = 0; i < arr.length(); i++) {
            try {
                String siret = arr.getJSONObject(i).getString("siret");
                String nom = arr.getJSONObject(i).getString("nom");
                String prenom = arr.getJSONObject(i).getString("prenom");
                String motDePasse = arr.getJSONObject(i).getString("motDePasse");
                ret.add(new InputData(siret, nom, prenom, motDePasse, "", "", null));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return ret;
    }

    public static boolean submitBPIJXMLToDB(String siret, String siretBPIJ, String idBPIJ,
                                            String xmlPath, String codeCpam, String dateBpij,
                                            double montant, Logger LOGGER) {
        final JsonArray bpijs = new JsonArray();
        final JsonObject in = new JsonObject();
        final JsonObject bpij = new JsonObject();

        bpij.addProperty("siret", siret);
        bpij.addProperty("siretBpij", siretBPIJ);
        bpij.addProperty("idBpij", idBPIJ);
        bpij.addProperty("codeCpam", codeCpam);
        bpij.addProperty("dateBpij", dateBpij);
        bpij.addProperty("montant", montant);

        try {
            bpij.addProperty("xml", UtilsMeth.readFile(xmlPath).replaceAll("[\r\n]+", ""));
        } catch (IOException e) {
            e.printStackTrace();
        }

        bpijs.add(bpij);
        in.add("bpijs", bpijs);

        final RequestEntity<String> entity = RequestEntity.post(null)
                .header("X-ApiKey", Constants.API_KEY)
                .header("X-ApiSecret", Constants.API_SECRET)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .body(in.toString());

        final JsonObject out =
                new RestTemplate().postForObject(Constants.API_PREFIX +
                        Constants.API_HOST +
                        ":" + Constants.API_PORT +
                        "/api/bpij/admin/bpijs", entity, JsonObject.class);
        LOGGER.info("[" + siret + "] Réponse API PUSH BPIJ " + out);

        return true;
    }

    public static boolean submitExtractionLogToDB(String date, String log, int siretFailureCount, int siretSuccessCount, String typeExtraction, Logger LOGGER) {
        final JsonArray logsJson = new JsonArray();
        final JsonObject in = new JsonObject();
        final JsonObject logJson = new JsonObject();

        logJson.addProperty("log", log);
        logJson.addProperty("date", UtilsMeth.getDateAsSeconds(date));
        logJson.addProperty("siretFailureCount", siretFailureCount);
        logJson.addProperty("siretSuccessCount", siretSuccessCount);
        logJson.addProperty("typeExtraction", typeExtraction);//UtilsMeth.getDateAsSeconds(dateBpij));
        logsJson.add(logJson);
        in.add("activityLogs", logsJson);

        final RequestEntity<String> entity = RequestEntity.post(null)
                .header("X-ApiKey", Constants.API_KEY)
                .header("X-ApiSecret", Constants.API_SECRET)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .body(in.toString());

        final JsonObject out =
                new RestTemplate().postForObject(Constants.API_PREFIX +
                        Constants.API_HOST +
                        ":" + Constants.API_PORT +
                        "/api/bpij/admin/activityLogs", entity, JsonObject.class);
        LOGGER.info("[ successCount=" + siretSuccessCount + " | failureCount" + siretFailureCount + "]  Réponse API --> " + out);

        return true;
    }

    public static boolean submitBpijCountProofToDB(String siret, String dateDebut,
                                                   String dateFin, String nombreBPIJ,
                                                   String jpgPath, Logger LOGGER) {
        final HttpHeaders headers = new HttpHeaders();
        headers.set("X-ApiKey", Constants.API_KEY);
        headers.set("X-ApiSecret", Constants.API_SECRET);
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);

        final MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
        body.add("siret", siret);
        body.add("dateDebut", dateDebut);//UtilsMeth.getDateAsSeconds(dateDebut));
        body.add("dateFin", dateFin);//UtilsMeth.getDateAsSeconds(dateFin));
        body.add("nombreBpij", nombreBPIJ);
        body.add("preuve", new FileSystemResource(jpgPath));
        LOGGER.info("-->");

        final HttpEntity<MultiValueMap<String, Object>> entity = new HttpEntity<>(body, headers);
        LOGGER.info("-->");

        final JsonObject out =
                new RestTemplate().postForObject(
                        Constants.API_PREFIX +
                                Constants.API_HOST +
                                ":" + Constants.API_PORT +
                                "/api/bpij/admin/bpijCounts", entity, JsonObject.class);
        LOGGER.info("[" + siret + "] Réponse API submitBpijCountProofToDB " + out);

        return true;
    }

    private static void submitSecuLoginToDB(String siret, String nom, String prenom, String password, Logger LOGGER) {
        final JsonObject in = new JsonObject();
        in.addProperty("siret", siret);
        in.addProperty("nom", nom);
        in.addProperty("prenom", prenom);
        in.addProperty("motDePasse", password);
        in.addProperty("societe", "societe");

        final RequestEntity<String> entity = RequestEntity.post(null)
                .header("X-ApiKey", Constants.API_KEY)
                .header("X-ApiSecret", Constants.API_SECRET)
                .contentType(MediaType.APPLICATION_JSON_UTF8).body(in.toString());

        final JsonObject out =
                new RestTemplate().postForObject(
                        Constants.API_PREFIX
                                + Constants.API_HOST
                                + ":" + Constants.API_PORT
                                + "/api/bpij/user/logins", entity, JsonObject.class);

        LOGGER.info("[" + siret + "] Réponse API submit SecuLogin " + out);
    }

    public static void main(final String[] args) {
        String logins2 = "1;IGIER;NATHALIE;1\n" +
                "2;clodic;yves;2\n" +
                "3;LES ROSIERS;LES ROSIERS;3\n" +
                "4;RAKOTOASITERA;CAPRICE;4\n" +
                "5;CAPSECUR;CAPSECUR;5\n" +
                "6;PRADEL;MICHEL;6\n" +
                "7;BASZYNSKI;MONIQUE;7";

        Logger LOGGER = Logger.getLogger(NetEntreprisesBot.class);
        for (String line0 : logins2.split("\n")) {
            String[] line = line0.split(";");
            submitSecuLoginToDB(line[0], line[1], line[2], line[3], LOGGER);
            setLoginStatus(line[0], "TEST", LOGGER);
        }

        submitBPIJXMLToDB("1", "1", "1", "testFiles\\xml.xml", "1", "27/09/2019", 122.2, LOGGER);
        submitBpijCountProofToDB("1", "26/09/2019", "26/09/2019", "1", "testFiles\\test.jpg", LOGGER);
        getLoginsList();
        submitExtractionLogToDB("27/09/2019", "SALUT", 1, 1, "BPIJ", LOGGER);
    }
}

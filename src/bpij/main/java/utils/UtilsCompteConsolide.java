package bpij.main.java.utils;

import java.io.*;

public class UtilsCompteConsolide {
    public static void main(String[] args) throws IOException {
        writeCompteConsolideReport("11-04-2019", "C:\\Users\\Lbedoucha Conseil" +
                "\\IdeaProjects\\simplicia-bpij\\sortie\\siren\\4-4-2019", false);
    }

    public static void writeCompteConsolideReport(String date, String folder, boolean isParSalarie) throws IOException {
        String outputFilenamePath = folder + "\\" + "Compte-Consolide-BPIJ--" + date + ".csv";
        BufferedWriter out = new BufferedWriter(new FileWriter(folder + "\\" + "Compte-Consolide-BPIJ--" + date + ".csv"));
        File[] files = new File(folder).listFiles();

        int cour0 = 0;
        for (File siret : files) {
            if (siret.isDirectory()) {
                File[] filesSiret = new File(siret.toString()).listFiles();
                for (File fileSiret : filesSiret) {
                    String csvFile = fileSiret.getName();
                    if (csvFile.endsWith(".csv") && !csvFile.startsWith("-")) {
                        String idBPIJ = "";
                        String[] csvFileTab = csvFile.split("---");

                        if (csvFileTab.length > 0 & !isParSalarie) {
                            idBPIJ = csvFileTab[0];
                        }

                        cour0++;
                        BufferedReader br = new BufferedReader(new FileReader(fileSiret));
                        String line;
                        int cour = 0;
                        while ((line = br.readLine()) != null) {
                            cour++;

                            if (cour == 1 && cour0 == 1) {
                                continue;
                            }

                            if (cour == 2 && cour0 == 1) {
                                line = line.replace(String.valueOf(line.charAt(1)), "");
                                line = line.replace("N SECURITE SOCIALE", "NNS");

                                if (isParSalarie) {
                                    out.write("SIRET" + ";" + line.replace("N° SECURITE SOCIALE", "NNS"));
                                } else {
                                    out.write("SIRET" + ";ID BPIJ;" + line.replace("N° SECURITE SOCIALE", "NNS"));
                                }

                                out.newLine();
                                continue;
                            }

                            if (cour <= 2 && cour0 > 1) {
                                continue;
                            }

                            if (line.split(";").length == 0) {
                                continue;
                            }

                            if (isParSalarie) {
                                out.write(siret.getName().substring(0, 14) + ";" + line.substring(1));
                            } else {
                                out.write(siret.getName().substring(0, 14) + ";" + idBPIJ + ";" + line.substring(1));
                            }

                            out.newLine();
                        }
                    }
                }
            }
        }

        out.close();
        CSVToExcelConverter.CSVtoXLS(outputFilenamePath, outputFilenamePath.replace("csv", "xls"), ";", false);
    }
}

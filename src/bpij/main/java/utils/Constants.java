package bpij.main.java.utils;

public class Constants {
    static final String BROWSER = "Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36";
    static final int TIMEOUT = 17_000;
    public static String PHANTOM_JS_PATH = "browser.exe";
    public static final String LOGIN_URL = "https://www.net-entreprises.fr/";
    public static final int DELAY = 1;

    public static final String CONFIG_FILE = "config.txt";

    public static final String LINKEDIN_DEMO_FILE = "demo.txt";
    public static final String BPIJ_FILE_FORMAT = "prestacsv";//"exportcsv";
    public static final boolean COMPTE_CONSOLIDE = true;
    public static final boolean LIMITED_RUN = false;
    public static final int MAX_LOGINS_TO_PROCESS = 1;
    public static boolean IS_DEMO_LINKEDIN = false;
    public static boolean IS_DATE_BLOCKED_DEMO = false;
    public static int DAY_DATE_BLOCKED_DEMO = 30;
    public static int MONTH_DATE_BLOCKED_DEMO = 12;
    public static int YEAR_DATE_BLOCKED_DEMO = 2019;
    public static boolean IS_COMMAND_LINE_VERSION = false;
    public static boolean IS_API_VERSION = false;
    public static final String HARDCODED_API_KEY = "";
    public static final String HARDCODED_API_SECRET = "" ;
    public static String API_KEY = System.getenv("SMPL_ADMIN_APIKEY");
    public static String API_SECRET = System.getenv("SMPL_ADMIN_APISECRET");
    public static String API_ADMIN_SUPER_KEY = System.getenv("SMPL_ADMIN_SUPER_APIKEY");
    public static String API_ADMIN_SUPER_SECRET = System.getenv("SMPL_ADMIN_SUPER_APISECRET");
    public static final boolean IS_HARDCODED_API_KEYS=false;
    static String API_HOST = "preprod-saas.simplicia.co";
    static String API_PORT = "8080";
    static String API_PREFIX = "http://";
}

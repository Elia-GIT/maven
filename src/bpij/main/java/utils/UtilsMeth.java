package bpij.main.java.utils;

import javafx.scene.control.DatePicker;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.nio.channels.Channels;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.Normalizer;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.util.List;
import java.util.*;
import java.util.concurrent.TimeUnit;

public class UtilsMeth {
    private static final Logger LOGGER = Logger.getLogger(UtilsMeth.class);

    public static int getYear() {
        return new Date().toInstant().atZone(ZoneId.systemDefault()).toLocalDate().getYear();
    }

    public static int getMonth() {
        return new Date().toInstant().atZone(ZoneId.systemDefault()).toLocalDate().getMonthValue();
    }

    public static int getDay() {
        return new Date().toInstant().atZone(ZoneId.systemDefault()).toLocalDate().getDayOfMonth();
    }

    public static int getHour() {
        return GregorianCalendar.getInstance().get(Calendar.HOUR_OF_DAY);   // gets hour in 12h format
    }

    public static int getMinute() {
        return GregorianCalendar.getInstance().get(Calendar.MINUTE);   // gets hour in 12h format
    }

    public static int getSecond() {
        return GregorianCalendar.getInstance().get(Calendar.SECOND);   // gets hour in 12h format
    }

    public static String stripAccents(String s) {
        return Normalizer.normalize(s, Normalizer.Form.NFD).replaceAll("[\\p{InCombiningDiacriticalMarks}]", "");
    }

    public static String cleanPrintDates(String beginningDate, String endDate) {
        if (beginningDate == null | endDate == null) {
            return "";
        } else {
            return "--" + beginningDate.replace("/", "-") + "-" + endDate.replace("/", "-");
        }
    }

    public static String cleanPrintEmployeeData(String customEmployeeName) {
        if (customEmployeeName == null) {
            return "";
        }

        String[] customEmployeeNameTab = customEmployeeName.split(";");
        String cleanPrint = "--";

        if (customEmployeeName.length() >= 1) {
            cleanPrint = cleanPrint + customEmployeeNameTab[0];
        }

        if (customEmployeeNameTab.length == 2) {
            cleanPrint = cleanPrint + "--" + customEmployeeNameTab[1];
        }

        if (customEmployeeNameTab.length == 3) {
            cleanPrint = cleanPrint + "--" + customEmployeeNameTab[2];
        }

        return cleanPrint;
    }

    public static HashMap initEtatSiret(int num) {
        HashMap<Integer, List<String>> h = new HashMap<>();

        for (int i = 0; i < num; i++) {
            ArrayList<String> list = new ArrayList<>();
            h.put(i, list);
        }

        return h;
    }

    public static boolean downloadFile(String downloadUrl, String cookie, String path) {
        try {
            TimeUnit.SECONDS.sleep(2);
            URLConnection conn = new URL(downloadUrl).openConnection();
            conn.setRequestProperty("Cookie", cookie);

            try (InputStream inputStream = conn.getInputStream()) {
                try (final FileOutputStream fos = new FileOutputStream(new File(path))) {
                    fos.getChannel().transferFrom(Channels.newChannel(inputStream), 0, Long.MAX_VALUE);
                    return true;
                }
            }
        } catch (Exception ex) {
            LOGGER.error("ERROR - downloadFile. downloadUrl: " + downloadUrl, ex);
        }

        return false;
    }

    public static String removeLastChar(String str, String character) {
        if (str.endsWith(character)) {
            str = StringUtils.substringBeforeLast(str, character);
        }

        return str;
    }

    public static String getLicenseUrlFromFile(String filename) {
        String url = "";
        File file = new File(filename);
        try (FileReader reader = new FileReader(file)) {
            char[] chars = new char[(int) file.length()];
            reader.read(chars);

            String licenseUrl = StringUtils.substringAfter(new String(chars), "http").trim();
            if (licenseUrl.contains(" ")) {
                licenseUrl = StringUtils.substringBefore(licenseUrl, " ").trim();
            }

            return "http" + licenseUrl;
        } catch (Exception e) {
            LOGGER.error("ERROR - getLicenseUrlFromFile. name: " + filename, e);
        }

        return url;
    }

    public static List<Integer> getLicenseValues(String url) {
        List<Integer> values = new ArrayList<>();

        try {
            Document doc = Jsoup.connect(url).ignoreContentType(true).
                    validateTLSCertificates(false).
                    userAgent(Constants.BROWSER).ignoreHttpErrors(true).timeout(Constants.TIMEOUT).get();
            values.add(Integer.valueOf(doc.select("#X").text().trim()));
            values.add(Integer.valueOf(doc.select("#Y").text().trim()));
        } catch (Exception e) {
            LOGGER.error("ERROR - getLicenseValues. url : " + url, e);
        }

        return values;
    }

    public static boolean isValidDemo(int dayOfMonth, int month, int year, Logger LOGGER) {
        LOGGER.info("hello");
        String TIME_SERVER = "time-a.nist.gov";
        LOGGER.info("hello1");
        LOGGER.info("hello2");

        Calendar c = Calendar.getInstance();
        Date today = c.getTime();
        LOGGER.info(today);
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);

        c.set(Calendar.YEAR, year);
        c.set(Calendar.MONTH, month);
        c.set(Calendar.DAY_OF_MONTH, dayOfMonth);

        Date dateSpecified = c.getTime();
        LOGGER.info(today + "--" + dateSpecified + "--" + today.before(dateSpecified));

        return today.before(dateSpecified);
    }

    public static void writeFile(String path, String data) {
        try {
            Files.write(Paths.get(path), data.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String readFile(String filePath) throws IOException {
        return FileUtils.readFileToString(new File(filePath), String.valueOf(StandardCharsets.UTF_8));
    }

    public static boolean hasInputDateUser(DatePicker datePickerDu, DatePicker datePickerAu) {
        return datePickerDu.getValue().toString().length() > 0 && datePickerAu.getValue().toString().length() > 0;
    }

    public static boolean hasInputDateUserCLI(String dateStart, String dateEnd) {
        return dateStart.length() > 0 && dateEnd.length() > 0;
    }

    public static String getCodeCPAM(String s) {
        HashMap<String, String> h = new HashMap<>();
        String[] cpamCodeTab = Text.cpamCode.split("\n");
        String[] cpamStrings = Text.cpamList.split("\n");

        for (int i = 0; i < cpamCodeTab.length; i++) {
            h.put(UtilsMeth.stripAccents(cpamStrings[i].replace("'", "-")
                    .replace("-", "-")
                    .replace(" ", "-")
                    .toLowerCase()), cpamCodeTab[i]);
        }

        return h.get(s);
    }

    static String getDateAsSeconds(String inputDate) {
        Date date = null;
        try {
            date = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").parse(inputDate + " 12:00:00");
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return String.valueOf(date.getTime());
    }

    public static BufferedImage joinBufferedImage(BufferedImage img1, BufferedImage img2) {
        int offset = 2;
        int width = img1.getWidth() + img2.getWidth() + offset;
        int height = Math.max(img1.getHeight(), img2.getHeight()) + offset;

        BufferedImage newImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g2 = newImage.createGraphics();
        Color oldColor = g2.getColor();

        g2.setPaint(Color.BLACK);
        g2.fillRect(0, 0, width, height);
        g2.setColor(oldColor);
        g2.drawImage(img1, null, 0, 0);
        g2.drawImage(img2, null, img1.getWidth() + offset, 0);
        g2.dispose();

        return newImage;
    }
}

package bpij.main.java.controller;

import bpij.main.java.bot.NetEntreprisesBot;
import bpij.main.java.csv.CsvReaderWriter;
import bpij.main.java.model.InputData;
import bpij.main.java.model.License;
import bpij.main.java.utils.Constants;
import bpij.main.java.utils.UtilsCompteConsolide;
import bpij.main.java.utils.UtilsMeth;
import bpij.main.java.utils.UtilsRapportRun;
import com.google.common.base.Stopwatch;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import static bpij.main.java.utils.UtilsLicenseLinkedIn.getId;
import static bpij.main.java.utils.UtilsMeth.initEtatSiret;

public class ControllerGUI implements Initializable {
    private volatile boolean FLAG_1 = false;
    private static final Logger LOGGER = Logger.getLogger(ControllerGUI.class);

    @FXML
    private Button selectBtn;
    @FXML
    private Button outBtn;
    @FXML
    private Button startSc1Btn;
    @FXML
    private Button stopSc1Btn;
    @FXML
    private CheckBox schChk;

    private FileChooser fileChooser = new FileChooser();
    private String csvPath;
    private DirectoryChooser directoryChooser = new DirectoryChooser();
    private String outputFolderPath;

    @FXML
    private TextArea txtA;
    @FXML
    private Label curr1Lbl;
    @FXML
    private Label size1Lbl;
    @FXML
    private TextField failedTimeOutTxtF;


    //to access new added textfields
    @FXML
    private TextField txtNom, txtPrenom, txtNss, txtDelai;

    //to access the datepickers
    @FXML
    static
    DatePicker datePickerAu;
    @FXML
    static
    DatePicker datePickerDu;

    private ExecutorService thr1;
    private CsvReaderWriter rdWr = new CsvReaderWriter();
    /**
     * VARIABLE DE LOG
     **/
    private int fileToDownload = 0;
    private NetEntreprisesBot currentBot;

    @FXML
    public void startSc1() {
        if (StringUtils.isEmpty(csvPath) || StringUtils.isEmpty(outputFolderPath)) {
            writeToTextArea("Merci de choisir un fichier/dossier !");
            return;
        } else {
            csvPath = "logins-short-1.txt";
            outputFolderPath = "sortie";
        }
        FLAG_1 = false;
        thr1 = Executors.newSingleThreadExecutor();
        thr1.submit(this::start);
    }

    @FXML
    public void stopSc1() {
        if (currentBot != null) {
            currentBot.leaveNetEntreprises();
            currentBot.closePhantomJsBr();
        }
        if (thr1 != null) {
            thr1.shutdownNow();
        }
        FLAG_1 = true;
        writeToTextArea("Planificateur arrГЄtГ© !");
    }

    private void start() {
        if (schChk.isSelected()) {
            writeToTextArea("Lancement auto dГ©marrГ© !");

            while (!FLAG_1) {
                FLAG_1 = false;
                impl();
                writeToTextArea("Prochain lancement prГ©vu Г  12H00 . . .");

                for (int i = 1; i <= (24 * 3600); i++) {
                    if (FLAG_1 || new Date().toString().contains("12:00:0")) {
                        break;
                    }

                    try {
                        TimeUnit.SECONDS.sleep(1);
                    } catch (InterruptedException e) {
                        //
                    }
                }
            }
        } else {
            if (licenseValuesRun().equals(License.ACTIVE)) {
                impl();
            }
        }

        thr1.shutdown();
    }

    private void impl() {
        writeToTextArea("DГ©but - " + time());
        List<InputData> inputDatas = rdWr.readInputDataList(csvPath);
        int size = inputDatas.size() - 1;
        int delay = Integer.parseInt(failedTimeOutTxtF.getText().trim());
        Platform.runLater(() -> curr1Lbl.setText("0"));
        Platform.runLater(() -> size1Lbl.setText("0"));
        Platform.runLater(() -> size1Lbl.setText(String.valueOf(size)));

        int count = 1;
        NetEntreprisesBot bot = new NetEntreprisesBot();
        currentBot = bot;
        if (!printPeriodDates(bot)) {
            writeToTextArea("Merci d'indiquer une pГ©riode pour la rГ©cupГ©ration");
            LOGGER.info("STOPPING : no date entered by user");
            return;
        }
        CsvReaderWriter writer = new CsvReaderWriter();
        String date = UtilsMeth.getDay() + "-"
                + UtilsMeth.getMonth() + "-" + UtilsMeth.getYear() + "-"
                + UtilsMeth.getHour() + "H-" + UtilsMeth.getMinute()
                + "M-" + UtilsMeth.getSecond() + "-s";
        String folder = outputFolderPath + "/{DATE}/";
        folder = folder.replace("{DATE}", date);
        outputFolderPath = folder;
        String successName = folder + "SIRET-rГ©ussis"/* + time().replace(":", "-")*/ + ".csv";
        String failedName = folder + "SIRET-Г©chouГ©s" /*+ time().replace(":", "-") */ + ".csv";
        new File(folder).mkdirs();
        if (!inputDatas.isEmpty()) {
            writer.writeHeaderLine(inputDatas.get(0).getHeaderLine(), successName);
            writer.writeHeaderLine(inputDatas.get(0).getHeaderLine(), failedName);
        }

        if (Constants.IS_DEMO_LINKEDIN && !Constants.IS_DATE_BLOCKED_DEMO) {
            try {
                if (Jsoup.connect("http://licence-simplicia.pbconseil.ovh/linkedin/" + getId(LOGGER) + ".html").execute().statusCode() != 200) {
                    writeToTextArea("DEMO INACTIVE");
                    return;
                }
            } catch (IOException e) {
                writeToTextArea("DEMO INACTIVE");
                return;
            }
            File f = new File(Constants.LINKEDIN_DEMO_FILE);
            if (f.exists() && !f.isDirectory()) {
                writeToTextArea("DEMO ACTIVE");
            } else {
                writeToTextArea("FICHIER DEMO MANQUANT");
                return;
            }
        }

        if (Constants.IS_DATE_BLOCKED_DEMO && !UtilsMeth.isValidDemo(Constants.DAY_DATE_BLOCKED_DEMO, Constants.MONTH_DATE_BLOCKED_DEMO - 1, Constants.YEAR_DATE_BLOCKED_DEMO, LOGGER)) {
            writeToTextArea("DEMO INACTIVE -- ECHEANCE = " + Constants.DAY_DATE_BLOCKED_DEMO + "/" + Constants.MONTH_DATE_BLOCKED_DEMO + "/" + Constants.YEAR_DATE_BLOCKED_DEMO);
            return;
        }

        long timerCour = 0;
        long averageTime = 0;
        fileToDownload = 0;

        HashMap<Integer, List<String>> etatSiret = initEtatSiret(10);
        for (int k = 1; k < inputDatas.size(); k++) {
            if (Constants.LIMITED_RUN) {
                if (k > Constants.MAX_LOGINS_TO_PROCESS) {
                    continue;
                }
            }
            LOGGER.info("INPUTDATA SIZE " + inputDatas.size());
            InputData inputData = inputDatas.get(k);
            if (FLAG_1) {
                break;
            }
            boolean isOpened = bot.openPhantomJs();
            if (!isOpened) {
                writeToTextArea("Erreur Г  l'ouverture de PhantomJS - assurez-vous que le fichier soit dans le mГЄme dossier que Simplicia.");
                break;
            }
            final int finalCount = count;
            Platform.runLater(() -> curr1Lbl.setText(String.valueOf(finalCount)));

            count++;
            String companyID = inputData.getCompanyID();
            if (companyID.length() != 14) {
                writeToTextArea("[" + companyID + "] " + "SIRET DE TAILLE INCORRECTE : IGNORE");
                continue;
            }
            writeToTextArea("                             ***");
            writeToTextArea("[" + companyID + "] " + "Connexion en cours");
            Stopwatch timer = new Stopwatch().start();
            int login = bot.login(inputData);
            int isSuc = scImpl(login, bot, companyID, inputData, txtNom, txtPrenom, txtNss);
            timer.stop();
            timerCour = timerCour + timer.elapsed(TimeUnit.MINUTES);
            writeToTextArea("[" + companyID + "] " + "DurГ©e du run : " + timer.elapsed(TimeUnit.SECONDS) + "sec");
            inputData.setStatusLogin(isSuc);

            if (isSuc == 7) {
                //le login est bon malgrГ© tout
                LOGGER.info("[" + companyID + "] " + "LOGIN MARQUE Г  OK");
                writer.writeCSV(inputData, successName);
                writeToTextArea("[" + companyID + "] " + "Pause de " + delay + " secondes . . .");
                try {
                    TimeUnit.SECONDS.sleep(delay);
                } catch (InterruptedException ignored) {
                }
            } else {
                if (isSuc >= 0 && isSuc <= 6) {
                    LOGGER.info("LOGIN MARQUE KO");
                    writer.writeCSV(inputData, failedName);
                    writeToTextArea("[" + companyID + "] " + "Pause de " + delay + " secondes . . .");
                    try {
                        TimeUnit.SECONDS.sleep(delay);
                    } catch (InterruptedException ignored) {
                    }
                }
            }

            etatSiret.get(isSuc).add(companyID);
            bot.closePhantomJsBr();
        }

        //Ecriture du compte consolidГ©
        if (Constants.COMPTE_CONSOLIDE) {
            try {
                UtilsCompteConsolide.writeCompteConsolideReport(date, folder, false);
                writeToTextArea("GГ©nГ©ration du Compte Consolide rГ©ussie");
            } catch (IOException ex) {
                writeToTextArea("Erreur de gГ©nГ©ration du Compte ConsolidГ©");
            }
        }

        writeToTextArea(UtilsRapportRun.writeRappportRun(etatSiret, timerCour, averageTime, 9));
        writeToTextArea("Fin - " + time());
    }

    /*
      renvoie 7 si le login a rГ©ussi et le download s'est bien passГ©.
      renvoie 6 si le login a rГ©ussi et pas assez de fichiers downloadГ©s
      renvoie 5 si le login a rГ©ussi et pas assez de fichiers downloadГ©s
      renvoie 4 si le login a rГ©ussi et pas assez de fichiers downloadГ©s
      renvoie 3 si le compte n'avait pas de BPIJ
      renvoie 2 si erreur login
      renvoie 1 si le compte n'a pas accГЁs Г  BPIJ
      renvoie 0 si erreur dans le parcours jusqu'au BPIJ
      renvoie 8 si erreur net-e : nous ne pouvons rГ©pondre Г  votre demande
      renvoie 9 si compte bloquГ© 3 Г©checs mot de passe
      */
    private int scImpl(int login, NetEntreprisesBot bot, String companyID, InputData inputData, TextField txtNom, TextField txtPrenom, TextField txtNss) {
        int waitTimeMin = 1;
        int maxAttempts = 1;
        int success = 0;
        fileToDownload = 0;
        while (success != 7 && maxAttempts <= 2) {
            maxAttempts++;
            if (login == 1) {
                writeToTextArea("[" + companyID + "][" + maxAttempts + "]" + "Connexion rГ©ussie !");
                writeToTextArea("[" + companyID + "] " + "AccГЁs en cours au compte BPIJ...");
                if (bot.passwordWillExpire()) {
                    writeToTextArea("[" + companyID + "] " + "Le mot de passe expire bientГґt");
                }
                /*starting loop for wholesearch inside bpij acc*/
                DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
                LocalDateTime now = LocalDateTime.now();
                LocalDateTime todayDate = LocalDateTime.of(now.getYear(), now.getMonthValue(), now.getDayOfMonth(), 0, 0);

                int passeCountCouloirDuTemps = 1;
                int nbAnneesCouloirDuTemps = 10;
                int couloirDuTempsNbPasses = UtilsMeth.hasInputDateUser(datePickerDu, datePickerAu) ? 1 : 10;
                LocalDateTime endDate = LocalDateTime.of(now.getYear(), now.getMonthValue(), now.getDayOfMonth(), 0, 0);
                LocalDateTime begDate = endDate.minusDays(45);
                if (UtilsMeth.hasInputDateUser(datePickerDu, datePickerAu)) {
                    begDate = datePickerDu.getValue().atStartOfDay();
                    endDate = datePickerAu.getValue().atStartOfDay();
                } else {
                    writeToTextArea("Pas de pГ©riode sГ©lectionnГ©e --> arrГЄt");
                    continue;
                }
                /*passeCouloirDuTemps <= 1 permet de fixer Г  BPIJ normal = selon date du user (+begDate/endDate=null)*/
                while (begDate.isAfter(todayDate.minusDays(nbAnneesCouloirDuTemps * 365)) && passeCountCouloirDuTemps <= couloirDuTempsNbPasses) {
                    LOGGER.info("Etape Boucle " + passeCountCouloirDuTemps++);
                    writeToTextArea("[" + companyID + "] " + "Nom=\"" + txtNom.getText() + "\" | Prenom=\"" + txtPrenom.getText() + "\" | Nss=\"" + txtNss.getText() + "\"");
                    String resultGoToSearchPage = bot.goToSearchPage(companyID, inputData, begDate, endDate, txtNom, txtPrenom, txtNss);
                    String resultGoToDownloadPage;
                    if (resultGoToSearchPage.startsWith("[3bis] accГЁs OK Г  la page de recherche")) {
                        resultGoToDownloadPage = bot.goToDownloadPage(companyID, inputData);
                    } else {
                        resultGoToDownloadPage = resultGoToSearchPage;
                    }
                    //SI LA VOIE EST LIBRE, ON DOWNLOAD
                    String outputParPeriodeFolderPathCompany = outputFolderPath
                            + companyID
                            + UtilsMeth.cleanPrintDates(bot.getBeginningDate(), bot.getEndDate())
                            + UtilsMeth.cleanPrintEmployeeData(inputData.getEmployeeCustomName())
                            + "/par-periode/"
                            + passeCountCouloirDuTemps
                            + "--"
                            + dtf.format(begDate).replace("/", "-")
                            + "--"
                            + dtf.format(endDate).replace("/", "-");
                    new File(outputParPeriodeFolderPathCompany).mkdirs();
                    HashMap<String, String> hash;
                    LOGGER.info("par salarie ? " + bot.parSalaries(inputData));
                    if (bot.parSalaries(inputData)) {
                        hash = bot.getPdfUrlsType2(companyID, Constants.BPIJ_FILE_FORMAT);
                    } else {
                        hash = bot.getPdfUrlsType1(companyID, Constants.BPIJ_FILE_FORMAT);
                    }
                    int nbBPIJ = checkIfBpijToDownload(resultGoToDownloadPage) ? hash.size() : 0;
                    String jpegScreenshotPath = outputParPeriodeFolderPathCompany
                            + "/" + "preuve-" + nbBPIJ + "-BPIJ-trouves-periode---"
                            + dtf.format(begDate).replace("/", "-")
                            + "--"
                            + dtf.format(endDate).replace("/", "-");
                    try {
                        BufferedImage joinedScreenshot = UtilsMeth.joinBufferedImage(
                                ImageIO.read(new File("LOG/preSearch-" + companyID + ".jpg")),
                                ImageIO.read(new File("LOG/resultsPage-" + companyID + ".jpg")));
                        ImageIO.write(joinedScreenshot, "jpg", new File("LOG/concat-" + companyID + ".jpg"));
                        ImageIO.write(joinedScreenshot, "jpg", new File(jpegScreenshotPath));

                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    success = assignSuccessValueAndLeaveNetEntreprises(companyID, resultGoToDownloadPage, bot, dtf, begDate, endDate);
                    if (checkIfBpijToDownload(resultGoToDownloadPage)) {

                        Set<String> filesToDownloadUrls = hash.keySet();
                        writeToTextArea("[" + companyID + "] " + "BPIJ trouvГ©s : " + filesToDownloadUrls.size() + " | " + bot.getBeginningDate() + "--" + bot.getEndDate());
                        String cookies = bot.getCookies();
                        String companyFolder = companyID;
                        if (inputData.getEmployeeCustomName() != null) {
                            companyFolder = inputData.getEmployeeCustomName();
                        }

                        for (String filesToDownloadUrl : filesToDownloadUrls) {
                            // Obtain a number between [0 - 49].
                            String idBPIJ = "";
                            String[] filesToDownloadUrlTab = filesToDownloadUrl.split("identifiant=");
                            if (filesToDownloadUrlTab.length >= 2) {
                                idBPIJ = filesToDownloadUrlTab[1];
                            }
                            String fileName = idBPIJ + "---" + hash.get(filesToDownloadUrl)  /*getRandomHexString(30) +*/;
                            //create outputfolderpath
                            /* ajouter un folder /par-cpam/cpamXX/BPIJ-YY.CSV
                             *
                             *
                             *
                             */
                            String outputFolderPathCompany = outputFolderPath
                                    + companyFolder
                                    + UtilsMeth.cleanPrintDates(bot.getBeginningDate(), bot.getEndDate())
                                    + UtilsMeth.cleanPrintEmployeeData(inputData.getEmployeeCustomName());/*
                                    + "/par-cpam/" + UtilsMeth.getCpamFolder(fileName);*/

                            new File(outputFolderPathCompany).mkdirs();
                            LOGGER.info(" currentURL =" + bot.getDriver().getCurrentUrl());
                            LOGGER.info(" downnloadURL =" + "https://dsij-bpij.net-entreprises.ameli.fr/IhmHF/" + filesToDownloadUrl);
                            String fileToDownloadPath = outputFolderPathCompany
                                    + "/" + fileName;
                            boolean isDownloaded = UtilsMeth.downloadFile("https://dsij-bpij.net-entreprises.ameli.fr/IhmHF/" + filesToDownloadUrl,
                                    cookies,
                                    fileToDownloadPath);

                            String msgTxt = "[ERREUR] Erreur TГ©lГ©chargement Fichier - " + fileName;
                            if (isDownloaded) {
                                fileToDownload++;
                                msgTxt = "TГ©lГ©chargement de fichier rГ©ussi - " + fileName;
                            }
                            writeToTextArea("[" + companyID + "] " + msgTxt);
                        }
                        success = 7;
                        if (fileToDownload != filesToDownloadUrls.size()) {
                            success = 4;
                        }

                        bot.clickOnPassedAnchor("Quitter");
                        bot.leaveNetEntreprises();

                    } else {
                        if (maxAttempts >= 2) {
                            writeToTextArea("[" + companyID + "][" + maxAttempts + "]" + "Reconnexion en cours");
                        }
                        login = bot.login(inputData);
                    }
                    endDate = endDate.minusDays(8);
                    begDate = endDate.minusDays(7);
                }
            } else {
                if (login == -1) {
                    success = 2;
                    writeToTextArea("[" + companyID + "] " + " Connexion Г©chouГ©e - accГЁs incorrects!");
                    login = bot.login(inputData);
                    writeToTextArea("[" + companyID + "][" + maxAttempts + "]" + "Connexion en cours");
                }
                if (login == 0) {
                    success = 8;
                    writeToTextArea("[" + companyID + "] " + " Erreur Net-Entreprises -- Nous ne sommes pas en mesure de rГ©pondre Г  votre demande.");
                    writeToTextArea("[" + companyID + "] " + "Pause " + waitTimeMin + "min puis nouvelle tentative");
                    try {
                        //WAIT 10 MINUTES
                        Thread.sleep(waitTimeMin * 60 * 1000);
                    } catch (InterruptedException e) {
                        LOGGER.info("ERREUR DE SLEEP");
                    }
                    login = bot.login(inputData);
                    writeToTextArea("[" + companyID + "][" + maxAttempts + "]" + "Connexion en cours");
                }
                if (login == 9) {
                    success = -2;
                    writeToTextArea("[" + companyID + "] " + " Echec 3 fois sur le mot de passe : blocage du compte 5min");
                    login = bot.login(inputData);
                    writeToTextArea("[" + companyID + "][" + maxAttempts + "]" + "Connexion en cours");
                }
            }
        }

        return success;
    }

    private void writeMessage(String message) {
        txtA.appendText(message + "\n");
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        setStyleButtons("-fx-background-color: gray");
        String activationFile = System.getProperty("user.home") + "\\" + "khsfv--__34p.pwd";

        if (new File(activationFile).exists()) {
            licenseValuesRun();
        } else if (new File(Constants.CONFIG_FILE).exists()) {
            String licenseUrl = UtilsMeth.getLicenseUrlFromFile(Constants.CONFIG_FILE);
            if (StringUtils.isEmpty(licenseUrl) || !licenseUrl.contains("pbconseil.ovh")) {
                writeToTextArea("config.txt invalide");
                disableButtons();
            } else {
                licenseValues(activationFile);
            }
        } else {
            disableButtons();
        }

        fileChooser.setInitialDirectory(new File(System.getProperty("user.dir")));
        selectBtn.setOnAction(e -> {
            File file = fileChooser.showOpenDialog(null);
            if (file != null) {
                csvPath = file.getAbsolutePath();
                writeToTextArea("Fichier sГ©lectionnГ© - " + csvPath);
                setStyleButtons(null);
            }
        });

        directoryChooser.setInitialDirectory(new File(System.getProperty("user.dir")));
        outBtn.setOnAction(e -> {
            File folder = directoryChooser.showDialog(null);
            if (folder != null) {
                outputFolderPath = folder.getAbsolutePath();
                writeToTextArea("RГ©pertoire de destination - " + outputFolderPath);
            }
        });
    }

    private void licenseValues(String activePath) {
        List<Integer> licenseValues = UtilsMeth.getLicenseValues(UtilsMeth.getLicenseUrlFromFile(Constants.CONFIG_FILE));

        if (licenseValues.size() < 2) {
            writeToTextArea("Erreur critique : fichier de licence manquant ?");
            disableButtons();
        } else {
            Integer x = licenseValues.get(0);

            if (x <= 0) {
                writeToTextArea("Licence dГ©sactivГ©e");
                disableButtons();
            } else if (x >= 2) {
                writeToTextArea("Licence dГ©jГ  activГ©e");
                disableButtons();
            } else {
                try {
                    if (new File(activePath).createNewFile()) {
                        writeToTextArea("Licence activГ©e");
                        return;
                    }
                } catch (Exception e) {
                    LOGGER.error("ERROR - create activation file", e);
                }

                writeToTextArea("Erreur critique : fichier de licence manquant ?");
                disableButtons();
            }
        }
    }

    private void disableButtons() {
        Platform.runLater(() -> startSc1Btn.setDisable(true));
        Platform.runLater(() -> stopSc1Btn.setDisable(true));
    }

    private void setStyleButtons(String style) {
        Platform.runLater(() -> startSc1Btn.setStyle(style));
        Platform.runLater(() -> stopSc1Btn.setStyle(style));
    }

    private License licenseValuesRun() {
        if (Constants.IS_DEMO_LINKEDIN | Constants.IS_DATE_BLOCKED_DEMO) {
            return License.ACTIVE;
        }

        List<Integer> licenseValues = UtilsMeth.getLicenseValues(UtilsMeth.getLicenseUrlFromFile(Constants.CONFIG_FILE));
        if (licenseValues.size() >= 2) {
            Integer x = licenseValues.get(0);

            if (x >= 1) {
                writeToTextArea("SIMPLICIA BPIJ");
                writeToTextArea("Licence activée");
                return License.ACTIVE;
            } else {
                writeToTextArea("Licence désactivée");
                disableButtons();

                return License.DESACTIVE;
            }
        }

        writeToTextArea("Erreur critique : fichier de licence manquant ?");
        return License.UNKNOWN;
    }

    private void writeToTextArea(final String message) {
        Platform.runLater(() -> writeMessage(message));
    }

    private int assignSuccessValueAndLeaveNetEntreprises(String companyID, String resultGoToDownloadPage, NetEntreprisesBot bot, DateTimeFormatter dtf, LocalDateTime begDate, LocalDateTime endDate) {
        boolean noBPIJModule = resultGoToDownloadPage.startsWith("[1]");
        boolean notPassedBordereauxLink = resultGoToDownloadPage.startsWith("[2]");
        boolean notPassedRechercheBPIJ = resultGoToDownloadPage.startsWith("[3]");
        boolean notPassedSelectAllOK = resultGoToDownloadPage.startsWith("[4]");
        boolean tooMuchBPIJ = resultGoToDownloadPage.startsWith("[5]");
        boolean pleaseEnterASiret = resultGoToDownloadPage.startsWith("[6]");
        boolean noBPIJFound = resultGoToDownloadPage.startsWith("[7]");

        int success = 0;
        if (noBPIJModule) {
            writeToTextArea("[" + companyID + "] " + resultGoToDownloadPage + " | " + dtf.format(begDate) + "--" + dtf.format(endDate));
            bot.clickOnPassedAnchor("Quitter");
            bot.leaveNetEntreprises();
            success = 1;
        }
        if (notPassedBordereauxLink | notPassedRechercheBPIJ) {
            writeToTextArea("[" + companyID + "] " + resultGoToDownloadPage + " | " + dtf.format(begDate) + "--" + dtf.format(endDate));
            bot.clickOnPassedAnchor("Quitter");
            bot.leaveNetEntreprises();
            success = 0;
            LOGGER.info("on passe là " + notPassedSelectAllOK + " -- " + success);
        }
        if (noBPIJFound) {
            writeToTextArea("[" + companyID + "] " + resultGoToDownloadPage + " | " + dtf.format(begDate) + "--" + dtf.format(endDate));
            bot.clickOnPassedAnchor("Quitter");
            bot.leaveNetEntreprises();
            success = 3;
        }
        if (tooMuchBPIJ) {
            writeToTextArea("[" + companyID + "] " + resultGoToDownloadPage + " | " + dtf.format(begDate) + "--" + dtf.format(endDate));
            bot.clickOnPassedAnchor("Quitter");
            bot.leaveNetEntreprises();
            success = 5;
        }
        if (pleaseEnterASiret) {
            writeToTextArea("[" + companyID + "] " + resultGoToDownloadPage + " | " + dtf.format(begDate) + "--" + dtf.format(endDate));
            bot.clickOnPassedAnchor("Quitter");
            bot.leaveNetEntreprises();
            success = 6;
        }

        return success;
    }

    private boolean printPeriodDates(NetEntreprisesBot bot) {
        if (getBeginningPeriodDate(bot).length() == 0 | getEndDate(bot).length() == 0) {
            return false;
        }

        writeToTextArea("Période de récupération des BPIJ : " + getBeginningPeriodDate(bot) + " -- " + getEndDate(bot));
        return true;
    }

    private String getBeginningPeriodDate(NetEntreprisesBot bot) {
        if (ControllerGUI.datePickerDu.getValue() == null) {
            return "";
        }

        if (ControllerGUI.datePickerDu.getValue().toString().length() > 0 && ControllerGUI.datePickerAu.getValue().toString().length() > 0) {
            return DateTimeFormatter.ofPattern("dd/MM/yyyy").format(ControllerGUI.datePickerDu.getValue());
        }

        return bot.getBeginningDate();
    }

    private String getEndDate(NetEntreprisesBot bot) {
        if (ControllerGUI.datePickerAu.getValue() == null) {
            return "";
        }

        if (ControllerGUI.datePickerDu.getValue().toString().length() > 0 && ControllerGUI.datePickerAu.getValue().toString().length() > 0) {
            return DateTimeFormatter.ofPattern("dd/MM/yyyy").format(ControllerGUI.datePickerAu.getValue());
        }

        return bot.getEndDate();
    }

    @FXML
    public void exit(ActionEvent event) {
        if(currentBot!=null){
            currentBot.leaveNetEntreprises();
        }
        if (thr1 != null) {
            thr1.shutdownNow();
        }
        FLAG_1 = true;
        Node source = (Node) event.getSource();
        Stage stage = (Stage) source.getScene().getWindow();
        stage.close();
        System.exit(0);
    }

    private boolean checkIfBpijToDownload(String resultGoToDownloadPage) {
        boolean hasBPIJ = !resultGoToDownloadPage.startsWith("[1]");
        boolean passedBordereauxLink = !resultGoToDownloadPage.startsWith("[2]");
        boolean passedRechercheBPIJ = !resultGoToDownloadPage.startsWith("[3]");
        boolean passedSelectAllOK = !resultGoToDownloadPage.startsWith("[4]");
        boolean tooMuchBPIJ = resultGoToDownloadPage.startsWith("[5]");
        boolean pleaseEnterASiret = resultGoToDownloadPage.startsWith("[6]");
        boolean noBPIJFound = resultGoToDownloadPage.startsWith("[7]");
        boolean hasBPIJToDownload = hasBPIJ
                && passedBordereauxLink
                && passedRechercheBPIJ
                && !tooMuchBPIJ
                && !pleaseEnterASiret
                && !noBPIJFound;
        LOGGER.info("BPIJ TO DOWNLOAD ? " + hasBPIJToDownload);
        return hasBPIJToDownload;
    }

    private String time() {
        return new SimpleDateFormat("HH:mm:ss").format(Calendar.getInstance().getTime());
    }
}
package bpij.main.java.controller;

import bpij.main.java.bot.NetEntreprisesBot;
import bpij.main.java.csv.CsvReaderWriter;
import bpij.main.java.model.InputData;
import bpij.main.java.model.License;
import bpij.main.java.utils.Constants;
import bpij.main.java.utils.UtilsAPI;
import bpij.main.java.utils.UtilsMeth;
import bpij.main.java.utils.UtilsRapportRun;
import com.google.common.base.Stopwatch;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import static bpij.main.java.utils.UtilsLicenseLinkedIn.getId;
import static bpij.main.java.utils.UtilsMeth.initEtatSiret;

public class ControllerCLI {
    private volatile boolean FLAG_1 = false;
    private static final Logger LOGGER = Logger.getLogger(ControllerCLI.class);

    private ExecutorService thr1;
    private CsvReaderWriter rdWr = new CsvReaderWriter();
    private NetEntreprisesBot currentBot;
    private int fileToDownload = 0;
    private long timerCour = 0;
    HashMap<Integer, List<String>> etatSiret;

    public String csvPath;
    public String outputFolderPath;
    public String txtNom;
    public String txtPrenom;
    public String txtNss;
    public String dateStart;
    public String dateEnd;

    public void startSc1() {
        if (Constants.IS_API_VERSION) {
            csvPath = "logins-short-1.txt";
            outputFolderPath = "sortie";
            txtNom = "";
            txtPrenom = "";
            txtNss = "";
            dateStart = ""; // Format is 01/01/2020
            dateEnd = ""; // Format is 31/12/2020
        }

        FLAG_1 = false;
        thr1 = Executors.newSingleThreadExecutor();
        thr1.submit(this::start);
    }

    private void start() {
        if (licenseValuesRun().equals(License.ACTIVE)) {
            impl();
        }

        thr1.shutdown();
        exitCommandLine();
    }

    private void impl() {
        writeToTextArea("Début - " + time());
        List<InputData> inputDatas = rdWr.readInputDataList(csvPath);
        int size = inputDatas.size() - 1;
        int delay = 1;

        NetEntreprisesBot bot = new NetEntreprisesBot();
        currentBot = bot;
        if (!printPeriodDates(bot)) {
            writeToTextArea("Merci d'indiquer une période pour la récupération");
            LOGGER.info("STOPPING : no date entered by user");
            return;
        }

        CsvReaderWriter writer = new CsvReaderWriter();
        String date = UtilsMeth.getDay() + "-"
                + UtilsMeth.getMonth() + "-" + UtilsMeth.getYear() + "-"
                + UtilsMeth.getHour() + "H-" + UtilsMeth.getMinute()
                + "M-" + UtilsMeth.getSecond() + "-s";
        String folder = outputFolderPath + "/{DATE}/";
        folder = folder.replace("{DATE}", date);
        outputFolderPath = folder;
        String successName = folder + "SIRET-réussis"/* + time().replace(":", "-")*/ + ".csv";
        String failedName = folder + "SIRET-échoués" /*+ time().replace(":", "-") */ + ".csv";
        new File(folder).mkdirs();
        if (!inputDatas.isEmpty()) {
            writer.writeHeaderLine(inputDatas.get(0).getHeaderLine(), successName);
            writer.writeHeaderLine(inputDatas.get(0).getHeaderLine(), failedName);
        }

        if (Constants.IS_DEMO_LINKEDIN && !Constants.IS_DATE_BLOCKED_DEMO) {
            try {
                if (Jsoup.connect("http://licence-simplicia.pbconseil.ovh/linkedin/" + getId(LOGGER) + ".html").execute().statusCode() != 200) {
                    writeToTextArea("DEMO INACTIVE");
                    return;
                }
            } catch (IOException e) {
                writeToTextArea("DEMO INACTIVE");
                return;
            }
            File f = new File(Constants.LINKEDIN_DEMO_FILE);
            if (f.exists() && !f.isDirectory()) {
                writeToTextArea("DEMO ACTIVE");
            } else {
                writeToTextArea("FICHIER DEMO MANQUANT");
                return;
            }
        }

        if (Constants.IS_DATE_BLOCKED_DEMO && !UtilsMeth.isValidDemo(Constants.DAY_DATE_BLOCKED_DEMO, Constants.MONTH_DATE_BLOCKED_DEMO - 1, Constants.YEAR_DATE_BLOCKED_DEMO, LOGGER)) {
            writeToTextArea("DEMO INACTIVE -- ECHEANCE = " + Constants.DAY_DATE_BLOCKED_DEMO + "/" + Constants.MONTH_DATE_BLOCKED_DEMO + "/" + Constants.YEAR_DATE_BLOCKED_DEMO);
            return;
        }

        timerCour = 0;
        long averageTime = 0;
        fileToDownload = 0;
        etatSiret = initEtatSiret(10);
        for (int k = 1; k < inputDatas.size(); k++) {
            if (Constants.LIMITED_RUN) {
                if (k > Constants.MAX_LOGINS_TO_PROCESS) {
                    continue;
                }
            }
            LOGGER.info("INPUTDATA SIZE " + inputDatas.size());
            InputData inputData = inputDatas.get(k);
            if (FLAG_1) {
                break;
            }
            boolean isOpened = bot.openPhantomJs();
            if (!isOpened) {
                writeToTextArea("Erreur à l'ouverture de PhantomJS - assurez-vous que le fichier soit dans le même dossier que Simplicia.");
                break;
            }
            String companyID = inputData.getCompanyID();
            if (companyID.length() != 14) {
                writeToTextArea("[" + companyID + "] " + "SIRET DE TAILLE INCORRECTE : IGNORE");
                String status = UtilsRapportRun.getTypeCompte(0, false);
                UtilsAPI.setLoginStatus(companyID, status, LOGGER);

                continue;
            }

            writeToTextArea("                             ***");
            writeToTextArea("[" + companyID + "] " + "Connexion en cours");
            Stopwatch timer = new Stopwatch().start();
            int login = bot.login(inputData);
            int isSuc = scImpl(login, bot, companyID, inputData, txtNom, txtPrenom, txtNss);
            timer.stop();
            timerCour = timerCour + timer.elapsed(TimeUnit.MINUTES);
            writeToTextArea("[" + companyID + "] " + "Durée du run : " + timer.elapsed(TimeUnit.SECONDS) + "sec");
            inputData.setStatusLogin(isSuc);

            if (isSuc == 7) {
                LOGGER.info("[" + companyID + "] " + "LOGIN MARQUE à OK");
                String status = UtilsRapportRun.getTypeCompte(isSuc, false);
                UtilsAPI.setLoginStatus(companyID, status, LOGGER);

                writeToTextArea("[" + companyID + "] " + "Pause de " + delay + " secondes . . .");
                try {
                    TimeUnit.SECONDS.sleep(delay);
                } catch (InterruptedException ignored) {
                }
            } else {
                if (isSuc >= 0 && isSuc <= 6) {
                    String status = UtilsRapportRun.getTypeCompte(isSuc, false);
                    UtilsAPI.setLoginStatus(companyID, status, LOGGER);

                    writeToTextArea("[" + companyID + "] " + "Pause de " + delay + " secondes . . .");
                    try {
                        TimeUnit.SECONDS.sleep(delay);
                    } catch (InterruptedException ignored) {
                    }
                }
            }

            etatSiret.get(isSuc).add(companyID);
            bot.closePhantomJsBr();
        }

        writeToTextArea(UtilsRapportRun.writeRappportRun(this.etatSiret, timerCour, averageTime, 9));
        writeToTextArea("Fin - " + time());
        /*
        try {
            String toPrint = UtilsMeth.readFile("LOG/log_net_entreprises.log");
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
            LocalDateTime now = LocalDateTime.now();
            if(Constants.IS_API_VERSION && this.etatSiret.size()>0) {
                UtilsAPI.submitExtractionLogToDB(dtf.format(now), toPrint, size - etatSiret.get(7).size(), etatSiret.get(7).size(), "BPIJ", LOGGER);
            }
            dtf = DateTimeFormatter.ofPattern("dd-MM-yyyy");
            UtilsMeth.writeFile("LOG/log_net_entreprises-" + dtf.format(now) + ".txt", toPrint);
        } catch (IOException e) {
            LOGGER.error(e);
        }*/
    }

    /*
      renvoie 7 si le login a réussi et le download s'est bien passé.
      renvoie 6 si le login a réussi et pas assez de fichiers downloadés
      renvoie 5 si le login a réussi et pas assez de fichiers downloadés
      renvoie 4 si le login a réussi et pas assez de fichiers downloadés
      renvoie 3 si le compte n'avait pas de BPIJ
      renvoie 2 si erreur login
      renvoie 1 si le compte n'a pas accès à BPIJ
      renvoie 0 si erreur dans le parcours jusqu'au BPIJ
      renvoie 8 si erreur net-e : nous ne pouvons répondre à votre demande
      renvoie 9 si compte bloqué 3 échecs mot de passe
      */
    private int scImpl(int login, NetEntreprisesBot bot, String companyID, InputData inputData, String txtNom, String txtPrenom, String txtNss) {
        int waitTimeMin = 1;
        int maxAttempts = 1;
        int success = 0;
        fileToDownload = 0;
        while (success != 7 && maxAttempts <= 2) {
            maxAttempts++;
            if (login == 1) {
                writeToTextArea("[" + companyID + "][" + maxAttempts + "]" + "Connexion réussie !");
                writeToTextArea("[" + companyID + "] " + "Accès en cours au compte BPIJ...");
                if (bot.passwordWillExpire()) {
                    writeToTextArea("[" + companyID + "] " + "Le mot de passe expire bientôt");
                }

                DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
                LocalDateTime now = LocalDateTime.now();
                LocalDateTime todayDate = LocalDateTime.of(now.getYear(), now.getMonthValue(), now.getDayOfMonth(), 0, 0);

                int passeCountCouloirDuTemps = 1;
                int nbAnneesCouloirDuTemps = 10;
                int couloirDuTempsNbPasses = UtilsMeth.hasInputDateUserCLI(dateStart, dateEnd) ? 1 : 10;
                LocalDateTime endDate = LocalDateTime.of(now.getYear(), now.getMonthValue(), now.getDayOfMonth(), 0, 0);
                LocalDateTime begDate = endDate.minusDays(45);
                if (UtilsMeth.hasInputDateUserCLI(dateStart, dateEnd)) {
                    begDate = LocalDateTime.parse(dateStart, DateTimeFormatter.ofPattern("dd/MM/yyyy"));
                    endDate = LocalDateTime.parse(dateEnd, DateTimeFormatter.ofPattern("dd/MM/yyyy"));
                } else {
                    writeToTextArea("Pas de période sélectionnée --> arrêt");
                    continue;
                }

                while (begDate.isAfter(todayDate.minusDays(nbAnneesCouloirDuTemps * 365)) && passeCountCouloirDuTemps <= couloirDuTempsNbPasses) {
                    LOGGER.info("Etape Boucle " + passeCountCouloirDuTemps++);
                    writeToTextArea("[" + companyID + "] " + "Nom=\"" + txtNom + "\" | Prenom=\"" + txtPrenom + "\" | Nss=\"" + txtNss + "\"");
                    String resultGoToSearchPage = bot.goToSearchPageCLI(companyID, inputData, begDate, endDate, txtNom, txtPrenom, txtNss);
                    String resultGoToDownloadPage;
                    if (resultGoToSearchPage.startsWith("[3bis] accès OK à la page de recherche")) {
                        resultGoToDownloadPage = bot.goToDownloadPage(companyID, inputData);
                    } else {
                        resultGoToDownloadPage = resultGoToSearchPage;
                    }

                    String outputParPeriodeFolderPathCompany = outputFolderPath
                            + companyID
                            + UtilsMeth.cleanPrintDates(bot.getBeginningDate(), bot.getEndDate())
                            + UtilsMeth.cleanPrintEmployeeData(inputData.getEmployeeCustomName())
                            + "/par-periode/"
                            + passeCountCouloirDuTemps
                            + "--"
                            + dtf.format(begDate).replace("/", "-")
                            + "--"
                            + dtf.format(endDate).replace("/", "-");
                    new File(outputParPeriodeFolderPathCompany).mkdirs();
                    HashMap<String, String> hash;
                    LOGGER.info("par salarie ? " + bot.parSalaries(inputData));
                    if (bot.parSalaries(inputData)) {
                        hash = bot.getPdfUrlsType2(companyID, Constants.BPIJ_FILE_FORMAT);
                    } else {
                        hash = bot.getPdfUrlsType1(companyID, Constants.BPIJ_FILE_FORMAT);
                    }
                    int nbBPIJ = checkIfBpijToDownload(resultGoToDownloadPage) ? hash.size() : 0;
                    String jpegScreenshotPath = outputParPeriodeFolderPathCompany
                            + "/" + "preuve-" + nbBPIJ + "-BPIJ-trouves-periode---"
                            + dtf.format(begDate).replace("/", "-")
                            + "--"
                            + dtf.format(endDate).replace("/", "-");
                    try {
                        BufferedImage joinedScreenshot = UtilsMeth.joinBufferedImage(
                                ImageIO.read(new File("LOG/preSearch-" + companyID + ".jpg")),
                                ImageIO.read(new File("LOG/resultsPage-" + companyID + ".jpg")));
                        ImageIO.write(joinedScreenshot, "jpg", new File("LOG/concat-" + companyID + ".jpg"));
                        ImageIO.write(joinedScreenshot, "jpg", new File(jpegScreenshotPath));

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    if (nbBPIJ > 0) {
                        UtilsAPI.submitBpijCountProofToDB(companyID, dtf.format(begDate), dtf.format(endDate), Integer.toString(nbBPIJ), "LOG/concat-" + companyID + ".jpg", LOGGER);
                    }
                    //On quitte NetE (si besoin) et on informe à l'écran
                    success = assignSuccessValueAndLeaveNetEntreprises(companyID, resultGoToDownloadPage, bot, dtf, begDate, endDate);
                    if (checkIfBpijToDownload(resultGoToDownloadPage)) {

                        Set<String> filesToDownloadUrls = hash.keySet();
                        writeToTextArea("[" + companyID + "] " + "BPIJ trouvés : " + filesToDownloadUrls.size() + " | " + bot.getBeginningDate() + "--" + bot.getEndDate());
                        String cookies = bot.getCookies();
                        String companyFolder = companyID;
                        if (inputData.getEmployeeCustomName() != null) {
                            companyFolder = inputData.getEmployeeCustomName();
                        }

                        for (String filesToDownloadUrl : filesToDownloadUrls) {
                            // Obtain a number between [0 - 49].
                            String idBPIJ = "";
                            String[] filesToDownloadUrlTab = filesToDownloadUrl.split("identifiant=");
                            if (filesToDownloadUrlTab.length >= 2) {
                                idBPIJ = filesToDownloadUrlTab[1];
                            }
                            String fileName = idBPIJ + "---" + hash.get(filesToDownloadUrl)  /*getRandomHexString(30) +*/;
                            String outputFolderPathCompany = outputFolderPath
                                    + companyFolder
                                    + UtilsMeth.cleanPrintDates(bot.getBeginningDate(), bot.getEndDate())
                                    + UtilsMeth.cleanPrintEmployeeData(inputData.getEmployeeCustomName());/*
                                    + "/par-cpam/" + UtilsMeth.getCpamFolder(fileName);*/

                            new File(outputFolderPathCompany).mkdirs();
                            LOGGER.info(" currentURL =" + bot.getDriver().getCurrentUrl());
                            LOGGER.info(" downnloadURL =" + "https://dsij-bpij.net-entreprises.ameli.fr/IhmHF/" + filesToDownloadUrl);
                            String fileToDownloadPath = outputFolderPathCompany
                                    + "/" + fileName;
                            boolean isDownloaded = UtilsMeth.downloadFile("https://dsij-bpij.net-entreprises.ameli.fr/IhmHF/" + filesToDownloadUrl,
                                    cookies,
                                    fileToDownloadPath);

                            String[] bpijDetailsTab = hash.get(filesToDownloadUrl).split("---");
                            String codeCpam = UtilsMeth.getCodeCPAM(bpijDetailsTab[0]);
                            if (codeCpam == null) {
                                LOGGER.info("[WARNING!!] CODE CPAM IS NULL");
                            }

                            String msgTxt = "[ERREUR] Erreur Téléchargement Fichier - " + fileName;
                            if (isDownloaded) {
                                fileToDownload++;
                                msgTxt = "Téléchargement de fichier réussi - " + fileName;
                            }
                            writeToTextArea("[" + companyID + "] " + msgTxt);
                        }
                        success = 7;
                        if (fileToDownload != filesToDownloadUrls.size()) {
                            success = 4;
                        }

                        bot.clickOnPassedAnchor("Quitter");
                        bot.leaveNetEntreprises();

                    } else {
                        if (maxAttempts >= 2) {
                            writeToTextArea("[" + companyID + "][" + maxAttempts + "]" + "Reconnexion en cours");
                        }
                        login = bot.login(inputData);
                    }
                    endDate = endDate.minusDays(8);
                    begDate = endDate.minusDays(7);
                }
            } else {
                if (login == -1) {
                    success = 2;
                    writeToTextArea("[" + companyID + "] " + " Connexion échouée - accès incorrects!");
                    login = bot.login(inputData);
                    writeToTextArea("[" + companyID + "][" + maxAttempts + "]" + "Connexion en cours");
                }
                if (login == 0) {
                    success = 8;
                    writeToTextArea("[" + companyID + "] " + " Erreur Net-Entreprises -- Nous ne sommes pas en mesure de répondre à votre demande.");
                    writeToTextArea("[" + companyID + "] " + "Pause " + waitTimeMin + "min puis nouvelle tentative");
                    try {
                        Thread.sleep(waitTimeMin * 60 * 1000);
                    } catch (InterruptedException e) {
                        LOGGER.info("ERREUR DE SLEEP");
                    }
                    login = bot.login(inputData);
                    writeToTextArea("[" + companyID + "][" + maxAttempts + "]" + "Connexion en cours");
                }
                if (login == 9) {
                    success = -2;
                    writeToTextArea("[" + companyID + "] " + " Echec 3 fois sur le mot de passe : blocage du compte 5min");
                    login = bot.login(inputData);
                    writeToTextArea("[" + companyID + "][" + maxAttempts + "]" + "Connexion en cours");
                }
            }
        }

        return success;
    }

    private void exitCommandLine() {
        if (currentBot != null) {
            currentBot.leaveNetEntreprises();
        }
        if (thr1 != null) {
            thr1.shutdownNow();
        }
        FLAG_1 = true;
        System.exit(0);
    }

    private License licenseValuesRun() {
        if(Constants.IS_API_VERSION){
            return License.ACTIVE;
        }
        if (Constants.IS_DEMO_LINKEDIN | Constants.IS_DATE_BLOCKED_DEMO) {
            return License.ACTIVE;
        }

        List<Integer> licenseValues = UtilsMeth.getLicenseValues(UtilsMeth.getLicenseUrlFromFile(Constants.CONFIG_FILE));
        if (licenseValues.size() >= 2) {
            Integer x = licenseValues.get(0);

            if (x >= 1) {
                writeToTextArea("SIMPLICIA BPIJ");
                writeToTextArea("Licence activée");
                return License.ACTIVE;
            } else {
                writeToTextArea("Licence désactivée");
                return License.DESACTIVE;
            }
        }

        writeToTextArea("Erreur critique : fichier de licence manquant ?");
        return License.UNKNOWN;
    }

    private void writeToTextArea(final String message) {
        LOGGER.info("### GUI ###  " + message);
    }

    private int assignSuccessValueAndLeaveNetEntreprises(String companyID, String resultGoToDownloadPage, NetEntreprisesBot bot, DateTimeFormatter dtf, LocalDateTime begDate, LocalDateTime endDate) {
        boolean noBPIJModule = resultGoToDownloadPage.startsWith("[1]");
        boolean notPassedBordereauxLink = resultGoToDownloadPage.startsWith("[2]");
        boolean notPassedRechercheBPIJ = resultGoToDownloadPage.startsWith("[3]");
        boolean notPassedSelectAllOK = resultGoToDownloadPage.startsWith("[4]");
        boolean tooMuchBPIJ = resultGoToDownloadPage.startsWith("[5]");
        boolean pleaseEnterASiret = resultGoToDownloadPage.startsWith("[6]");
        boolean noBPIJFound = resultGoToDownloadPage.startsWith("[7]");

        int success = 0;
        if (noBPIJModule) {
            writeToTextArea("[" + companyID + "] " + resultGoToDownloadPage + " | " + dtf.format(begDate) + "--" + dtf.format(endDate));
            bot.clickOnPassedAnchor("Quitter");
            bot.leaveNetEntreprises();
            success = 1;
        }
        if (notPassedBordereauxLink | notPassedRechercheBPIJ) {
            writeToTextArea("[" + companyID + "] " + resultGoToDownloadPage + " | " + dtf.format(begDate) + "--" + dtf.format(endDate));
            bot.clickOnPassedAnchor("Quitter");
            bot.leaveNetEntreprises();
            success = 0;
            LOGGER.info("on passe là " + notPassedSelectAllOK + " -- " + success);
        }
        if (noBPIJFound) {
            writeToTextArea("[" + companyID + "] " + resultGoToDownloadPage + " | " + dtf.format(begDate) + "--" + dtf.format(endDate));
            bot.clickOnPassedAnchor("Quitter");
            bot.leaveNetEntreprises();
            success = 3;
        }
        if (tooMuchBPIJ) {
            writeToTextArea("[" + companyID + "] " + resultGoToDownloadPage + " | " + dtf.format(begDate) + "--" + dtf.format(endDate));
            bot.clickOnPassedAnchor("Quitter");
            bot.leaveNetEntreprises();
            success = 5;
        }
        if (pleaseEnterASiret) {
            writeToTextArea("[" + companyID + "] " + resultGoToDownloadPage + " | " + dtf.format(begDate) + "--" + dtf.format(endDate));
            bot.clickOnPassedAnchor("Quitter");
            bot.leaveNetEntreprises();
            success = 6;
        }

        return success;
    }

    private boolean printPeriodDates(NetEntreprisesBot bot) {
        if (getBeginningPeriodDate(bot).length() == 0 | getEndDate(bot).length() == 0) {
            return false;
        }

        writeToTextArea("Période de récupération des BPIJ : " + getBeginningPeriodDate(bot) + " -- " + getEndDate(bot));
        return true;
    }

    private String getBeginningPeriodDate(NetEntreprisesBot bot) {
        if (dateStart == null) {
            return "";
        }

        if (dateStart.length() > 0 && dateEnd.length() > 0) {
            return dateStart;
        }

        return bot.getBeginningDate();
    }

    private String getEndDate(NetEntreprisesBot bot) {
        if (dateEnd == null) {
            return "";
        }

        if (dateStart.length() > 0 && dateEnd.length() > 0) {
            return dateEnd;
        }

        return bot.getEndDate();
    }

    private boolean checkIfBpijToDownload(String resultGoToDownloadPage) {
        boolean hasBPIJ = !resultGoToDownloadPage.startsWith("[1]");
        boolean passedBordereauxLink = !resultGoToDownloadPage.startsWith("[2]");
        boolean passedRechercheBPIJ = !resultGoToDownloadPage.startsWith("[3]");
        boolean passedSelectAllOK = !resultGoToDownloadPage.startsWith("[4]");
        boolean tooMuchBPIJ = resultGoToDownloadPage.startsWith("[5]");
        boolean pleaseEnterASiret = resultGoToDownloadPage.startsWith("[6]");
        boolean noBPIJFound = resultGoToDownloadPage.startsWith("[7]");
        boolean hasBPIJToDownload = hasBPIJ
                && passedBordereauxLink
                && passedRechercheBPIJ
                && !tooMuchBPIJ
                && !pleaseEnterASiret
                && !noBPIJFound;
        LOGGER.info("BPIJ TO DOWNLOAD ? " + hasBPIJToDownload);
        return hasBPIJToDownload;
    }

    private String time() {
        return new SimpleDateFormat("HH:mm:ss").format(Calendar.getInstance().getTime());
    }
}

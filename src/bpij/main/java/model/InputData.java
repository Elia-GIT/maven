package bpij.main.java.model;

public class InputData {
    private String companyCustomName;
    private String headerLine;
    private String companyID;
    private String lastname;
    private String firstname;
    private String password;
    private String allLine;
    private String employeeCustomName;
    private int statusLogin = -1;

    public InputData(String headerLine) {
        this.headerLine = headerLine;
    }

    public InputData(String companyID, String lastname, String firstname, String password, String allLine, String companyCustomName, String employeeCustomName) {
        this.companyID = companyID;
        this.lastname = lastname;
        this.firstname = firstname;
        this.password = password;
        this.allLine = allLine;
        this.companyCustomName = companyCustomName;
        this.employeeCustomName = employeeCustomName;
    }

    public String getEmployeeCustomName() {
        return employeeCustomName;
    }

    public String getCompanyCustomName() {
        return companyCustomName;
    }

    public String getCompanyID() {
        return companyID;
    }

    public String getLastname() {
        return lastname;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getPassword() {
        return password;
    }

    public String getHeaderLine() {
        return headerLine;
    }

    public int getStatusLogin() {
        return statusLogin;
    }

    public void setStatusLogin(int statusLogin) {
        this.statusLogin = statusLogin;
    }
}

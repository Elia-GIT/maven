package bpij.main.java.bot;

import bpij.main.java.model.InputData;
import bpij.main.java.utils.Constants;
import bpij.main.java.utils.UtilsMeth;
import javafx.scene.control.TextField;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.*;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class NetEntreprisesBot {
    private static final Logger LOGGER = Logger.getLogger(NetEntreprisesBot.class);
    private static final String typeRecherche = "typeRecherche1";
    private static final String paramsFile = "params/params.txt";
    private static LocalDateTime beginningDate;
    private static LocalDateTime endDate;
    private WebDriver driver;

    public NetEntreprisesBot() {
        if (Constants.IS_COMMAND_LINE_VERSION) {
            return;
        }

        File file = new File(paramsFile);
        try (FileReader reader = new FileReader(file)) {
            char[] chars = new char[(int) file.length()];
            reader.read(chars);

            String[] lines = new String(chars).split("\\r?\\n");
            for (int k = 0; k < lines.length; k++) {
                if (k == 1) {
                    String[] currLineTab = lines[k].split(";");
                    if (currLineTab.length == 2) {
                        String[] beginningDateTab = currLineTab[0].split("/");
                        String[] endDateTab = currLineTab[1].split("/");

                        beginningDate = LocalDateTime.of(
                                Integer.parseInt(beginningDateTab[2]),
                                Integer.parseInt(beginningDateTab[1]),
                                Integer.parseInt(beginningDateTab[0]), 0, 0);
                        endDate = LocalDateTime.of(
                                Integer.parseInt(endDateTab[2]),
                                Integer.parseInt(endDateTab[1]),
                                Integer.parseInt(endDateTab[0]), 0, 0);
                    }
                }
            }

            LOGGER.info("BEGIN DATE : " + beginningDate);
            LOGGER.info("END DATE : " + endDate);
        } catch (Exception e) {
            LOGGER.error("[READ-PARAMS-FILE-ERROR]");
        }
    }

    public WebDriver getDriver() {
        return driver;
    }

    public boolean parSalaries(InputData inputData) {
        return inputData.getEmployeeCustomName() != null;
    }

    public boolean openPhantomJs() {
        try {
            Proxy p = new Proxy();
            DesiredCapabilities cap = new DesiredCapabilities();
            cap.setCapability(CapabilityType.PROXY, p);
            System.setProperty("phantomjs.binary.path", Constants.PHANTOM_JS_PATH);

            driver = new PhantomJSDriver();
            driver.manage().window().maximize();

            return true;
        } catch (Exception e) {
            LOGGER.error("error with opening phantom js ", e);
        }
        return false;
    }

    public void closePhantomJsBr() {
        if (driver != null) {
            driver.close();
            driver.quit();
        }
    }

    public HashMap<String, String> getPdfUrlsType1(String companyID, String type) {
        LOGGER.info("url when listing BPIJ = " + driver.getCurrentUrl());
        List<String> pdfUrls = new ArrayList<>();
        HashMap<String, String> h = new HashMap<>();

        try {
            Document parsedDoc = Jsoup.parse(driver.getPageSource());
            Elements headerRow = parsedDoc.select("th.cell.triable.tri");
            int hasSIRENColumn = 0;
            if (headerRow.size() > 0) {
                Element titleRow = headerRow.get(0);
                if (titleRow.text().contains("SIREN/SIRET")) {
                    hasSIRENColumn = 1;
                }
            }

            Elements rows = parsedDoc.select("#tabPaiements tbody tr");
            for (Element row : rows) {
                String pdfUrl = row.getElementsByAttributeValueContaining("href", type).attr("href");
                pdfUrls.add(pdfUrl);
                int indexSiren = 1;
                int indexCaisseEmettrice = hasSIRENColumn + 1;
                int indexDate = hasSIRENColumn + 2;
                int indexMontant = hasSIRENColumn + 3;
                String siren = "";
                String caisseEmettrice = UtilsMeth.stripAccents(row.select("td:nth-child(" + indexCaisseEmettrice + ")")
                        .text().replace("'", "-")
                        .replace("-", "-")
                        .replace(" ", "-")
                        .toLowerCase());
                String date = row.select("td:nth-child(" + indexDate + ")").text().replace("/", "-");
                String montant = row.select("td:nth-child(" + indexMontant + ")").text().replace(",", "-").replace("€", "euros");
                if (hasSIRENColumn == 1) {
                    siren = row.select("td:nth-child(" + indexSiren + ")").text().replace("/", "-").replace(" ", "");
                }

                String extension = type.contains("xml") ? ".xml" : ".csv";
                if (hasSIRENColumn == 1) {
                    h.put(pdfUrl, caisseEmettrice + "---" + date + "---" + siren + "---" + montant + extension);
                } else {
                    h.put(pdfUrl, caisseEmettrice + "---" + date + "---" + montant + extension);
                }

            }
        } catch (Exception e) {
            LOGGER.error("ERROR - getPdfUrlsType1. companyID: " + companyID, e);
        }
        LOGGER.info("BPIJ trouvés : " + h.size());

        return h;
    }

    public HashMap<String, String> getPdfUrlsType2(String companyID, String type) {
        List<String> pdfUrls = new ArrayList<>();
        HashMap<String, String> h = new HashMap<>();
        try {
            Document parsedDoc = Jsoup.parse(driver.getPageSource());
            Elements headerRow = parsedDoc.select("th.cell.triable.tri");
            int decalageIndex = 0;
            if (headerRow.size() > 0) {
                Element titleRow = parsedDoc.select("th.cell.triable.tri").get(0);
                if (titleRow.text().contains("SIREN/SIRET")) {
                    decalageIndex = 1;
                }
            }
            Elements rows = parsedDoc.select("#paiementSalarietab tbody tr");
            for (Element row : rows) {
                String pdfUrl = row.getElementsByAttributeValueContaining("href", type).attr("href");
                pdfUrls.add(pdfUrl);

                int indexDate = decalageIndex + 2;
                int indexNns = decalageIndex + 3;
                int indexMontant = decalageIndex + 5;
                int indexNom = 4;
                if (decalageIndex == 1) {
                    indexNom = indexMontant - 1;
                }
                String nom = row.select("td:nth-child(" + indexNom + ")").text().replace(" ", "-");
                String date = row.select("td:nth-child(" + indexDate + ")").text().replace("/", "-");
                String nns = row.select("td:nth-child(" + indexNns + ")").text();
                String montant = row.select("td:nth-child(" + indexMontant + ")").text().replace(",", "-").replace("€", "euros");
                h.put(pdfUrl, date + "---" + nom + "---" + nns + "---" + montant + ".csv");
            }
        } catch (Exception e) {
            LOGGER.error("ERROR - getPdfUrlsType2. companyID: " + companyID, e);
        }

        LOGGER.info("BPIJ trouvés : " + h.size());

        return h;
    }

    public String goToSearchPage(String companyID, InputData inputData, LocalDateTime dateDebut, LocalDateTime dateFin, TextField txtNom, TextField txtPrenom, TextField txtNns) {
        if (dateDebut != null && dateFin != null) {
            beginningDate = dateDebut;
            endDate = dateFin;
        }
        boolean hasBPIJ = clickOnAttestation(companyID);
        if (!hasBPIJ) {
            takeScreenshot("LOG/preSearch-" + companyID/*+"-"+UtilsMeth.getRandomHexString(10)*/);
            takeScreenshot("LOG/resultsPage-" + companyID/*+"-"+UtilsMeth.getRandomHexString(10)*/);
            return "[1][Erreur] : Ce compte n'a pas d'accès à BPIJ";
        }
        boolean okAnchor = clickOnAnchor();
        LOGGER.info("step 1 OK");
        LOGGER.info(driver.getCurrentUrl());
        selectCheckBoxIfNotSelected(companyID, inputData);
        LOGGER.info("step 2 OK");
        LOGGER.info(driver.getCurrentUrl());
        setDates(dateDebut, dateFin);
        LOGGER.info("step 3 OK");
        LOGGER.info("pre search=" + driver.getCurrentUrl());
        setCustomEmployeeNameOrNNSIfNeeded(companyID, txtNom, txtPrenom, txtNns);
        takeScreenshot("LOG/preSearch-" + companyID/*+"-"+UtilsMeth.getRandomHexString(10)*/);
        if (!okAnchor) {
            takeScreenshot("LOG/resultsPage-" + companyID/*+"-"+UtilsMeth.getRandomHexString(10)*/);
            return "[2][Erreur] : impossible d'accéder aux bordereaux de paiement des IJ";
        } else {
            takeScreenshot("LOG/resultsPage-" + companyID/*+"-"+UtilsMeth.getRandomHexString(10)*/);
            return "[3bis] accès OK à la page de recherche";
        }
    }

    public String goToSearchPageCLI(String companyID, InputData inputData, LocalDateTime dateDebut, LocalDateTime dateFin, String txtNom, String txtPrenom, String txtNns) {
        if (dateDebut != null && dateFin != null) {
            beginningDate = dateDebut;
            endDate = dateFin;
        }
        boolean hasBPIJ = clickOnAttestation(companyID);
        if (!hasBPIJ) {
            takeScreenshot("LOG/preSearch-" + companyID/*+"-"+UtilsMeth.getRandomHexString(10)*/);
            takeScreenshot("LOG/resultsPage-" + companyID/*+"-"+UtilsMeth.getRandomHexString(10)*/);
            return "[1][Erreur] : Ce compte n'a pas d'accès à BPIJ";
        }
        boolean okAnchor = clickOnAnchor();
        LOGGER.info("step 1 OK");
        LOGGER.info(driver.getCurrentUrl());
        selectCheckBoxIfNotSelected(companyID, inputData);
        LOGGER.info("step 2 OK");
        LOGGER.info(driver.getCurrentUrl());
        setDates(dateDebut, dateFin);
        LOGGER.info("step 3 OK");
        LOGGER.info("pre search=" + driver.getCurrentUrl());
        setCustomEmployeeNameOrNNSIfNeededCLI(companyID, txtNom, txtPrenom, txtNns);
        takeScreenshot("LOG/preSearch-" + companyID/*+"-"+UtilsMeth.getRandomHexString(10)*/);
        if (!okAnchor) {
            takeScreenshot("LOG/resultsPage-" + companyID/*+"-"+UtilsMeth.getRandomHexString(10)*/);
            return "[2][Erreur] : impossible d'accéder aux bordereaux de paiement des IJ";
        } else {
            takeScreenshot("LOG/resultsPage-" + companyID/*+"-"+UtilsMeth.getRandomHexString(10)*/);
            return "[3bis] accès OK à la page de recherche";
        }
    }

    public String goToDownloadPage(String companyID, InputData inputData) {
        boolean hasResults = clickOnSearch(companyID);
        LOGGER.info("post search=" + driver.getCurrentUrl());
        if (!hasResults) {
            takeScreenshot("LOG/resultsPage-" + companyID/*+"-"+UtilsMeth.getRandomHexString(10)*/);
            return "[3][Erreur] : accès impossible au listing BPIJ";
        }
        LOGGER.info("step 4 OK");
        LOGGER.info("pre select ALL=" + driver.getCurrentUrl());
        boolean selectAllOK = selectAll(companyID);
        takeScreenshot("LOG/resultsPage-" + companyID/*+"-"+UtilsMeth.getRandomHexString(10)*/);
        if (!selectAllOK) {
            if (driver.getPageSource().contains("Aucun résultat, modifiez votre filtre ou utilisez le bouton Rétablir pour réafficher le tableau.")) {
                LOGGER.info("[7][Warning] : 0 BPIJ trouvés sur la période");
                return "[7][Warning] : 0 BPIJ trouvés sur la période";
            }
            if (driver.getPageSource().contains("veuillez affiner votre ")) {
                LOGGER.info("Trop de BPIJ");
                return "[5]Le nombre de résultats est trop important, veuillez affiner votre recherche.";
            }
            if (driver.getPageSource().contains("La saisie du siret est obligatoire")) {
                LOGGER.info("La saisie du siret est obligatoire");
                return "[6]La saisie du siret est obligatoire";
            }
            LOGGER.info("[4][Warning] : Moins de 10 BPIJ trouvés");
            return "[4][Warning] : Moins de 10 BPIJ trouvés";
        }
        LOGGER.info("step 5 OK");
        return "Accès aux listing BPIJ réussi";
    }

    private boolean setCustomEmployeeNameOrNNSIfNeeded(String companyID, TextField txtNom, TextField txtPrenom, TextField txtNns) {
        try {
            LOGGER.info("currentURL " + driver.getCurrentUrl());
            WebElement firstNameElement = driver.findElement(By.id("prenom"));
            WebElement lastNameElement = driver.findElement(By.id("nom"));
            WebElement nnsElement = driver.findElement(By.id("nir"));
            nnsElement.sendKeys(txtNns.getText());
            firstNameElement.sendKeys(txtPrenom.getText());
            lastNameElement.sendKeys(txtNom.getText());
        } catch (Exception e) {
            LOGGER.error("ERROR - setCustomEmployeeName. companyID: " + companyID, e);
            return false;
        }
        return true;
    }

    private boolean setCustomEmployeeNameOrNNSIfNeededCLI(String companyID, String txtNom, String txtPrenom, String txtNns) {
        try {
            LOGGER.info("currentURL " + driver.getCurrentUrl());
            WebElement firstNameElement = driver.findElement(By.id("prenom"));
            WebElement lastNameElement = driver.findElement(By.id("nom"));
            WebElement nnsElement = driver.findElement(By.id("nir"));
            nnsElement.sendKeys(txtNns);
            firstNameElement.sendKeys(txtPrenom);
            lastNameElement.sendKeys(txtNom);
        } catch (Exception e) {
            LOGGER.error("ERROR - setCustomEmployeeName. companyID: " + companyID, e);
            return false;
        }
        return true;
    }

    private boolean selectAll(String companyID) {
        if (!driver.getPageSource().contains("Informations de paiement des IJ")) {
            return false;
        }
        try {
            WebElement selectAll = driver.findElement(By.tagName("select"));
            selectAll.click();
            TimeUnit.SECONDS.sleep(2);
            List<WebElement> options = driver.findElement(By.tagName("select")).findElements(By.tagName("option"));
            for (WebElement option : options) {
                String optTxt = option.getText();
                if (StringUtils.containsIgnoreCase(optTxt, "Toutes")) {
                    option.click();
                    TimeUnit.SECONDS.sleep(Constants.DELAY);
                    break;
                }
            }
        } catch (Exception e) {
            LOGGER.error("ERROR - selectAll. companyID: " + companyID, e);
            return false;
        }
        return true;
    }


    private boolean clickOnSearch(String companyID) {
        int maxAttempts = 0;
        String targetUrl = "https://dsij-bpij.net-entreprises.ameli.fr/IhmHF/afficherPaiement.go";
        try {
            driver.findElement(By.id("rechercher")).click();
            TimeUnit.SECONDS.sleep(Constants.DELAY + 2);
            while (!driver.getCurrentUrl().contains(targetUrl) && maxAttempts < 5) {
                driver.findElement(By.id("rechercher")).click();
                maxAttempts++;
            }
        } catch (Exception e) {
            LOGGER.error("ERROR - clickOnSearch. companyID: " + companyID, e);
        }
        return maxAttempts < 5;
    }

    private void selectCheckBoxIfNotSelected(String companyID, InputData inputData) {
        LOGGER.info("*** URL = " + driver.getCurrentUrl());
        try {
            if (inputData.getEmployeeCustomName() != null) {
                //recherche par employé
                WebElement checkbox = driver.findElement(By.id("typeRecherche2"));
                boolean selected = checkbox.isSelected();
                if (!selected) {
                    checkbox.click();
                    TimeUnit.SECONDS.sleep(2);
                }
            } else {
                //recherche par entreprises
                WebElement checkbox = driver.findElement(By.id(typeRecherche));
                boolean selected = checkbox.isSelected();
                if (!selected) {
                    checkbox.click();
                    TimeUnit.SECONDS.sleep(2);
                }
            }

        } catch (Exception e) {
            LOGGER.error("ERROR - selectCheckBoxIfNotSelected. companyID: " + companyID, e);
        }
    }

    private boolean clickOnAttestation(String companyID) {
        int count = 0;
        try {
            Document postLoginPage = Jsoup.parse(driver.getPageSource());
            String url = "https://portail.net-entreprises.fr/priv/" + postLoginPage.select("#service-3").attr("href");
            String targetUrl = "https://dsij-bpij.net-entreprises.ameli.fr/IhmHF/startGlobal.jsp";
            while (count < 5 && !driver.getCurrentUrl().contains(targetUrl)) {
                driver.get(url);
                TimeUnit.SECONDS.sleep(Constants.DELAY + 1);
                LOGGER.info("Tentative Connexion Attestation Salaire");
                count++;
            }

        } catch (Exception e) {
            LOGGER.error("ERROR - clickOnCompte. companyID: " + companyID, e);
        }
        return count < 5;
    }


    private boolean clickOnAnchor() {
        try {
            List<WebElement> menuEls = driver.findElements(By.tagName("a"));
            for (WebElement menuEl : menuEls) {
                if (StringUtils.containsIgnoreCase(menuEl.getText(), "Bordereaux de paiement des")) {
                    menuEl.click();
                    TimeUnit.SECONDS.sleep(Constants.DELAY + 2);
                    break;
                }
            }
        } catch (Exception e) {
            LOGGER.error("ERROR - clickOnAnchor.", e);
            return false;
        }
        return true;
    }


    public void clickOnPassedAnchor(String menuItemName) {
        try {
            driver.get("https://dsij-bpij.net-entreprises.ameli.fr/IhmHF/startGlobal.jsp");
        } catch (Exception e) {
            LOGGER.error("ERROR - leaving " + menuItemName, e);
        }
    }

    public int login(InputData inputData) {
        String companyID = inputData.getCompanyID();

        try {
            driver.get(Constants.LOGIN_URL);
            TimeUnit.SECONDS.sleep(Constants.DELAY);
            driver.findElement(By.className("header__login")).click();
            TimeUnit.SECONDS.sleep(Constants.DELAY);
            driver.findElement(By.id("j_siret")).sendKeys(inputData.getCompanyID());
            driver.findElement(By.id("j_nom")).sendKeys(inputData.getLastname());
            driver.findElement(By.id("j_prenom")).sendKeys(inputData.getFirstname());
            driver.findElement(By.id("j_password")).clear();
            driver.findElement(By.id("j_password")).sendKeys(inputData.getPassword());
            driver.findElement(By.id("validButtonConnexion")).click();
            TimeUnit.SECONDS.sleep(Constants.DELAY);
            String pageSource = driver.getPageSource();

            if (Jsoup.parse(pageSource).select(".slick-track").size() > 0
                    && pageSource.contains("Se déconnecter")) {
                LOGGER.info("Logged in successfully - " + companyID);
                return 1;
            }

            if (pageSource.contains("Nous ne sommes pas en mesure de répondre à votre demande.")) {
                return 0;
            }

            if (pageSource.contains("Vous avez échoué 3 fois consécutivement")) {
                return -2;
            }
        } catch (Exception e) {
            LOGGER.error("ERROR - login. companyID: " + companyID, e);
        }

        takeScreenshot("LOG/" + "failed_login_" + companyID);
        LOGGER.info("Failed to login - " + companyID);

        return -1;
    }

    public String getCookies() {
        StringBuilder builder = new StringBuilder();
        for (Cookie cookie : driver.manage().getCookies()) {
            builder.append(cookie.getName()).append("=").append(cookie.getValue()).append("; ");
        }

        return UtilsMeth.removeLastChar(builder.toString(), "; ");
    }

    private void takeScreenshot(String screenshotName) {
        try {
            FileUtils.copyFile(((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE), new File(screenshotName + ".jpg"), true);
        } catch (IOException e) {
            LOGGER.error("error ", e);
        }
    }

    public void leaveNetEntreprises() {
        try {
            if(driver!=null) {
                driver.get("https://dsij-bpij.net-entreprises.ameli.fr/IhmHF/startGlobal.jsp");
            }
            TimeUnit.SECONDS.sleep(Constants.DELAY + 1);
        } catch (Exception e) {
            LOGGER.error("ERROR - leaving net-entreprises", e);
        }
    }

    public String getBeginningDate() {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        LocalDateTime now = LocalDateTime.now();
        LocalDateTime beginningDate0 = LocalDateTime.of(now.getYear(), now.getMonth(), now.getDayOfMonth(), 0, 0).minusDays(10);
        if (beginningDate == null) {
            return dtf.format(beginningDate0);
        }

        if (Constants.IS_DATE_BLOCKED_DEMO) {
            dtf.format(beginningDate);

            if (beginningDate.getMonthValue() != 4 && beginningDate.getYear() != 2019) {
                beginningDate = beginningDate0;
            }
        }

        return dtf.format(beginningDate);
    }

    public String getEndDate() {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        LocalDateTime now = LocalDateTime.now();
        LocalDateTime endDate0 = LocalDateTime.of(now.getYear(), now.getMonth(), now.getDayOfMonth(), 0, 0);

        if (endDate == null) {
            return dtf.format(endDate0);
        }

        if (Constants.IS_DATE_BLOCKED_DEMO) {
            if (endDate.getMonthValue() != 4 && endDate.getYear() != 2019) {
                endDate = endDate0;
            }
        }

        return dtf.format(endDate);
    }

    private void setDates(LocalDateTime dateDebut, LocalDateTime dateFin) {
        String idDebut = "dateDebut";
        String idFin = "dateFin";

        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        LocalDateTime now = LocalDateTime.now();
        LocalDateTime todayDate = LocalDateTime.of(now.getYear(), now.getMonthValue(), now.getDayOfMonth(), 0, 0);
        LocalDateTime startingDate = todayDate.minusDays(30);
        LOGGER.info(dtf.format(startingDate));
        LOGGER.info(dtf.format(todayDate));

        if (dateDebut == null && dateFin == null) {
            try {
                if (getBeginningDate() != null && getEndDate() != null) {
                    LOGGER.info("[SETDATE] input utilisateur " + getBeginningDate() + "--" + getEndDate());
                    driver.findElement(By.id(idDebut)).sendKeys(getBeginningDate());
                    driver.findElement(By.id(idFin)).sendKeys(getEndDate());
                    TimeUnit.SECONDS.sleep(Constants.DELAY + 1);
                } else {
                    LOGGER.info("[SETDATE] " + startingDate + "--" + todayDate);
                    driver.findElement(By.id(idDebut)).sendKeys(dtf.format(startingDate));
                    driver.findElement(By.id(idFin)).sendKeys(dtf.format(todayDate));
                    TimeUnit.SECONDS.sleep(Constants.DELAY + 1);
                }
            } catch (Exception e) {
                LOGGER.error("ERROR - leaving net-entreprises", e);
            }
        } else {
            LOGGER.info("[SETDATE] input Couloir du temps " + dtf.format(dateDebut) + "--" + dtf.format(dateFin));
            driver.findElement(By.id(idDebut)).sendKeys(dtf.format(dateDebut));
            driver.findElement(By.id(idFin)).sendKeys(dtf.format(dateFin));
        }
    }

    public boolean passwordWillExpire() {
        return driver.getPageSource().contains("sa modification deviendra donc obligatoire le");
    }
}



package bpij.main.java;

import bpij.main.java.controller.ControllerCLI;
import bpij.main.java.utils.Constants;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.apache.log4j.BasicConfigurator;

import java.io.File;

/**
 * FAIRE un programCLI pour BPIJ aussi, à la manière de ATMP
 * Possibilities :
 * cli api : CLI API mode ;)
 * cli api debug : just shows api keys
 * cli input.csv outputfolder/
 */

public class Program extends Application {
    public static void main(final String[] args) {
        BasicConfigurator.configure();
        Constants.IS_COMMAND_LINE_VERSION = args.length > 0 && args[0].contains("cli");

        if (Constants.IS_COMMAND_LINE_VERSION) {
            Constants.IS_API_VERSION = args[1].contains("api");

            new File("./LOG/log_net_entreprises.log").delete();

            ControllerCLI controllerCLI = new ControllerCLI();
            if (!Constants.IS_API_VERSION) {
                controllerCLI.csvPath = args[1];
                controllerCLI.outputFolderPath = args[2];
                controllerCLI.txtNom = args[3];
                controllerCLI.txtPrenom = args[4];
                controllerCLI.txtNss = args[5];
                controllerCLI.dateStart = args[6];
                controllerCLI.dateEnd = args[7];
            }

            controllerCLI.startSc1();
        } else {
            launch(args);
        }
    }

    @Override
    public void start(final Stage primaryStage) throws Exception {
        primaryStage.setTitle("SIMPLICIA - BPIJ");
        primaryStage.setScene(new Scene(FXMLLoader.load(this.getClass().getResource("/newGui.fxml"))));
        primaryStage.setResizable(false);
        primaryStage.show();

        primaryStage.setOnCloseRequest(t -> {
            Platform.exit();
            System.exit(0);
        });
    }
}

package atmp.main.java;

import atmp.main.java.controller.ControllerCLI;
import atmp.main.java.utils.Constants;
import org.apache.log4j.BasicConfigurator;

import java.io.File;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class ProgramCLI {
    /**
     *
     * @param args
     * Possibilities :
     * cli api : CLI API mode ;)
     * cli api debug : just shows api keys
     * cli input.csv outputfolder/
     *
     */

    public static void main(String[] args) {
        BasicConfigurator.configure();
        if (args.length > 0) {
                Constants.PHANTOM_JS_PATH=System.getenv("PHANTOM_JS_PATH");
                Constants.IS_COMMAND_LINE_VERSION = args[0].contains("cli");

                if (Constants.IS_COMMAND_LINE_VERSION) {
                    Constants.IS_API_VERSION = args[1].contains("api");

                    if(Constants.IS_API_VERSION && Constants.IS_HARDCODED_API_KEYS){
                        Constants.API_KEY=Constants.HARDCODED_API_KEY;
                        Constants.API_SECRET=Constants.HARDCODED_API_SECRET;
                    }
                    new File("./LOG/log_net_entreprises.log").delete();

                    ControllerCLI controllerCLI = new ControllerCLI();
                    if (!Constants.IS_API_VERSION) {
                        controllerCLI.csvPath = args[1];
                        controllerCLI.outputFolderPath = args[2];
                    }
                    if(Constants.IS_API_VERSION && args.length==3 && args[2].contains("debug")){
                        System.out.println("[DEBUG MODE] SHOWING API KEY VALUES FOR DEBUG & STOPPING PROGRAM");
                        System.out.println("API_KEY="+Constants.API_KEY);
                        System.out.println("API_SECRET="+Constants.API_SECRET);
                        System.out.println("API_ADMIN_SUPER_KEY="+Constants.API_ADMIN_SUPER_KEY);
                        System.out.println("API_ADMIN_SUPER_SECRET="+Constants.API_ADMIN_SUPER_SECRET);
                        return;
                    }
                    controllerCLI.startSc1();
                }

        } else {
            System.out.println("\nplease input args : \n 0 : cli \n 1 : api OR csvFilePath \n 2 : outputPath or nothing \n 3 : debug or nothing");
        }
    }
    private static void ScheduleExtraction(int delay) {
        ZonedDateTime zonedNow = ZonedDateTime.of(LocalDateTime.now(), ZoneId.of("Europe/Paris"));
        ZonedDateTime zoneNextRun = delay == 1440 ? zonedNow.withHour(05).withMinute(00).withSecond(00) : zonedNow.plusMinutes(delay);

        if (zonedNow.compareTo(zoneNextRun) > 0) {
            zoneNextRun = zoneNextRun.plusDays(1);
        }

        long initalDelay = Duration.between(zonedNow, zoneNextRun).toMinutes();
        System.out.println(delay + "--" + initalDelay + "\n" + zonedNow + "\n" + zoneNextRun);
        Executors.newScheduledThreadPool(1)
                .scheduleAtFixedRate(() -> {
                    new File("./LOG/log_net_entreprises.log").delete();
                    new ControllerCLI().startSc1();
                }, initalDelay, delay, TimeUnit.MINUTES);
    }
}

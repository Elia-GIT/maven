package atmp.main.java.model;

import atmp.main.java.csv.CSVConnecter;

import java.util.ArrayList;
import java.util.List;

import static atmp.main.java.csv.CSVConnecter.STATUS_NON;
import static atmp.main.java.csv.CSVConnecter.STATUS_OUI;

/**
 * Class for making Table (from .csv file with encryption)
 * Also can be used for another tables
 */

public class Table {
    private ArrayList<String> headers;
    private List<TableLine> lines;

    public Table(ArrayList<String> headers, List<TableLine> lines) {
        this.headers = headers;
        this.lines = lines;
    }

    public void addTableLine(TableLine tableLine) {
        lines.add(tableLine);
    }

    public void encrypt() {
        switch (headers.size()) {
            case 5:
                headers.add("cryptage");
                headers.add(STATUS_OUI);
                break;
            case 4:
                headers.add("societe");
                headers.add("cryptage");
                headers.add(STATUS_OUI);
            default:
                headers.set(6, STATUS_OUI);
        }

        for (TableLine line : lines) {
            line.encryptPassw();
        }
    }

    public void decrypt() {
        switch (headers.size()) {
            case 5:
                headers.add("cryptage");
                headers.add(STATUS_NON);
                return;
            case 4:
                headers.add("societe");
                headers.add("cryptage");
                headers.add(STATUS_NON);
            default:
                headers.set(6, STATUS_NON);
        }

        for (TableLine line : lines) {
            line.decryptPassw();
        }
    }

    public String toCSV() {
        StringBuilder str = new StringBuilder();
        str.append(CSVConnecter.makeSplittedStringFromValues(";", headers));

        for (TableLine line : lines) {
            str.append("\n" + line.toCSV());
        }

        return str.toString();
    }
}

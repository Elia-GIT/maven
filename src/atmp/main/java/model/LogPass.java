package atmp.main.java.model;

import atmp.main.java.csv.CSVConnecter;
import atmp.main.java.utils.UtilsMeth;

public class LogPass {

    private String login;
    private String password;
    private String encryptionStatus;
    private String allValsStr;

    public LogPass(String login, String password, String encryptionStatus) {
        this.login = login;
        this.password = password;
        this.encryptionStatus = encryptionStatus;

        remakeAllValsStr();
    }

    public void encrypt(){
        login = UtilsMeth.encrypt(login);
        password = UtilsMeth.encrypt(password);
        encryptionStatus = CSVConnecter.STATUS_OUI;

        remakeAllValsStr();
    }

    public void decrypt(){
        login = UtilsMeth.decrypt(login);
        password = UtilsMeth.decrypt(password);
        encryptionStatus = CSVConnecter.STATUS_NON;

        remakeAllValsStr();
    }

    private void remakeAllValsStr(){
        allValsStr = CSVConnecter.makeSplittedStringFromValues(";", login, password, encryptionStatus);
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public String getEncryptionStatus() {
        return encryptionStatus;
    }

    public String getAllValsStr() {
        return allValsStr;
    }

    public String toCSV(){
        return allValsStr;
    }
}

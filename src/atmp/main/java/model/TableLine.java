package atmp.main.java.model;

import atmp.main.java.csv.CSVConnecter;
import atmp.main.java.utils.UtilsMeth;

/**
 * Class for table line (table with encryption)
 * Also have universal string for any table (param 'allValues')
 */

public class TableLine {
    private String allValues;
    private String companyID;
    private String firstName;
    private String lastName;
    private String password;
    private String companyName;

    public TableLine(String allValues) {
        this.allValues = allValues;
        String[] vals = allValues.split(";");

        if (vals.length >= 4) {
            this.companyID = vals[0];
            this.firstName = vals[1];
            this.lastName = vals[2];
            this.password = vals[3];

            try {
                this.companyName = vals[4]; // can be null (when table was 4 col)
            } catch (ArrayIndexOutOfBoundsException ignored) {
            }
        }
    }

    private void remakeAllValStr() {
        if (companyName != null) {
            this.allValues = CSVConnecter.makeSplittedStringFromValues(";", companyID, firstName, lastName, password, companyName);
        } else {
            this.allValues = CSVConnecter.makeSplittedStringFromValues(";", companyID, firstName, lastName, password);
        }
    }

    void encryptPassw() {
        this.password = UtilsMeth.encrypt(this.password); // encrypt
        remakeAllValStr();
    }

    void decryptPassw() {
        this.password = UtilsMeth.decrypt(this.password); // decrypt
        remakeAllValStr();
    }

    String toCSV() {
        return allValues;
    }
}

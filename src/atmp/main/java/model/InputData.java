package atmp.main.java.model;

public class InputData {
    private String headerLine;
    private String col1;
    private String col2;
    private String companyID;
    private String lastname;
    private String firstname;
    private String password;
    private String companyCustomName;

    public String getCodeRisque() {
        return codeRisque;
    }

    private String codeRisque;

    public int getStatusLogin() {
        return statusLogin;
    }

    public void setStatusLogin(int statusLogin) {
        this.statusLogin = statusLogin;
    }

    private int statusLogin = -1;

    public String getCompanyCustomName() {
        return companyCustomName;
    }

    public InputData(String headerLine) {
        this.headerLine = headerLine;
    }

    public InputData(String companyID, String lastname, String firstname, String password, String allLine, String companyCustomName, String codeRisque) {
        this.companyID = companyID;
        this.lastname = lastname;
        this.firstname = firstname;
        this.password = password;
        this.companyCustomName = companyCustomName;
        this.codeRisque = codeRisque;
    }

    public String getCompanyID() {
        return companyID;
    }

    public String getLastname() {
        return lastname;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getPassword() {
        return password;
    }

    public String getHeaderLine() {
        return headerLine;
    }
}

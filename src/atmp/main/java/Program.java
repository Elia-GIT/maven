package atmp.main.java;

import atmp.main.java.controller.ControllerCLI;
import atmp.main.java.controller.LoginScreenController;
import atmp.main.java.utils.Constants;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.apache.log4j.BasicConfigurator;

import java.io.File;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class Program extends Application {
    public static final String csvLogPassFilePath = "params\\access.csv";

    //GUI = gui --
    //CLI inputFile = cli csv csvPath
    //CLI API  = cli api

    public static void main(String[] args) {
        BasicConfigurator.configure();

        if (args.length > 0) {
            if (args[0].contains("production")) {
                ScheduleExtraction(1440);
            } else {
                Constants.IS_COMMAND_LINE_VERSION = args[0].contains("cli");

                if (Constants.IS_COMMAND_LINE_VERSION) {
                    Constants.IS_API_VERSION = args[1].contains("api");

                    new File("./LOG/log_net_entreprises.log").delete();

                    ControllerCLI controllerCLI = new ControllerCLI();
                    if (!Constants.IS_API_VERSION) {
                        controllerCLI.csvPath = args[1];
                        controllerCLI.outputFolderPath = args[2];
                    }

                    controllerCLI.startSc1();
                }
            }
        } else {
            launch(args);
        }
    }

    private static void ScheduleExtraction(int delay) {
        ZonedDateTime zonedNow = ZonedDateTime.of(LocalDateTime.now(), ZoneId.of("Europe/Paris"));
        ZonedDateTime zoneNextRun = delay == 1440 ? zonedNow.withHour(05).withMinute(00).withSecond(00) : zonedNow.plusMinutes(delay);

        if (zonedNow.compareTo(zoneNextRun) > 0) {
            zoneNextRun = zoneNextRun.plusDays(1);
        }

        long initalDelay = Duration.between(zonedNow, zoneNextRun).toMinutes();
        System.out.println(delay + "--" + initalDelay + "\n" + zonedNow + "\n" + zoneNextRun);
        Executors.newScheduledThreadPool(1)
                .scheduleAtFixedRate(() -> {
                    new File("./LOG/log_net_entreprises.log").delete();
                    new ControllerCLI().startSc1();
                }, initalDelay, delay, TimeUnit.MINUTES);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent root;
        if (Constants.IS_DEMO_LINKEDIN | Constants.LOGIN_SCREEN_OFF) {
            root = new FXMLLoader(getClass().getResource("/GUI.fxml")).load();
        } else {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/login.fxml"));
            root = loader.load();
            LoginScreenController controller = loader.getController();
            controller.setPrimaryStage(primaryStage);
        }

        String title = Constants.IS_DEMO_LINKEDIN | Constants.LOGIN_SCREEN_OFF ? "SIMPLICIA - Version 5.3" : "SIMPLICIA - Version 5.3 Login";
        int width = Constants.IS_DEMO_LINKEDIN | Constants.LOGIN_SCREEN_OFF ? 666 : 273;
        int height = Constants.IS_DEMO_LINKEDIN | Constants.LOGIN_SCREEN_OFF ? 625 : 128;

        primaryStage.setTitle(title);
        primaryStage.setScene(new Scene(root, width, height));
        primaryStage.setResizable(false);
        primaryStage.show();
        primaryStage.setOnCloseRequest(t -> {
            Platform.exit();
            System.exit(0);
        });
    }
}

package atmp.main.java.bot;

import atmp.main.java.model.InputData;
import atmp.main.java.utils.Constants;
import atmp.main.java.utils.UtilsAPI;
import atmp.main.java.utils.UtilsMeth;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.openqa.selenium.*;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;

import javax.net.ssl.*;
import java.io.File;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

public class NetEntreprisesBotSelenium {

    private static final Logger LOGGER = Logger.getLogger(NetEntreprisesBotSelenium.class);
    private WebDriver driver;

    public boolean openPhantomJs() {
        try {
            System.setProperty("phantomjs.binary.path", Constants.PHANTOM_JS_PATH);
            driver = new PhantomJSDriver();
            driver.manage().window().maximize();
            return true;
        } catch (Exception e) {
            LOGGER.error("error with opening phantom js ", e);
        }

        return false;
    }

    public void quit() {
        try {
            driver.get("https://www.net-entreprises.fr");
            TimeUnit.SECONDS.sleep(Constants.DELAY + 1);
        } catch (Exception e) {
            LOGGER.error("ERROR - leaving net-entreprises", e);
        }
    }

    public void closePhantomJsBr() {

        if (driver != null) {
            if (((RemoteWebDriver) driver).getSessionId() != null) {
                driver.close();
                driver.quit();
            }
        }
    }

    public static void main(String[] args) {
        new NetEntreprisesBotSelenium().openPhantomJs();
    }

    public List<String> getSiretIds() {
        List<String> siretIdList = new ArrayList<>();

        try {
            List<WebElement> trEls = driver.findElements(By.tagName("tr"));

            for (WebElement trEl : trEls) {
                try {
                    String classAtr = trEl.getAttribute("class");

                    if (StringUtils.isNotEmpty(classAtr) && classAtr.contains("ligne_")) {
                        List<WebElement> anchors = trEl.findElements(By.tagName("a"));

                        for (WebElement anchor : anchors) {
                            if (anchor.getAttribute("title").contains("feuille de calcul")) {
                                siretIdList.add(trEl.findElements(By.tagName("td")).get(1).getText());
                                break;
                            }
                        }
                    }
                } catch (Exception e) {
                    LOGGER.error("error foreach - trEl: " + trEl.toString(), e);
                }
            }
        } catch (Exception e) {
            LOGGER.error("ERROR - getSiretIds", e);
        }

        return siretIdList;
    }

    private WebElement getCurrentRow(String siretID) {
        try {
            List<WebElement> trEls = driver.findElements(By.tagName("tr"));

            for (WebElement trEl : trEls) {
                try {
                    String classAtr = trEl.getAttribute("class");

                    if (StringUtils.isNotEmpty(classAtr) && classAtr.contains("ligne_")) {
                        List<WebElement> anchors = trEl.findElements(By.tagName("a"));
                        for (WebElement anchor : anchors) {
                            if (anchor.getAttribute("title").contains("feuille de calcul") && siretID.equals(trEl.findElements(By.tagName("td")).get(1).getText())) {
                                return anchor;
                            }
                        }
                    }
                } catch (Exception e) {
                    LOGGER.error("error foreach - trEl: " + trEl.toString(), e);
                }
            }
        } catch (Exception e) {
            LOGGER.error("ERROR - getCurrentRow. siretID: " + siretID, e);
        }

        return null;
    }

    public int getLastPageNum() {
        int lastPageNum = 1;

        try {
            String paginationTxt = Jsoup.parse(driver.getPageSource()).select("#pagination").text().trim();
            String lastPageStr = StringUtils.substringBetween(paginationTxt, "Page : 1/", "-").replaceAll("[^\\d]+", "");
            lastPageNum = Integer.parseInt(lastPageStr);
        } catch (Exception e) {
            LOGGER.error("ERROR - getLastPageNum", e);
        }
        return lastPageNum;
    }

    public int goToDownloadPage(String companyID) {
        if (clickOnCompte(companyID)) {
            return 1;
        }

        if (driver.getPageSource().contains("Il n’y a aucun AT/MP pour la période N et N-1.")) {
            return 3;
        }

        if (clickOnPassedAnchor("Compte Employeur Courant")) {
            return 2;
        }

        if (Constants.CODE_RISQUE_BC != null) {
            if (driver.findElements(By.name("codeRisque")).size() > 0) {
                LOGGER.info("RISK CODE  FOUND");
                new Select(driver.findElement(By.name("codeRisque"))).selectByVisibleText(Constants.CODE_RISQUE_BC);
                this.takeScreenshot("LOG/testDate" + UtilsMeth.getRandomHexString(5));
            } else {
                LOGGER.info("RISK CODE NOT FOUND");
            }
        }

        return 0;
    }


    private boolean clickOnCompte(String companyID) {
        boolean error;
        int maxAttempts = 0;
        String targetUrl = "https://www.teleservices.cnav.fr/CeWeb/index.htm";

        try {
            WebElement moduleATMPBox = driver.findElement(By.id("service-11"));
            String prefixUrl = "https://portail.net-entreprises.fr/priv/";

            if (moduleATMPBox.getAttribute("href").startsWith("https")) {
                prefixUrl = "";
            }

            driver.get(prefixUrl + moduleATMPBox.getAttribute("href"));
            TimeUnit.SECONDS.sleep(Constants.DELAY + 1);

            while (!driver.getCurrentUrl().startsWith(targetUrl) && maxAttempts < 5) {
                moduleATMPBox = driver.findElement(By.id("service-11"));
                prefixUrl = "https://portail.net-entreprises.fr/priv/";

                if (moduleATMPBox.getAttribute("href").startsWith("https")) {
                    prefixUrl = "";
                }

                driver.get(prefixUrl + moduleATMPBox.getAttribute("href"));
                TimeUnit.SECONDS.sleep(Constants.DELAY + 1);
                LOGGER.info("goToAT/MP -- Attempt " + maxAttempts++);
            }

            if (driver.getCurrentUrl().contains("https://www.teleservices.cnav.fr/CeWeb/index.htm")) {
                error = false;
            } else {
                if (driver.getPageSource().contains("aucun SIRET connu dans la base. Veuillez contacter votre CARSAT ou votre CGSS")) {
                    LOGGER.error("ERROR - clickOnCompte. ** AUCUN SIRET CONNU DANS LA BASE **companyID: " + companyID);
                }

                error = true;
            }
        } catch (Exception e) {
            LOGGER.error("ERROR - clickOnCompte. companyID: " + companyID, e);
            LOGGER.info("Current url compte introuvable : " + driver.getCurrentUrl());
            takeScreenshot("LOG/" + "compte-AT-MP-introuvable_" + companyID);
            error = true;
        }

        return error;
    }

    public void leaveNetEntreprises() {
        clickOnPassedAnchor("Quitter");

        try {
            driver.findElement(By.cssSelector("#header_deconnection1")).click();
        } catch (Exception e) {
            LOGGER.error("ERROR - leaveNetEntreprises");
        }
    }

    private boolean clickOnPassedAnchor(String menuItemName) {
        boolean error ;
        try {
            List<WebElement> menuEls = driver.findElement(By.id("menu")).findElements(By.tagName("a"));
            for (WebElement menuEl : menuEls) {
                if (menuEl.getText().contains(menuItemName)) {
                    menuEl.click();
                    TimeUnit.SECONDS.sleep(Constants.DELAY + 2);
                    break;
                }
            }

            error = false;
        } catch (Exception e) {
            LOGGER.error("ERROR - clickOnPassedAnchor. menuItemName: " + menuItemName, e);
            error = true;
        }

        return error;
    }

    public int login(InputData inputData) {
        String companyID = inputData.getCompanyID();

        try {
            driver.get(Constants.LOGIN_URL);
            TimeUnit.SECONDS.sleep(Constants.DELAY);
            driver.findElement(By.className("header__login")).click();
            TimeUnit.SECONDS.sleep(Constants.DELAY);
            driver.findElement(By.id("j_siret")).sendKeys(inputData.getCompanyID());
            driver.findElement(By.id("j_nom")).sendKeys(inputData.getLastname());
            driver.findElement(By.id("j_prenom")).sendKeys(inputData.getFirstname());
            driver.findElement(By.id("j_password")).clear();
            driver.findElement(By.id("j_password")).sendKeys(inputData.getPassword());
            driver.findElement(By.id("validButtonConnexion")).click();
            TimeUnit.SECONDS.sleep(Constants.DELAY);
            String pageSource = driver.getPageSource();
            boolean accountConfirmed=false;
            if(Jsoup.parse(pageSource).text().contains("Vos coordonnées doivent être confirmées")){
                LOGGER.info("CONFIRMATION AUTO INFOS POST LOGIN");
                driver.findElement(By.id("inputConfirmationConditions")).click();
                driver.findElement(By.id("validButton")).click();
                accountConfirmed=true;
                LOGGER.info("Logged in successfully with auto confirm INFOs- " + companyID);
                return 2;
            }
            if(Jsoup.parse(pageSource).select(".slick-track").size()>0
                    &&pageSource.contains("Se déconnecter") &&accountConfirmed) {
                LOGGER.info("Logged in successfully - " + companyID);
                return 2;
            }
            if(Jsoup.parse(pageSource).text().contains("Afin de garantir les échanges par courriel")){
                LOGGER.info("CONFIRMATION INFOS POST LOGIN");
                return -3;
            }
            if (Jsoup.parse(pageSource).select(".slick-track").size() > 0
                    && pageSource.contains("Se déconnecter")) {
                LOGGER.info("Logged in successfully - " + companyID);
                return 1;
            }

            if (pageSource.contains("Nous ne sommes pas en mesure de répondre à votre demande.")) {
                return 0;
            }

            if (pageSource.contains("Vous avez échoué 3 fois consécutivement")) {
                return -2;
            }
        } catch (Exception e) {
            LOGGER.error("ERROR - login. companyID: " + companyID, e);
        }

        takeScreenshot("LOG/" + "failed_login_" + companyID);
        LOGGER.info("Failed to login - " + companyID);
        return -1;
    }

    private String getCookies() {
        Set<Cookie> cookies = driver.manage().getCookies();
        StringBuilder builder = new StringBuilder();

        for (Cookie cookie : cookies) {
            builder.append(cookie.getName()).append("=").append(cookie.getValue()).append("; ");
        }

        return UtilsMeth.removeLastChar(builder.toString(), "; ");
    }

    public boolean goToFeuilleDeCalculPage(String companyID) {
        clickOnCompte(companyID);
        return selectYearAndHitOnRechercher(String.valueOf(Calendar.getInstance().get(Calendar.YEAR)));
    }


    public void refreshPage() {
        try {
            driver.navigate().refresh();
            TimeUnit.SECONDS.sleep(2);
        } catch (Exception e) {
            LOGGER.error("ERROR - refreshPage", e);
        }
    }

    public void clickOnElementsDeCalcul() {
        try {
            List<WebElement> anchors = driver.findElements(By.tagName("a"));

            for (WebElement anchor : anchors) {
                if (anchor.getText().contains("Eléments de calcul")) {
                    anchor.click();
                    TimeUnit.SECONDS.sleep(Constants.DELAY);
                    break;
                }
            }
        } catch (Exception e) {
            LOGGER.error("ERROR - clickOnElementsDeCalcul", e);
        }
    }

    public boolean downloadFeuilleFiles(InputData inputData, String companyID, String outputFolderPath, String siretYY) {
        try {
            int currYear = Calendar.getInstance().get(Calendar.YEAR);
            WebElement currentRow = getCurrentRow(siretYY);
            if (currentRow != null) {
                currentRow.click();
                TimeUnit.SECONDS.sleep(Constants.DELAY);
                String folder = outputFolderPath + "/" + inputData.getCompanyCustomName() + "/" + currYear + "/" + siretYY + "/";
                new File(folder).mkdirs();

                return download(folder + "FeuilleDeCalcul-" + siretYY + "-" + currYear + ".pdf", folder + "FeuilleDeCalcul-" + siretYY + "-" + currYear + ".csv");
            }
        } catch (Exception e) {
            LOGGER.error("ERROR - downloadFirst2Files. companyID: " + companyID, e);
        }

        return false;
    }

    public boolean downloadElemCalculFiles(InputData inputData, String companyID, String outputFolderPath, String siretYY, String year) {
        try {
            String folder = outputFolderPath + "/" + inputData.getCompanyCustomName() + "/" + Calendar.getInstance().get(Calendar.YEAR) + "/" + siretYY + "/";
            new File(folder).mkdirs();

            return download(folder + "ElemCalcul-" + year + ".pdf", folder + "ElemCalcul-" + year + ".csv");
        } catch (Exception e) {
            LOGGER.error("ERROR - downloadFirst2Files. companyID: " + companyID, e);
        }

        return false;
    }

    public boolean selectYearAndHitOnRechercher(String currYear) {
        boolean success = false;

        try {
            WebElement exerciceEl = driver.findElement(By.name("exercice"));
            List<WebElement> options = exerciceEl.findElements(By.tagName("option"));

            for (WebElement option : options) {
                String optTxt = option.getText();
                if (StringUtils.isNotEmpty(optTxt) && optTxt.contains(String.valueOf(currYear))) {
                    option.click();
                    TimeUnit.SECONDS.sleep(Constants.DELAY);
                    success = true;
                    break;
                }
            }

            driver.findElement(By.name("Rechercher")).click();
            TimeUnit.SECONDS.sleep(Constants.DELAY);
        } catch (Exception e) {
            LOGGER.error("ERROR - selectYearAndHitOnSearch. currYear: " + currYear, e);
        }

        return success;
    }

    public boolean downloadFiles(InputData inputData, String companyID, String outputFolderPath) {
        if (!driver.getCurrentUrl().startsWith("https://www.teleservices.cnav.fr/CeWeb/initCompteDynamique.htm") && !driver.getCurrentUrl().startsWith("https://www.teleservices.cnav.fr/CeWeb/rechercheCompte.htm")) {
            LOGGER.info("erreur mauvaise URL pour DL PDF/CSV");
            LOGGER.info(driver.getCurrentUrl());

            return false;
        }

        String companyCustomName = inputData.getCompanyCustomName();
        if (companyCustomName.length() == 0) {
            companyCustomName = companyID;
        }

        try {
            String date = UtilsMeth.getDay() + "-" + UtilsMeth.getMonth() + "-" + UtilsMeth.getYear();
            String folder = outputFolderPath + "/" + companyCustomName + "/";
            folder = folder.replace("{DATE}", date);
            new File(folder).mkdirs();
            String pdfPath = folder + "CEC-" + companyID + "-" + date + ".pdf";
            String csvPath = folder + "CEC-" + companyID + "-" + date + ".csv";
            boolean download = download(pdfPath, csvPath);

            if (Constants.IS_COMMAND_LINE_VERSION) {
                boolean resultPushCSVDB = UtilsAPI.submitCSVtoDB(csvPath,companyID,LOGGER);
                boolean resultPushPDFDB = UtilsAPI.submitPDFtoDB(pdfPath,companyID,LOGGER);
                new File(csvPath).delete();
                new File(pdfPath).delete();
            }

            /*send Status*/
            if (!download) {
                FileUtils.deleteDirectory(new File(folder));
                TimeUnit.SECONDS.sleep(1);
            }

            return download;
        } catch (Exception e) {
            LOGGER.error("ERROR - downloadFiles. companyID: " + companyID, e);
        }

        return false;
    }
    private void disableSSLCertCheck() throws NoSuchAlgorithmException, KeyManagementException {
        // Create a trust manager that does not validate certificate chains
        TrustManager[] trustAllCerts = new TrustManager[] {new X509TrustManager() {
            public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                return null;
            }
            public void checkClientTrusted(X509Certificate[] certs, String authType) {
            }
            public void checkServerTrusted(X509Certificate[] certs, String authType) {
            }
        }
        };

        // Install the all-trusting trust manager
        SSLContext sc = SSLContext.getInstance("SSL");
        sc.init(null, trustAllCerts, new java.security.SecureRandom());
        HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

        // Create all-trusting host name verifier
        HostnameVerifier allHostsValid = new HostnameVerifier() {
            public boolean verify(String hostname, SSLSession session) {
                return true;
            }
        };

        // Install the all-trusting host verifier
        HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
    }
    private boolean download(String pdfPath, String csvPath) {
        try {
            String pdfLink = driver.findElement(By.className("pdf")).getAttribute("href");
            String csvLink = driver.findElement(By.className("csv")).getAttribute("href");
            driver.navigate().refresh();
            driver.navigate().refresh();
            String cookies = getCookies();
            driver.get(pdfPath);
            if(driver.getCurrentUrl().contains("net-entreprises")){
                System.out.println("URL = "+driver.getCurrentUrl());
            }
            System.out.println("URL = "+driver.getCurrentUrl());
            int maxCount=100;
            int count=0;
            //disableSSLCertCheck();
            while (!(UtilsMeth.downloadFile(csvLink, cookies, csvPath)  && UtilsMeth.inspect(csvPath) ) && count++<maxCount) {
                System.out.println("CSV KO "+count);
                driver.navigate().refresh();
                driver.navigate().refresh();
                Thread.sleep(Constants.SLEEP_DELAY_NETE_EXPIRATION);
                cookies = getCookies();
            }
            System.out.println("CSV OK " + count++);
            count=0;
            while (!( UtilsMeth.downloadFile(pdfLink, cookies, pdfPath)  && UtilsMeth.inspect(pdfPath)) && count++<maxCount) {
                System.out.println("PDF KO "+count);
                driver.navigate().refresh();
                driver.navigate().refresh();
                Thread.sleep(Constants.SLEEP_DELAY_NETE_EXPIRATION);
                cookies = getCookies();
            }
            System.out.println("PDF OK " + count++);
            if(UtilsMeth.inspect(csvPath) && UtilsMeth.inspect(pdfPath)) {
                System.out.println("CSV & PDF OK " + count);
            }
            return true;
        } catch (Exception e) {
            LOGGER.error("ERROR - download", e);
        }

        return false;
    }

    public void navigateToGivenPage(int currPage) {
        try {
            List<WebElement> aEls = driver.findElements(By.tagName("a"));
            for (WebElement aEl : aEls) {
                if ("Taux AT/MP".equals(aEl.getText())) {
                    aEl.click();
                    TimeUnit.SECONDS.sleep(Constants.DELAY + 1);
                    break;
                }
            }

            driver.get("https://www.teleservices.cnav.fr/CeWeb/paginerTaux.htm?pager.offset=" + currPage * 10);
            TimeUnit.SECONDS.sleep(Constants.DELAY);
        } catch (Exception e) {
            LOGGER.error("error - navigateBack", e);
        }
    }

    private void takeScreenshot(String screenshotName) {
        try {
            FileUtils.copyFile(((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE), new File(screenshotName + ".jpg"), true);
        } catch (IOException e) {
            LOGGER.error("error ", e);
        }
    }
}

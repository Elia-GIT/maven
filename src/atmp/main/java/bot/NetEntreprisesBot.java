package atmp.main.java.bot;

import atmp.main.java.model.InputData;
import atmp.main.java.utils.Constants;
import atmp.main.java.utils.UtilsMeth;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.CookieStore;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLContexts;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.LaxRedirectStrategy;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.Select;

import java.io.File;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class NetEntreprisesBot {
    private static final Logger LOGGER = Logger.getLogger(NetEntreprisesBot.class);
    private WebDriver driver;
    private HttpClient httpClient;
    private CookieStore httpCookieStore;

    public List<String> getSiretIds() {
        List<String> siretIdList = new ArrayList<>();

        try {
            List<WebElement> trEls = driver.findElements(By.tagName("tr"));

            for (WebElement trEl : trEls) {
                try {
                    String classAtr = trEl.getAttribute("class");

                    if (StringUtils.isNotEmpty(classAtr) && classAtr.contains("ligne_")) {
                        List<WebElement> anchors = trEl.findElements(By.tagName("a"));

                        for (WebElement anchor : anchors) {
                            if (anchor.getAttribute("title").contains("feuille de calcul")) {
                                siretIdList.add(trEl.findElements(By.tagName("td")).get(1).getText());
                                break;
                            }
                        }
                    }
                } catch (Exception e) {
                    LOGGER.error("error foreach - trEl: " + trEl.toString(), e);
                }
            }
        } catch (Exception e) {
            LOGGER.error("ERROR - getSiretIds", e);
        }

        return siretIdList;
    }

    private WebElement getCurrentRow(String siretID) {
        try {
            List<WebElement> trEls = driver.findElements(By.tagName("tr"));

            for (WebElement trEl : trEls) {
                try {
                    String classAtr = trEl.getAttribute("class");

                    if (StringUtils.isNotEmpty(classAtr) && classAtr.contains("ligne_")) {
                        List<WebElement> anchors = trEl.findElements(By.tagName("a"));
                        for (WebElement anchor : anchors) {
                            if (anchor.getAttribute("title").contains("feuille de calcul") && siretID.equals(trEl.findElements(By.tagName("td")).get(1).getText())) {
                                return anchor;
                            }
                        }
                    }
                } catch (Exception e) {
                    LOGGER.error("error foreach - trEl: " + trEl.toString(), e);
                }
            }
        } catch (Exception e) {
            LOGGER.error("ERROR - getCurrentRow. siretID: " + siretID, e);
        }

        return null;
    }

    public int getLastPageNum() {
        int lastPageNum = 1;

        try {
            String paginationTxt = Jsoup.parse(driver.getPageSource()).select("#pagination").text().trim();
            String lastPageStr = StringUtils.substringBetween(paginationTxt, "Page : 1/", "-").replaceAll("[^\\d]+", "");
            lastPageNum = Integer.parseInt(lastPageStr);
        } catch (Exception e) {
            LOGGER.error("ERROR - getLastPageNum", e);
        }
        return lastPageNum;
    }

    public int goToDownloadPage(String companyID) {
        if (clickOnCompte(companyID)) {
            return 1;
        }

        if (driver.getPageSource().contains("Il n’y a aucun AT/MP pour la période N et N-1.")) {
            return 3;
        }

        if (clickOnPassedAnchor("Compte Employeur Courant")) {
            return 2;
        }

        if (Constants.CODE_RISQUE_BC != null) {
            if (driver.findElements(By.name("codeRisque")).size() > 0) {
                LOGGER.info("RISK CODE  FOUND");
                new Select(driver.findElement(By.name("codeRisque"))).selectByVisibleText(Constants.CODE_RISQUE_BC);
            } else {
                LOGGER.info("RISK CODE NOT FOUND");
            }
        }

        return 0;
    }


    private boolean clickOnCompte(String companyID) {
        boolean error;
        int maxAttempts = 0;
        String targetUrl = "https://www.teleservices.cnav.fr/CeWeb/index.htm";

        try {
            WebElement moduleATMPBox = driver.findElement(By.id("service-11"));
            String prefixUrl = "https://portail.net-entreprises.fr/priv/";

            if (moduleATMPBox.getAttribute("href").startsWith("https")) {
                prefixUrl = "";
            }

            driver.get(prefixUrl + moduleATMPBox.getAttribute("href"));
            TimeUnit.SECONDS.sleep(Constants.DELAY + 1);

            while (!driver.getCurrentUrl().startsWith(targetUrl) && maxAttempts < 5) {
                moduleATMPBox = driver.findElement(By.id("service-11"));
                prefixUrl = "https://portail.net-entreprises.fr/priv/";

                if (moduleATMPBox.getAttribute("href").startsWith("https")) {
                    prefixUrl = "";
                }

                driver.get(prefixUrl + moduleATMPBox.getAttribute("href"));
                TimeUnit.SECONDS.sleep(Constants.DELAY + 1);
                LOGGER.info("goToAT/MP -- Attempt " + maxAttempts++);
            }

            if (driver.getCurrentUrl().contains("https://www.teleservices.cnav.fr/CeWeb/index.htm")) {
                error = false;
            } else {
                if (driver.getPageSource().contains("aucun SIRET connu dans la base. Veuillez contacter votre CARSAT ou votre CGSS")) {
                    LOGGER.error("ERROR - clickOnCompte. ** AUCUN SIRET CONNU DANS LA BASE **companyID: " + companyID);
                }

                error = true;
            }
        } catch (Exception e) {
            LOGGER.error("ERROR - clickOnCompte. companyID: " + companyID, e);
            LOGGER.info("Current url compte introuvable : " + driver.getCurrentUrl());
            error = true;
        }

        return error;
    }

    private boolean clickOnPassedAnchor(String menuItemName) {
        boolean error;
        try {
            List<WebElement> menuEls = driver.findElement(By.id("menu")).findElements(By.tagName("a"));
            for (WebElement menuEl : menuEls) {
                if (menuEl.getText().contains(menuItemName)) {
                    menuEl.click();
                    TimeUnit.SECONDS.sleep(Constants.DELAY + 2);
                    break;
                }
            }

            error = false;
        } catch (Exception e) {
            LOGGER.error("ERROR - clickOnPassedAnchor. menuItemName: " + menuItemName, e);
            error = true;
        }

        return error;
    }

    public int login(InputData inputData) {
        try {
            httpCookieStore = new BasicCookieStore();
            httpClient = HttpClientBuilder.create()
                    .setDefaultCookieStore(httpCookieStore)
                    .setRedirectStrategy(new LaxRedirectStrategy())
                    .setSSLSocketFactory(new SSLConnectionSocketFactory(SSLContexts.custom()
                            .loadTrustMaterial(null, (chain, authType) -> true)
                            .useTLS()
                            .build()))
                    .build();

            List<NameValuePair> params = new ArrayList<>(2);
            params.add(new BasicNameValuePair("j_siret", inputData.getCompanyID()));
            params.add(new BasicNameValuePair("j_nom", inputData.getLastname()));
            params.add(new BasicNameValuePair("j_prenom", inputData.getFirstname()));
            params.add(new BasicNameValuePair("j_password", inputData.getPassword()));

            HttpPost httppost = new HttpPost("https://portail.net-entreprises.fr/auth/pass");
            httppost.setEntity(new UrlEncodedFormEntity(params, "UTF-8"));
            httpClient.execute(httppost);
            HttpResponse response = httpClient.execute(new HttpGet("https://portail.net-entreprises.fr/srv"));

            String pageSource = EntityUtils.toString(response.getEntity());
            if (pageSource.contains("Se déconnecter")) {
                LOGGER.info("Logged in successfully - " + inputData.getCompanyID());
                return 1;
            }

            if (pageSource.contains("Nous ne sommes pas en mesure de répondre à votre demande.")) {
                return 0;
            }

            if (pageSource.contains("Vous avez échoué 3 fois consécutivement")) {
                return -2;
            }
        } catch (NoSuchAlgorithmException | KeyManagementException | KeyStoreException | IOException e) {
           LOGGER.error("ERROR - login. companyID: " + inputData.getCompanyID(), e);
        }

        LOGGER.info("Failed to login - " + inputData.getCompanyID());
        return -1;
    }

    private String getCookies() {
        StringBuilder builder = new StringBuilder();

        List<Cookie> cookies = httpCookieStore.getCookies();
        for (Cookie cookie : cookies) {
            builder.append(cookie.getName()).append("=").append(cookie.getValue()).append("; ");
        }

        return UtilsMeth.removeLastChar(builder.toString(), "; ");
    }

    public boolean goToFeuilleDeCalculPage(String companyID) {
        clickOnCompte(companyID);
        return selectYearAndHitOnRechercher(String.valueOf(Calendar.getInstance().get(Calendar.YEAR)));
    }

    public void clickOnElementsDeCalcul() {
        try {
            List<WebElement> anchors = driver.findElements(By.tagName("a"));

            for (WebElement anchor : anchors) {
                if (anchor.getText().contains("Eléments de calcul")) {
                    anchor.click();
                    TimeUnit.SECONDS.sleep(Constants.DELAY);
                    break;
                }
            }
        } catch (Exception e) {
            LOGGER.error("ERROR - clickOnElementsDeCalcul", e);
        }
    }

    public boolean downloadFeuilleFiles(InputData inputData, String companyID, String outputFolderPath, String siretYY) {
        try {
            int currYear = Calendar.getInstance().get(Calendar.YEAR);
            WebElement currentRow = getCurrentRow(siretYY);
            if (currentRow != null) {
                currentRow.click();
                TimeUnit.SECONDS.sleep(Constants.DELAY);
                String folder = outputFolderPath + "/" + inputData.getCompanyCustomName() + "/" + currYear + "/" + siretYY + "/";
                new File(folder).mkdirs();

                return download(folder + "FeuilleDeCalcul-" + siretYY + "-" + currYear + ".pdf", folder + "FeuilleDeCalcul-" + siretYY + "-" + currYear + ".csv");
            }
        } catch (Exception e) {
            LOGGER.error("ERROR - downloadFirst2Files. companyID: " + companyID, e);
        }

        return false;
    }

    public boolean downloadElemCalculFiles(InputData inputData, String companyID, String outputFolderPath, String siretYY, String year) {
        try {
            String folder = outputFolderPath + "/" + inputData.getCompanyCustomName() + "/" + Calendar.getInstance().get(Calendar.YEAR) + "/" + siretYY + "/";
            new File(folder).mkdirs();

            return download(folder + "ElemCalcul-" + year + ".pdf", folder + "ElemCalcul-" + year + ".csv");
        } catch (Exception e) {
            LOGGER.error("ERROR - downloadFirst2Files. companyID: " + companyID, e);
        }

        return false;
    }

    public boolean selectYearAndHitOnRechercher(String currYear) {
        boolean success = false;

        try {
            WebElement exerciceEl = driver.findElement(By.name("exercice"));
            List<WebElement> options = exerciceEl.findElements(By.tagName("option"));

            for (WebElement option : options) {
                String optTxt = option.getText();
                if (StringUtils.isNotEmpty(optTxt) && optTxt.contains(String.valueOf(currYear))) {
                    option.click();
                    TimeUnit.SECONDS.sleep(Constants.DELAY);
                    success = true;
                    break;
                }
            }

            driver.findElement(By.name("Rechercher")).click();
            TimeUnit.SECONDS.sleep(Constants.DELAY);
        } catch (Exception e) {
            LOGGER.error("ERROR - selectYearAndHitOnSearch. currYear: " + currYear, e);
        }

        return success;
    }

    public boolean downloadFiles(InputData inputData, String companyID, String outputFolderPath) {
        if (!driver.getCurrentUrl().startsWith("https://www.teleservices.cnav.fr/CeWeb/initCompteDynamique.htm") && !driver.getCurrentUrl().startsWith("https://www.teleservices.cnav.fr/CeWeb/rechercheCompte.htm")) {
            LOGGER.info("erreur mauvaise URL pour DL PDF/CSV");
            LOGGER.info(driver.getCurrentUrl());

            return false;
        }

        String companyCustomName = inputData.getCompanyCustomName();
        if (companyCustomName.length() == 0) {
            companyCustomName = companyID;
        }

        try {
            String date = UtilsMeth.getDay() + "-" + UtilsMeth.getMonth() + "-" + UtilsMeth.getYear();
            String folder = outputFolderPath + "/" + companyCustomName + "/";
            folder = folder.replace("{DATE}", date);
            new File(folder).mkdirs();
            String pdfPath = folder + "CEC-" + companyID + "-" + date + ".pdf";
            String csvPath = folder + "CEC-" + companyID + "-" + date + ".csv";
            boolean download = download(pdfPath, csvPath);

            if (Constants.IS_COMMAND_LINE_VERSION) {
                new File(csvPath).delete();
                new File(pdfPath).delete();
            }

            /*send Status*/
            if (!download) {
                FileUtils.deleteDirectory(new File(folder));
                TimeUnit.SECONDS.sleep(1);
            }

            return download;
        } catch (Exception e) {
            LOGGER.error("ERROR - downloadFiles. companyID: " + companyID, e);
        }

        return false;
    }

    private boolean download(String pdfPath, String csvPath) {
        try {
            String pdfLink = driver.findElement(By.className("pdf")).getAttribute("href");
            String csvLink = driver.findElement(By.className("csv")).getAttribute("href");
            String cookies = getCookies();

            if (UtilsMeth.downloadFile(csvLink, cookies, csvPath) && UtilsMeth.downloadFile(pdfLink, cookies, pdfPath)) {
                return true;
            }
        } catch (Exception e) {
            LOGGER.error("ERROR - download", e);
        }

        return false;
    }

    public void navigateToGivenPage(int currPage) {
        try {
            List<WebElement> aEls = driver.findElements(By.tagName("a"));
            for (WebElement aEl : aEls) {
                if ("Taux AT/MP".equals(aEl.getText())) {
                    aEl.click();
                    TimeUnit.SECONDS.sleep(Constants.DELAY + 1);
                    break;
                }
            }

            driver.get("https://www.teleservices.cnav.fr/CeWeb/paginerTaux.htm?pager.offset=" + currPage * 10);
            TimeUnit.SECONDS.sleep(Constants.DELAY);
        } catch (Exception e) {
            LOGGER.error("error - navigateBack", e);
        }
    }
}

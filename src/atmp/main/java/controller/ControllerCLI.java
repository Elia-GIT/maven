package atmp.main.java.controller;

import atmp.main.java.bot.NetEntreprisesBotSelenium;
import atmp.main.java.csv.CsvReaderWriter;
import atmp.main.java.model.InputData;
import atmp.main.java.model.License;
import atmp.main.java.utils.*;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class ControllerCLI {
    private volatile boolean FLAG_1 = false;
    private static final Logger LOGGER = Logger.getLogger(ControllerCLI.class);

    public String csvPath;
    public String outputFolderPath;
    private String fullFolderPath;

    private ExecutorService thr1;
    private CsvReaderWriter rdWr = new CsvReaderWriter();

    public void startSc1() {
        if (Constants.IS_API_VERSION) {
            //No matter what
            csvPath = "input.csv";
            outputFolderPath = "sortie";
        }

        FLAG_1 = false;

        if (thr1 != null) {
            thr1.shutdownNow();
        }

        thr1 = Executors.newSingleThreadExecutor();
        thr1.submit(this::stSc1);
    }

    private void stSc1() {
        if (licenseValuesRun(0).equals(License.ACTIVE)) {
            scImpl("(Scenario 1)");
        }

        thr1.shutdown();
        exitCommandLine();
    }

    private void scImpl(String scenario) {
        writeToTextArea("Début" + scenario + " - " + time());
        List<InputData> inputDatas = rdWr.readInputDataList(csvPath);
        int size = inputDatas.size() - 1;
        NetEntreprisesBotSelenium bot = new NetEntreprisesBotSelenium();
        CsvReaderWriter writer = new CsvReaderWriter();
        String date = UtilsMeth.getDay() + "-"
                + UtilsMeth.getMonth() + "-" + UtilsMeth.getYear() + "-"
                + UtilsMeth.getHour() + "H-" + UtilsMeth.getMinute()
                + "M-" + UtilsMeth.getSecond() + "s";
        String folder = outputFolderPath + "/CompteEmployeurCourant/{DATE}/";
        if ("(Scenario 2)".equals(scenario)) {
            folder = folder.replace("CompteEmployeurCourant", "TauxAT-MP");
        }
        folder = folder.replace("{DATE}", date);
        fullFolderPath = folder;

        if (Constants.IS_DEMO_LINKEDIN) {
            try {
                if (Jsoup.connect("http://licence-simplicia.pbconseil.ovh/linkedin/" + UtilsLicenseLinkedIn.getId(LOGGER) + ".html").execute().statusCode() != 200) {
                    writeToTextArea("DEMO INACTIVE");
                    return;
                }
            } catch (IOException e) {
                writeToTextArea("DEMO INACTIVE");
                return;
            }

            File f = new File(Constants.LINKEDIN_DEMO_FILE);
            if (!(f.exists() && !f.isDirectory())) {
                writeToTextArea("FICHIER DEMO MANQUANT");
                return;
            }

            writeToTextArea("DEMO ACTIVE");
        }

        String successName = folder + "succès" /*+ time().replace(":", "-")*/ + ".csv";
        String failedName = folder + "échoué" /*+ time().replace(":", "-") */ + ".csv";

        HashMap<Integer, List<String>> etatSiret = UtilsMeth.initEtatSiret(10);

        for (int k = 1; k < inputDatas.size(); k++) {
            InputData inputData = inputDatas.get(k);
            if ((FLAG_1 && "(Scenario 1)".equals(scenario)) || (FLAG_2 && "(Scenario 2)".equals(scenario))) {
                break;
            }

            if (!bot.openPhantomJs()) {
                writeToTextArea("Erreur à l'ouverture de PhantomJS - assurez vous que le fichier soit dans le même dossier que le Bot" + scenario + " !");
                break;
            }

            String companyID = inputData.getCompanyID();

            if (companyID.length() != 14) {
                writeToTextArea("[" + companyID + "] " + "SIRET DE TAILLE INCORRECTE : IGNORE");
                String status = UtilsRapportRun.getTypeCompte(0, false);
                UtilsAPI.setLoginStatus(companyID, status, LOGGER);

                continue;
            }

            writeToTextArea("                             ***");
            writeToTextArea("[" + companyID + "] " + "Connexion en cours");

            int login = bot.login(inputData);
            if ("(Scenario 1)".equals(scenario)) {
                int isSuc = sc1Impl(inputData, login, bot, companyID);
                inputData.setStatusLogin(isSuc);
//                inputData.setPassword(encrypted);

                if (isSuc == 5) {
                    String status = UtilsRapportRun.getTypeCompte(isSuc, false);
                    etatSiret.get(isSuc).add(companyID);
                    UtilsAPI.setLoginStatus(companyID, status, LOGGER);
                } else {
                    /*[ECHEC]CALL API companyID + isSuc*/
                    String status = UtilsRapportRun.getTypeCompte(isSuc, false);
                    etatSiret.get(isSuc).add(companyID);
                    UtilsAPI.setLoginStatus(companyID, status, LOGGER);
                }
            } else {
                boolean isSuc = sc2Impl(inputData, login, bot, companyID);
                inputData.setStatusLogin(isSuc ? 1 : 0);
                writer.writeCSV(inputData, isSuc ? successName : failedName);
                bot.leaveNetEntreprises();
            }

            bot.closePhantomJsBr();

            try {
                TimeUnit.SECONDS.sleep(2);
            } catch (InterruptedException ignored) {
            }
        }

        bot.closePhantomJsBr();

        writeToTextArea("Fin" + scenario + " - " + time());
        if (inputDatas.size() > 1) {
            try {
                String toPrint = UtilsMeth.readFile("LOG/log_net_entreprises.log");
                DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
                LocalDateTime now = LocalDateTime.now();
                UtilsMeth.writeFile("LOG/log_net_entreprises-" + dtf.format(now) + ".txt", toPrint);
                UtilsAPI.submitExtractionLogToDB(dtf.format(now), toPrint, size - etatSiret.get(5).size(), etatSiret.get(5).size(), "ATMP", LOGGER);
            } catch (IOException e) {
                LOGGER.error(e);
            }
        }
    }

    private int sc1Impl(InputData inputData, int login, NetEntreprisesBotSelenium bot, String companyID) {
        int nbAttempts = 1;
        int maxAttempts = 2;
        int success = 0;
        int waitTimeSec = 15;
        int waitTimeMinPassWd = 1;
        boolean infosAConfirmerParMail=false;
        while(success <5 && nbAttempts<=maxAttempts && !infosAConfirmerParMail) {
            LOGGER.info("-->"+login);
            if (login==1 | login==2) {
                success = 1;
                if(login==2) {
                    writeToTextArea("[" + companyID + "] " + "Compte avec informations de connexion confirmées");
                }
                writeToTextArea("[" + companyID + "] " + "Connexion réussie(Scenario 1) !");
                writeToTextArea("[" + companyID + "] " + "Merci de patienter, navigation à travers les pages et téléchargement des fichiers (Scenario 1) . . .");
                Constants.CODE_RISQUE_BC = inputData.getCodeRisque();
                int goToDownloadPage = bot.goToDownloadPage(companyID);

                if (goToDownloadPage >= 1) {
                    if (goToDownloadPage == 3) {
                        writeToTextArea("[" + companyID + "] " + "Compte AT/MP ok - Pas d'AT pour la période N et N-1");
                        success = 2;
                    }

                    if (goToDownloadPage == 2) {
                        writeToTextArea("[" + companyID + "] " + "Compte AT/MP ok - CEC introuvable");
                        success = 2;
                    }

                    if (goToDownloadPage == 1) {
                        writeToTextArea("[" + companyID + "] " + "Compte AT/MP introuvable");
                        success = 1;
                    }

                    writeToTextArea("[" + companyID + "] " + "Pause " + waitTimeSec + "sec puis nouvelle tentative");

                    try {
                        //WAIT 1 MINUTES
                        Thread.sleep(waitTimeSec * 1000);
                    } catch (InterruptedException e) {
                        LOGGER.info("ERREUR DE SLEEP");
                    }

                    nbAttempts++;
                    bot.leaveNetEntreprises();
                    login = bot.login(inputData);
                } else {
                    String msgTxt = "[" + companyID + "] " + "Aucuns fichiers à télécharger(Scenario 1) !";
                    if (bot.downloadFiles(inputData, companyID, fullFolderPath)) {
                        msgTxt = "[" + companyID + "] " + "Téléchargement des fichiers réussi(Scenario 1) !";
                        success = 5;
                    } else {
                        success = 4;
                    }

                    writeToTextArea(msgTxt);
                }
            } else if (login == -1) {
                if (nbAttempts == 0) {
                    nbAttempts = maxAttempts;
                } else {
                    nbAttempts++;
                }
                writeToTextArea("[" + companyID + "] " + "Connexion échouée");

                login = bot.login(inputData);
                if (nbAttempts < maxAttempts) {
                    writeToTextArea("[" + companyID + "][" + nbAttempts + "]" + "Connexion en cours");
                } else {
                    writeToTextArea("[" + companyID + "][" + nbAttempts + "]" + "Abandon : nb tentatives max atteint (" + maxAttempts + ")");
                }
                /*MOT DE PASSE ERRONE 3 FOIS : WAIT 5min*/
            } else if (login == -2) {
                writeToTextArea("[" + companyID + "] " + "Mot de passe incorrect 3 fois : blocage 5 min");
                writeToTextArea("[" + companyID + "] " + "Pause " + waitTimeMinPassWd + "min puis nouvelle tentative");

                try {
                    //WAIT 10 MINUTES
                    Thread.sleep(waitTimeMinPassWd *0* 60 * 1000);
                } catch (InterruptedException e) {
                    LOGGER.info("ERREUR DE SLEEP");
                }
               /*
                nbAttempts++;
                if(nbAttempts<maxAttempts) {
                    bot.leaveNetEntreprises();
                    login = bot.login(inputData);
                    writeToTextArea("[" + companyID + "][" + nbAttempts + "]" + "Connexion en cours");
                }else{*/
                writeToTextArea("[" + companyID + "][" + nbAttempts + "]" + "Abandon : nb tentatives max atteint");
                nbAttempts=maxAttempts+1;
                /*}*/
            }


            else if(login==-3){
                LOGGER.info("-->"+login);

                writeToTextArea("[" + companyID + "][" + nbAttempts + "]" + "Informations de connexion à valider via le lien reçu par mail");
                infosAConfirmerParMail=true;
                success=6;
            }
    else if (login == 0) {
                writeToTextArea("[" + companyID + "] " + "Erreur Net-Entreprises - Contactez l'assistance");
                writeToTextArea("[" + companyID + "] " + "Pause " + waitTimeSec + "sec puis nouvelle tentative");

                try {
                    //WAIT 10 MINUTES
                    Thread.sleep(waitTimeSec * 1000);
                } catch (InterruptedException e) {
                    LOGGER.info("ERREUR DE SLEEP");
                }

                nbAttempts++;
                if (nbAttempts < maxAttempts) {
                    bot.leaveNetEntreprises();
                }
                login = bot.login(inputData);
                success = 3;
            }
        }
        if (nbAttempts < maxAttempts) {
            bot.leaveNetEntreprises();
        }
        return success;
    }

    private void exitCommandLine() {
        if (thr1 != null) {
            thr1.shutdownNow();
        }

        FLAG_1 = true;
        System.exit(0);
    }

    private volatile boolean FLAG_2 = false;

    private boolean sc2Impl(InputData inputData, int login, NetEntreprisesBotSelenium bot, String companyID) {
        boolean suc = false;

        if (login == 1) {
            writeToTextArea("[" + companyID + "] " + "Connexion réussie(Scenario 2) !");

            if (bot.goToFeuilleDeCalculPage(companyID)) {
                int lastPageNum = bot.getLastPageNum();
                for (int i = 1; i <= lastPageNum; i++) {
                    if (FLAG_2) {
                        break;
                    }

                    writeToTextArea("[" + companyID + "] " + "Page(Scenario 2) - " + i + " sur " + lastPageNum);
                    for (String id : bot.getSiretIds()) {
                        if (FLAG_2) {
                            break;
                        }

                        writeToTextArea("[" + companyID + "] " + "Merci de patienter, téléchargement en cours des fichiers - id: " + id);
                        if (bot.downloadFeuilleFiles(inputData, companyID, fullFolderPath, id)) {
                            writeToTextArea("[" + companyID + "] " + "Feuilles De Calcul téléchargées avec succés(Scenario 2) !");
                            bot.clickOnElementsDeCalcul();

                            int currYear = Calendar.getInstance().get(Calendar.YEAR) - 2;
                            for (int year = currYear; year >= currYear - 2; year--) {
                                if (FLAG_2) {
                                    break;
                                }

                                writeToTextArea("[" + companyID + "] " + "Téléchargement des Elements de Calcul(Scenario 2) - year: " + year);
                                bot.selectYearAndHitOnRechercher(String.valueOf(year));
                                if (bot.downloadElemCalculFiles(inputData, companyID, fullFolderPath, id, String.valueOf(year))) {
                                    writeToTextArea("[" + companyID + "] " + "Elements de Calcul téléchargés avec succés(Scenario 2) !");
                                }
                            }
                        }

                        bot.navigateToGivenPage(i - 1);
                        bot.refreshPage();
                    }

                    bot.navigateToGivenPage(i);
                    bot.refreshPage();
                }

                suc = true;
            } else {
                writeToTextArea("[" + companyID + "] " + "Le listings des sites du client n'a pas pu être affiché - SIRET(Scenario 2): " + companyID);
            }
        } else {
            writeToTextArea("[" + companyID + "] " + "Connexion échouée(Scenario 2) !");
        }

        return suc;
    }

    private License licenseValuesRun(int xOrY) {
        if (Constants.IS_API_VERSION) {
            return License.ACTIVE;
        }
        if (Constants.IS_DEMO_LINKEDIN) {
            return License.ACTIVE;
        }

        List<Integer> licenseValues = UtilsMeth.getLicenseValues(UtilsMeth.getLicenseUrlFromFile(Constants.CONFIG_FILE));
        if (licenseValues.size() >= 2) {
            Integer x = licenseValues.get(xOrY);

            if (x >= 1) {
                writeToTextArea("Licence activée");
                return License.ACTIVE;
            } else {
                writeToTextArea("Licence désactivée");
                return License.DESACTIVE;
            }
        }

        writeToTextArea("Unknown error.");
        return License.UNKNOWN;
    }

    private void writeToTextArea(final String message) {
        LOGGER.info("### GUI ###  " + message);
    }

    private String time() {
        return new SimpleDateFormat("HH:mm:ss").format(Calendar.getInstance().getTime());
    }
}

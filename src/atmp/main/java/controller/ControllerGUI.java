package atmp.main.java.controller;

import atmp.main.java.bot.NetEntreprisesBotSelenium;
import atmp.main.java.csv.CSVConnecter;
import atmp.main.java.csv.CsvReaderWriter;
import atmp.main.java.model.InputData;
import atmp.main.java.model.License;
import atmp.main.java.utils.Constants;
import atmp.main.java.utils.UtilsCompteConsolide;
import atmp.main.java.utils.UtilsLicenseLinkedIn;
import atmp.main.java.utils.UtilsMeth;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class ControllerGUI implements Initializable {
    private volatile boolean FLAG_1 = false;
    private static final Logger LOGGER = Logger.getLogger(ControllerGUI.class);

    @FXML
    private Button selectBtn;
    @FXML
    private Button outBtn;

    @FXML
    private Button startSc1Btn;
    @FXML
    private Button stopSc1Btn;
    @FXML
    private Button startSc2Btn;
    @FXML
    private Button stopSc2Btn;

    @FXML
    private CheckBox schChk;
    private FileChooser fileChooser = new FileChooser();
    private String csvPath;
    private DirectoryChooser directoryChooser = new DirectoryChooser();
    private String outputFolderPath;
    private NetEntreprisesBotSelenium currentBot;
    private String fullFolderPath;

    @FXML
    private TextArea txtA;
    @FXML
    private Label curr1Lbl;
    @FXML
    private Label curr2Lbl;
    @FXML
    private Label size1Lbl;
    @FXML
    private Label size2Lbl;
    @FXML
    private TextField failedTimeOutTxtF;

    private CSVConnecter csvConnecter;
    private ExecutorService thr1;
    private CsvReaderWriter rdWr = new CsvReaderWriter();

    @FXML
    public void startSc1() {
        if (StringUtils.isEmpty(csvPath) || StringUtils.isEmpty(outputFolderPath)) {
            writeToTextArea("Merci de choisir un fichier/dossier !");
            return;
        }

        //csvPath = "input.csv";
        //outputFolderPath = "sortie";
        FLAG_1 = false;

        if (thr1 != null) {
            thr1.shutdownNow();
        }

        thr1 = Executors.newSingleThreadExecutor();
        thr1.submit(this::stSc1);
    }


    private void stSc1() {
        if (schChk.isSelected()) {
            writeToTextArea("Scheduler démarré !");

            while (!FLAG_1) {
                FLAG_1 = false;
                scImpl("(Scenario 1)");
                writeToTextArea("Prochain run prévu à 12H00 . . .");

                for (int i = 1; i <= (24 * 3600); i++) {
                    if (FLAG_1 || new Date().toString().contains("12:00:0")) {
                        break;
                    }

                    try {
                        TimeUnit.SECONDS.sleep(1);
                    } catch (InterruptedException ignored) {
                    }
                }
            }
        } else {
            if (licenseValuesRun(0).equals(License.ACTIVE)) {
                scImpl("(Scenario 1)");
            }
        }

        thr1.shutdown();
    }

    private void scImpl(String scenario) {
        Label currLbl = "(Scenario 1)".equals(scenario) ? curr1Lbl : curr2Lbl;
        Label sizeLbl = "(Scenario 1)".equals(scenario) ? size1Lbl : size2Lbl;

        int delay = Integer.parseInt(failedTimeOutTxtF.getText().trim());
        writeToTextArea("Début" + scenario + " - " + time());
        /*ICI CHANGER LE READINPUT DATALIST AVEC JSON ... */
        List<InputData> inputDatas = rdWr.readInputDataList(csvPath);
        int size = inputDatas.size() - 1;
        Platform.runLater(() -> sizeLbl.setText(String.valueOf(size)));
        Platform.runLater(() -> currLbl.setText("0"));
        Platform.runLater(() -> sizeLbl.setText("0"));

        int count = 1;
        NetEntreprisesBotSelenium bot = new NetEntreprisesBotSelenium();
        currentBot = bot;
        CsvReaderWriter writer = new CsvReaderWriter();
        String date = UtilsMeth.getDay() + "-"
                + UtilsMeth.getMonth() + "-" + UtilsMeth.getYear() + "-"
                + UtilsMeth.getHour() + "H-" + UtilsMeth.getMinute()
                + "M-" + UtilsMeth.getSecond() + "s";
        String folder = outputFolderPath + "/CompteEmployeurCourant/{DATE}/";
        if ("(Scenario 2)".equals(scenario)) {
            folder = folder.replace("CompteEmployeurCourant", "TauxAT-MP");
        }
        folder = folder.replace("{DATE}", date);
        fullFolderPath = folder;

        if (Constants.IS_DEMO_LINKEDIN) {
            try {
                if (Jsoup.connect("http://licence-simplicia.pbconseil.ovh/linkedin/" + UtilsLicenseLinkedIn.getId(LOGGER) + ".html").execute().statusCode() != 200) {
                    writeToTextArea("DEMO INACTIVE");
                    return;
                }
            } catch (IOException e) {
                writeToTextArea("DEMO INACTIVE");
                return;
            }

            File f = new File(Constants.LINKEDIN_DEMO_FILE);
            if (!(f.exists() && !f.isDirectory())) {
                writeToTextArea("FICHIER DEMO MANQUANT");
                return;
            }

            writeToTextArea("DEMO ACTIVE");
        }

        String successName = folder + "succès" /*+ time().replace(":", "-")*/ + ".csv";
        String failedName = folder + "échoué" /*+ time().replace(":", "-") */ + ".csv";

        new File(folder).mkdirs();

        if (!inputDatas.isEmpty()) {
            writer.writeHeaderLine(inputDatas.get(0).getHeaderLine(), successName);
            writer.writeHeaderLine(inputDatas.get(0).getHeaderLine(), failedName);
        }

        for (int k = 1; k < inputDatas.size(); k++) {
            InputData inputData = inputDatas.get(k);
            if ((FLAG_1 && "(Scenario 1)".equals(scenario)) || (FLAG_2 && "(Scenario 2)".equals(scenario))) {
                break;
            }
            if (!bot.openPhantomJs()) {
                writeToTextArea("Erreur à l'ouverture de PhantomJS - assurez vous que le fichier soit dans le même dossier que le Bot" + scenario + " !");
                break;
            }

            final int finalCount = count;
            Platform.runLater(() -> currLbl.setText(String.valueOf(finalCount)));

            count++;
            String companyID = inputData.getCompanyID();

            if (companyID.length() != 14) {
                writeToTextArea("[" + companyID + "] " + "SIRET DE TAILLE INCORRECTE : IGNORE");
                continue;
            }

            writeToTextArea("                             ***");
            writeToTextArea("[" + companyID + "] " + "Connexion en cours");

            int login = bot.login(inputData);
            if ("(Scenario 1)".equals(scenario)) {
                int isSuc = sc1Impl(inputData, login, bot, companyID);
                inputData.setStatusLogin(isSuc);
//                inputData.setPassword(encrypted);

                if (isSuc == 5) {
                    writer.writeCSV(inputData, successName);
                } else {
                    writer.writeCSV(inputData, failedName);
                    writeToTextArea("[" + companyID + "] " + "Pause de " + delay + " secondes . . .");
                    try {
                        TimeUnit.SECONDS.sleep(delay);
                    } catch (InterruptedException ignored) {
                    }
                }
            } else {
                boolean isSuc = sc2Impl(inputData, login, bot, companyID);
                inputData.setStatusLogin(isSuc ? 1 : 0);
                writer.writeCSV(inputData, isSuc ? successName : failedName);
                bot.leaveNetEntreprises();
            }

            bot.closePhantomJsBr();

            try {
                TimeUnit.SECONDS.sleep(2);
            } catch (InterruptedException ignored) {
            }
        }

        bot.closePhantomJsBr();

        if ("(Scenario 1)".equals(scenario) && UtilsCompteConsolide.compteConsolideOn()) {
            try {
                UtilsCompteConsolide.writeCompteConsolideReport(date, folder, LOGGER);//,inputDatas);
                writeToTextArea("Génération du Compte Consolide réussie");
            } catch (IOException ex) {
                writeToTextArea("Erreur de génération du Compte Consolide");
            }
        }

        writeToTextArea("Fin" + scenario + " - " + time());
    }

    private int sc1Impl(InputData inputData, int login, NetEntreprisesBotSelenium bot, String companyID) {
        int nbAttempts = 1;
        int maxAttempts = 2;
        int success = 0;
        int waitTimeSec = 15;
        int waitTimeMinPassWd = 1;
        boolean infosAConfirmerParMail=false;
        while(success <5 && nbAttempts<=maxAttempts && !infosAConfirmerParMail) {
            LOGGER.info("-->"+login);
            if (login==1 | login==2) {
                success = 1;
                if(login==2) {
                    writeToTextArea("[" + companyID + "] " + "Compte avec informations de connexion confirmées");
                }
                writeToTextArea("[" + companyID + "] " + "Connexion réussie(Scenario 1) !");
                writeToTextArea("[" + companyID + "] " + "Merci de patienter, navigation à travers les pages et téléchargement des fichiers (Scenario 1) . . .");
                Constants.CODE_RISQUE_BC = inputData.getCodeRisque();
                int goToDownloadPage = bot.goToDownloadPage(companyID);

                if (goToDownloadPage >= 1) {
                    if (goToDownloadPage == 3) {
                        writeToTextArea("[" + companyID + "] " + "Compte AT/MP ok - Pas d'AT pour la période N et N-1");
                        success = 2;
                    }

                    if (goToDownloadPage == 2) {
                        writeToTextArea("[" + companyID + "] " + "Compte AT/MP ok - CEC introuvable");
                        success = 2;
                    }

                    if (goToDownloadPage == 1) {
                        writeToTextArea("[" + companyID + "] " + "Compte AT/MP introuvable");
                        success = 1;
                    }

                    writeToTextArea("[" + companyID + "] " + "Pause " + waitTimeSec + "sec puis nouvelle tentative");

                    try {
                        //WAIT 1 MINUTES
                        Thread.sleep(waitTimeSec * 1000);
                    } catch (InterruptedException e) {
                        LOGGER.info("ERREUR DE SLEEP");
                    }

                    nbAttempts++;
                    bot.leaveNetEntreprises();
                    login = bot.login(inputData);
                } else {
                    String msgTxt = "[" + companyID + "] " + "Aucuns fichiers à télécharger(Scenario 1) !";
                    if (bot.downloadFiles(inputData, companyID, fullFolderPath)) {
                        msgTxt = "[" + companyID + "] " + "Téléchargement des fichiers réussi(Scenario 1) !";
                        success = 5;
                    } else {
                        success = 4;
                    }

                    writeToTextArea(msgTxt);
                }
            } else if (login == -1) {
                if (nbAttempts == 0) {
                    nbAttempts = maxAttempts;
                } else {
                    nbAttempts++;
                }
                writeToTextArea("[" + companyID + "] " + "Connexion échouée");

                login = bot.login(inputData);
                if (nbAttempts < maxAttempts) {
                    writeToTextArea("[" + companyID + "][" + nbAttempts + "]" + "Connexion en cours");
                } else {
                    writeToTextArea("[" + companyID + "][" + nbAttempts + "]" + "Abandon : nb tentatives max atteint (" + maxAttempts + ")");
                }
                /*MOT DE PASSE ERRONE 3 FOIS : WAIT 5min*/
            } else if (login == -2) {
                writeToTextArea("[" + companyID + "] " + "Mot de passe incorrect 3 fois : blocage 5 min");
                writeToTextArea("[" + companyID + "] " + "Pause " + waitTimeMinPassWd + "min puis nouvelle tentative");

                try {
                    //WAIT 10 MINUTES
                    Thread.sleep(waitTimeMinPassWd * 60 * 1000);
                } catch (InterruptedException e) {
                    LOGGER.info("ERREUR DE SLEEP");
                }
              /*
                nbAttempts++;
                if(nbAttempts<maxAttempts) {
                    bot.leaveNetEntreprises();
                    login = bot.login(inputData);
                    writeToTextArea("[" + companyID + "][" + nbAttempts + "]" + "Connexion en cours");
                }else{*/
                writeToTextArea("[" + companyID + "][" + nbAttempts + "]" + "Abandon : nb tentatives max atteint");

                /*}*/
            }


            else if(login==-3){
                LOGGER.info("-->"+login);

                writeToTextArea("[" + companyID + "][" + nbAttempts + "]" + "Informations de connexion à valider via le lien reçu par mail");
                infosAConfirmerParMail=true;
                success=6;
            }
 else if (login == 0) {
                writeToTextArea("[" + companyID + "] " + "Erreur Net-Entreprises - Contactez l'assistance");
                writeToTextArea("[" + companyID + "] " + "Pause " + waitTimeSec + "sec puis nouvelle tentative");

                try {
                    //WAIT 10 MINUTES
                    Thread.sleep(waitTimeSec * 1000);
                } catch (InterruptedException e) {
                    LOGGER.info("ERREUR DE SLEEP");
                }

                nbAttempts++;
                if (nbAttempts < maxAttempts) {
                    bot.leaveNetEntreprises();
                }

                login = bot.login(inputData);
                success = 3;
            }
        }

        if (nbAttempts < maxAttempts) {
            bot.leaveNetEntreprises();
        }
        return success;
    }

    @FXML
    public void stopSc1() {
        if (currentBot != null) {
            currentBot.quit();
            currentBot.closePhantomJsBr();
        }

        if (thr1 != null) {
            thr1.shutdownNow();
        }

        FLAG_1 = true;
        writeToTextArea("Scheduler arrêté !");
    }

    @FXML
    public void exit(ActionEvent event) {
        if (currentBot != null) {
            currentBot.quit();
            currentBot.closePhantomJsBr();
        }

        if (thr1 != null) {
            thr1.shutdown();
        }

        FLAG_1 = true;
        Node source = (Node) event.getSource();
        ((Stage) source.getScene().getWindow()).close();
        System.exit(0);
    }

    private void writeMessage(String message) {
        txtA.appendText(message + "\n");
    }

    private volatile boolean FLAG_2 = false;
    private ExecutorService thr2;

    @FXML
    public void startSc2() {
        if (StringUtils.isEmpty(csvPath) || StringUtils.isEmpty(outputFolderPath)) {
            writeToTextArea("Merci de choisir un fichier/dossier !");
            return;
        }

        if (!licenseValuesRun(1).equals(License.ACTIVE)) {
            return;
        }

        thr2 = Executors.newSingleThreadExecutor();
        thr2.submit(this::stSc2);
    }


    private void stSc2() {
        if (Constants.IS_DEMO_LINKEDIN) {
            writeToTextArea("RECUPERATION DES TAUX INDISPONIBLE EN VERSION DEMO");
            return;
        }

        scImpl("(Scenario 2)");
        thr2.shutdown();
    }

    private boolean sc2Impl(InputData inputData, int login, NetEntreprisesBotSelenium bot, String companyID) {
        boolean suc = false;

        if (login == 1) {
            writeToTextArea("[" + companyID + "] " + "Connexion réussie(Scenario 2) !");

            if (bot.goToFeuilleDeCalculPage(companyID)) {
                int lastPageNum = bot.getLastPageNum();
                for (int i = 1; i <= lastPageNum; i++) {
                    if (FLAG_2) {
                        break;
                    }

                    writeToTextArea("[" + companyID + "] " + "Page(Scenario 2) - " + i + " sur " + lastPageNum);
                    for (String id : bot.getSiretIds()) {
                        if (FLAG_2) {
                            break;
                        }

                        writeToTextArea("[" + companyID + "] " + "Merci de patienter, téléchargement en cours des fichiers - id: " + id);
                        if (bot.downloadFeuilleFiles(inputData, companyID, fullFolderPath, id)) {
                            writeToTextArea("[" + companyID + "] " + "Feuilles De Calcul téléchargées avec succés(Scenario 2) !");
                            bot.clickOnElementsDeCalcul();

                            int currYear = Calendar.getInstance().get(Calendar.YEAR) - 2;
                            for (int year = currYear; year >= currYear - 2; year--) {
                                if (FLAG_2) {
                                    break;
                                }

                                writeToTextArea("[" + companyID + "] " + "Téléchargement des Elements de Calcul(Scenario 2) - year: " + year);
                                bot.selectYearAndHitOnRechercher(String.valueOf(year));
                                if (bot.downloadElemCalculFiles(inputData, companyID, fullFolderPath, id, String.valueOf(year))) {
                                    writeToTextArea("[" + companyID + "] " + "Elements de Calcul téléchargés avec succés(Scenario 2) !");
                                }
                            }
                        }

                        bot.navigateToGivenPage(i - 1);
                        bot.refreshPage();
                    }

                    bot.navigateToGivenPage(i);
                    bot.refreshPage();
                }

                suc = true;
            } else {
                writeToTextArea("[" + companyID + "] " + "Le listings des sites du client n'a pas pu être affiché - SIRET(Scenario 2): " + companyID);
            }
        } else {
            writeToTextArea("[" + companyID + "] " + "Connexion échouée(Scenario 2) !");
        }

        return suc;
    }

    @FXML
    public void stopSc2() {
        FLAG_2 = true;
        writeToTextArea("Arrêt (Scenario 2) !");
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        String activationFile = System.getProperty("user.home") + "\\" + "khsfv--__34p.pwd";

        if (new File(activationFile).exists()) {
            licenseValuesRun(0);
        } else if (new File(Constants.CONFIG_FILE).exists()) {
            String licenseUrl = UtilsMeth.getLicenseUrlFromFile(Constants.CONFIG_FILE);
            if (StringUtils.isEmpty(licenseUrl) || !licenseUrl.contains("pbconseil.ovh")) {
                writeToTextArea("config.txt invalide");
                disableButtons();
            } else {
                licenseValues(activationFile);
            }
        } else {
            disableButtons();
        }

        fileChooser.setInitialDirectory(new File(System.getProperty("user.dir")));
        selectBtn.setOnAction(e -> {
            File file = fileChooser.showOpenDialog(null);
            if (file != null) {
                csvPath = file.getAbsolutePath();
                writeToTextArea("Fichier sélectionné - " + csvPath);
                setStyleButtons();
            }
        });

        directoryChooser.setInitialDirectory(new File(System.getProperty("user.dir")));
        outBtn.setOnAction(e -> {
            File folder = directoryChooser.showDialog(null);

            if (folder != null) {
                outputFolderPath = folder.getAbsolutePath();
                writeToTextArea("Répertoire de destination - " + outputFolderPath);
            }
        });
    }

    private void licenseValues(String activePath) {
        if (Constants.IS_DEMO_LINKEDIN) {
            return;
        }
        List<Integer> licenseValues = UtilsMeth.getLicenseValues(UtilsMeth.getLicenseUrlFromFile(Constants.CONFIG_FILE));
        if (licenseValues.size() < 2) {
            writeToTextArea("Unknown error.");
            disableButtons();
        } else {
            Integer x = licenseValues.get(0);
            if (x <= 0) {
                writeToTextArea("Licence désactivée");
                disableButtons();
            } else if (x >= 2) {
                writeToTextArea("Licence déjà activée");
                disableButtons();
            } else {

                try {
                    boolean newFile = new File(activePath).createNewFile();
                    if (newFile) {
                        writeToTextArea("Licence activée");
                        return;
                    }
                } catch (Exception e) {
                    LOGGER.error("ERROR - create activation file", e);
                }
                writeToTextArea("Unknown error.");
                disableButtons();
            }
        }
    }

    private void disableButtons() {
        Platform.runLater(() -> startSc1Btn.setDisable(true));
        Platform.runLater(() -> stopSc1Btn.setDisable(true));
        Platform.runLater(() -> startSc2Btn.setDisable(true));
        Platform.runLater(() -> stopSc2Btn.setDisable(true));
    }

    private void setStyleButtons() {
        Platform.runLater(() -> startSc1Btn.setStyle(null));
        Platform.runLater(() -> startSc2Btn.setStyle(null));
        Platform.runLater(() -> stopSc1Btn.setStyle(null));
        Platform.runLater(() -> stopSc2Btn.setStyle(null));
    }

    private void setupCSVFilePath() {
        String path = null;
        fileChooser.setInitialDirectory(new File(System.getProperty("user.dir")));

        File file = fileChooser.showOpenDialog(null);
        if (file != null) {
            path = file.getAbsolutePath();
        }

        csvConnecter = new CSVConnecter(path);
    }

    public void onEnctyptBtnClicked() {
        try {
            setupCSVFilePath();
            csvConnecter.enctypt();
            writeToTextArea("Encryption success");
        } catch (Exception e) {
            e.printStackTrace();
            writeToTextArea("Encryption failed");
        }
    }

    public void onDectyptBtnClicked() {
        try {
            setupCSVFilePath();
            csvConnecter.decrypt();
            writeToTextArea("Decryption success");
        } catch (Exception e) {
            e.printStackTrace();
            writeToTextArea("Decryption failed");
        }
    }

    private License licenseValuesRun(int xOrY) {
        if (Constants.IS_DEMO_LINKEDIN) {
            return License.ACTIVE;
        }

        List<Integer> licenseValues = UtilsMeth.getLicenseValues(UtilsMeth.getLicenseUrlFromFile(Constants.CONFIG_FILE));
        if (licenseValues.size() >= 2) {
            Integer x = licenseValues.get(xOrY);

            if (x >= 1) {
                writeToTextArea("Licence activée");
                return License.ACTIVE;
            } else {
                writeToTextArea("Licence désactivée");
                disableButtons();

                return License.DESACTIVE;
            }
        }

        writeToTextArea("Unknown error.");
        return License.UNKNOWN;
    }

    private void writeToTextArea(final String message) {
        Platform.runLater(() -> writeMessage(message));
    }

    private String time() {
        return new SimpleDateFormat("HH:mm:ss").format(Calendar.getInstance().getTime());
    }
}

package atmp.main.java.controller;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import atmp.main.java.Program;
import atmp.main.java.csv.CSVConnecter;
import atmp.main.java.model.LogPass;

import java.io.IOException;

public class LoginScreenController {
    @FXML
    private TextField loginTF;
    @FXML
    private PasswordField passwTF;
    @FXML
    private Button loginBtn;
    @FXML
    private Label statusLbl;
    private Stage primaryStage;
    private LogPass csvFileDecryptedLogPass;

    public void initialize() {
        statusLbl.setWrapText(true);
    }

    public void onLoginBtnClicked(ActionEvent actionEvent) {
        CSVConnecter csvConnecter = new CSVConnecter(Program.csvLogPassFilePath);
        csvFileDecryptedLogPass = csvConnecter.getDecryptedLogPass();

        //checking login
        String login = loginTF.getText();
        String passw = passwTF.getText();

        if (!checkLogin(login)) {
            setStatusLblText("Invalid login", Color.RED);
            return;
        }

        if (!checkPassword(login, passw)) {
            setStatusLblText("Invalid password", Color.RED);
            return;
        }

        csvFileDecryptedLogPass = null;

        try {
            runMainInterface();
        } catch (IOException e) {
            e.printStackTrace();
            setStatusLblText("Invalid password", Color.RED);
        }
    }

    private void runMainInterface() throws IOException {
        Stage newStage = new Stage();
        newStage.setTitle("SIMPLICIA - Version 5.3");
        newStage.setScene(new Scene(FXMLLoader.load(getClass().getResource("/GUI.fxml")), 666, 618));
        newStage.setResizable(false);
        newStage.show();

        newStage.setOnCloseRequest(t -> {
            Platform.exit();
            System.exit(0);
        });

        this.primaryStage.close();
    }

    private boolean checkLogin(String login) {
        return csvFileDecryptedLogPass.getLogin().equals(login);
    }

    private boolean checkPassword(String login, String passw) {
        return csvFileDecryptedLogPass.getLogin().equals(login) && csvFileDecryptedLogPass.getPassword().equals(passw);
    }

    private void setStatusLblText(String text, Color color) {
        Platform.runLater(() -> { // if it will invoke from other classes
            statusLbl.setText(text);

            if (color != null) {
                statusLbl.setTextFill(color);
            }
        });
    }

    public void setPrimaryStage(Stage primaryStage) {
        this.primaryStage = primaryStage;
    }
}

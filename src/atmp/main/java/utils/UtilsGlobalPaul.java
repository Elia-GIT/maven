/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package atmp.main.java.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Lbedoucha Conseil
 */
public class UtilsGlobalPaul {
    private static SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

    public static void main(String[] args){
        System.out.println(daysBetween("25-03-2019","02-04-2019"));
    }

    static int daysBetween(String dateStart0, String dateEnd0) {
        try {
            if(dateStart0.length()==0){
                return 0;
            }

            return (int) ((simpleDateFormat.parse(dateEnd0).getTime() - simpleDateFormat.parse(dateStart0).getTime()) / (1000 * 60 * 60 * 24));
        } catch (ParseException ex) {
            Logger.getLogger(UtilsAlertes.class.getName()).log(Level.SEVERE, null, ex);
        }

        return -1;
    }
}

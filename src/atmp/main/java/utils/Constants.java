package atmp.main.java.utils;

public class Constants {

    static final String BROWSER = "Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36";
    static final int TIMEOUT = 17_000;
    public static String PHANTOM_JS_PATH = "browser.exe";
    public static final String LOGIN_URL = "https://www.net-entreprises.fr/";
    public static final int DELAY = 1;
    static final String KEY = "netentreprisesbt";
    static final String INIT_VECTOR = "RandomInitVector";

    public static final String CONFIG_FILE = "config.txt";
    static final boolean NIC_FILTER_ADEQUAT=true;

    static final String URL_DEMO_PASSWD="http://licence-simplicia.pbconseil.ovh/demopasswd.html";

    public static final String LINKEDIN_DEMO_FILE = "demo.txt";
    public static final boolean LOGIN_SCREEN_OFF = true ;
    public static final boolean IS_PRAD = false ;
    public static  String CODE_RISQUE_BC = null;
    public static boolean IS_DEMO_LINKEDIN = false;
    public static boolean IS_COMMAND_LINE_VERSION=true;
    public static boolean IS_API_VERSION = false;
    //static final String API_KEY_USER = "";
    //static final String API_KEY = "APIKEYADMIN";
    //static final String API_SECRET = "APISECRET";
    //public static final String API_KEY_USER = System.getenv("");
    public static final long SLEEP_DELAY_NETE_EXPIRATION = 10 ;
    public static final String HARDCODED_API_KEY = "APIKEYADMIN";
    public static final String HARDCODED_API_SECRET = "" ;
    public static String API_KEY = System.getenv("SMPL_ADMIN_APIKEY");
    public static String API_SECRET = System.getenv("SMPL_ADMIN_APISECRET");
    public static String API_ADMIN_SUPER_KEY = System.getenv("SMPL_ADMIN_SUPER_APIKEY");
    public static String API_ADMIN_SUPER_SECRET = System.getenv("SMPL_ADMIN_SUPER_APISECRET");
    public static final boolean IS_HARDCODED_API_KEYS=false;
    static String API_HOST ="demoapi.simplicia.co";
    static String API_PORT ="8082";
    static String API_PREFIX ="https://";
}

package atmp.main.java.utils;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.io.*;
import java.net.*;
import java.nio.channels.Channels;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.ZoneId;
import java.util.*;

public class UtilsMeth {

    private static final Logger LOGGER = Logger.getLogger(UtilsMeth.class);

    public static String getRandomHexString(int numchars) {
        Random r = new Random();
        StringBuilder sb = new StringBuilder();

        while (sb.length() < numchars) {
            sb.append(Integer.toHexString(r.nextInt()));
        }

        return sb.toString().substring(0, numchars);
    }

    public static int getYear() {
        return new Date().toInstant().atZone(ZoneId.systemDefault()).toLocalDate().getYear();
    }

    public static int getMonth() {
        return new Date().toInstant().atZone(ZoneId.systemDefault()).toLocalDate().getMonthValue();
    }

    public static int getDay() {
        return new Date().toInstant().atZone(ZoneId.systemDefault()).toLocalDate().getDayOfMonth();
    }

    public static int getHour() {
        return GregorianCalendar.getInstance().get(Calendar.HOUR_OF_DAY);   // gets hour in 12h format
    }

    public static int getMinute() {
        return GregorianCalendar.getInstance().get(Calendar.MINUTE);   // gets hour in 12h format
    }

    public static int getSecond() {
        return GregorianCalendar.getInstance().get(Calendar.SECOND);   // gets hour in 12h format
    }

    public static boolean downloadFile(String downloadUrl, String cookie, String path) {
        //trusting all certs for downloads
        TrustManager[] trustAllCerts = new TrustManager[]{
                new X509TrustManager() {
                    public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                        return null;
                    }

                    public void checkClientTrusted(java.security.cert.X509Certificate[] certs, String authType) {
                        //No need to implement.
                    }

                    public void checkServerTrusted(java.security.cert.X509Certificate[] certs, String authType) {
                        //No need to implement.
                    }
                }
        };

        // Install the all-trusting trust manager
        try {
            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        } catch (Exception e) {
            System.out.println(e);
        }

        try {
            URLConnection conn = new URL(downloadUrl).openConnection();
            conn.setRequestProperty("Cookie", cookie);

            try (InputStream inputStream = conn.getInputStream()) {
                try (final FileOutputStream fos = new FileOutputStream(new File(path))) {
                    fos.getChannel().transferFrom(Channels.newChannel(inputStream), 0, Long.MAX_VALUE);
                    return true;
                }
            }
        } catch (Exception ex) {
            LOGGER.info("ERROR - downloadFile. downloadUrl: " + downloadUrl, ex);
            return false;
        }
    }

    public static String removeLastChar(String str, String character) {
        if (str.endsWith(character)) {
            str = StringUtils.substringBeforeLast(str, character);
        }

        return str;
    }

    public static String encrypt(String txt) {
        try {
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
            cipher.init(Cipher.ENCRYPT_MODE, new SecretKeySpec(Constants.KEY.getBytes("UTF-8"), "AES"), new IvParameterSpec(Constants.INIT_VECTOR.getBytes("UTF-8")));
            return Base64.encodeBase64String(cipher.doFinal(txt.getBytes()));
        } catch (Exception e) {
            LOGGER.error("ERROR - encrypt. TXT: " + txt, e);
        }

        return "";
    }

    public static String decrypt(String encrypted) {
        try {
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
            cipher.init(Cipher.DECRYPT_MODE, new SecretKeySpec(Constants.KEY.getBytes("UTF-8"), "AES"), new IvParameterSpec(Constants.INIT_VECTOR.getBytes("UTF-8")));
            return new String(cipher.doFinal(Base64.decodeBase64(encrypted)));
        } catch (Exception e) {
            LOGGER.error("ERROR - decrypt. TXT: " + encrypted, e);
        }

        return "";
    }

    public static void writeFile(String path, String data) {
        try {
            Files.write(Paths.get(path), data.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String readFile(String filePath) throws IOException {
        return FileUtils.readFileToString(new File(filePath), String.valueOf(StandardCharsets.UTF_8));
    }

    public static String getLicenseUrlFromFile(String filename) {
        String url = "";
        File file = new File(filename);

        try (FileReader reader = new FileReader(file)) {
            char[] chars = new char[(int) file.length()];
            reader.read(chars);
            String licenseUrl = StringUtils.substringAfter(new String(chars), "http").trim();

            if (licenseUrl.contains(" ")) {
                licenseUrl = StringUtils.substringBefore(licenseUrl, " ").trim();
            }

            return "http" + licenseUrl;
        } catch (Exception e) {
            LOGGER.error("ERROR - getLicenseUrlFromFile. name: " + filename, e);
        }

        return url;
    }


    public static List<Integer> getLicenseValues(String url) {
        List<Integer> values = new ArrayList<>();

        try {
            Document doc = Jsoup.connect(url).ignoreContentType(true)
                    .validateTLSCertificates(false)
                    .userAgent(Constants.BROWSER).ignoreHttpErrors(true).timeout(Constants.TIMEOUT).get();

            values.add(Integer.valueOf(doc.select("#X").text().trim()));
            values.add(Integer.valueOf(doc.select("#Y").text().trim()));
        } catch (Exception e) {
            LOGGER.error("ERROR - getLicenseValues. url : " + url, e);
        }
        return values;
    }

    static String randInt(int min, int max) {
        return Integer.toString(new Random().nextInt((max - min) + 1) + min);
    }

    public static HashMap initEtatSiret(int num) {
        HashMap<Integer, List<String>> h = new HashMap<>();

        for (int i = 0; i < num; i++) {
            ArrayList<String> list = new ArrayList<>();
            h.put(i, list);
        }

        return h;
    }

    static String getDateAsMillis(String inputDate) {
        if (inputDate.length() == 0) {
            return null;
        } else {
            return inputDate.substring(6, 10) + "-" + inputDate.substring(3, 5) + "-" + inputDate.substring(0, 2);
        }
    }

    public static String getFileAsString(File file) throws IOException {
        InputStream is = new FileInputStream(file);
        BufferedReader buf = new BufferedReader(new InputStreamReader(is));

        String line = buf.readLine();
        StringBuilder sb = new StringBuilder();

        while(line != null){
            sb.append(line).append("\n");
            line = buf.readLine();
        }

        String fileAsString = sb.toString();
        return fileAsString;
    }
    public static int getStatusCodeUrl(String urlParam){
        URL url = null;
        try {
            url = new URL(urlParam);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        int code=0;
        HttpURLConnection connection = null;
        try {
            connection = (HttpURLConnection)url.openConnection();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            connection.setRequestMethod("GET");
            connection.connect();
            code = connection.getResponseCode();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("code = "+code);
        return code;
    }
    public static boolean inspect(String csvPath) {
        String csvString= null;
        try {
            csvString = getFileAsString(new File(csvPath));
        } catch (IOException e) {
            e.printStackTrace();
        }
        boolean resultat=!csvString.contains("expiré");//csvString.contains("Bureau;CTN;NNS;Nom;Prenom;Type");
        return resultat ;
    }
}

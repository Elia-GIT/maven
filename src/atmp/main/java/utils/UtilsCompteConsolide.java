/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package atmp.main.java.utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

import org.apache.log4j.Logger;

/**
 * @author Lbedoucha Conseil
 */
public class UtilsCompteConsolide {
    private static String consolideFileName = "compteConsolide.txt";

    public static void main(String[] args) throws IOException {
        Logger LOGGER = Logger.getLogger(UtilsCompteConsolide.class);
        writeCompteConsolideReport("22-11-2019",
                "C:\\Users\\pc\\IdeaProjects\\simplicia-atmp-bpij" +
                        "\\sortie\\CompteEmployeurCourant\\22-11-2019-11H-41M-33s", LOGGER);
    }

    public static String[] parseConsolideFile(String consoFile) throws FileNotFoundException, IOException {
        String line;
        int cour = 0;
        String[] ret = new String[1];

        while ((line = new BufferedReader(new FileReader(consoFile)).readLine()) != null) {
            if (cour == 1) {
                ret[0] = line;
            }

            cour++;
        }

        return ret;
    }

    public static boolean compteConsolideOn() {
        File f = new File(System.getProperty("user.dir") + "\\params\\" + UtilsCompteConsolide.consolideFileName);
        return f.exists() && !f.isDirectory();
    }

    public static void writeCompteConsolideReport(String date, String folder, Logger LOGGER) throws IOException {
        String outputFilenamePath = folder + "\\" + "Compte-Consolide--" + date + ".csv";
        BufferedWriter out = new BufferedWriter(new FileWriter(folder + "\\" + "Compte-Consolide--" + date + ".csv"));
        File[] files = new File(folder).listFiles();

        int cour0 = 0;
        int nbSiretCheckes = 0;
        for (File siret : files) {
            LOGGER.info(files.length);

            if (siret.isDirectory()) {
                nbSiretCheckes++;
                //loop on SIRET
                File[] filesSiret = new File(siret.toString()).listFiles();
                for (File fileSiret : filesSiret) {
                    //loop on THE csv file
                    String csvFile = fileSiret.getName();
                    if (csvFile.endsWith(".csv")) {
                        LOGGER.info("compte-conso:" + siret.getName());
                        LOGGER.info("   compte-conso-csv:" + csvFile);
                        String siretFromFileName = csvFile.substring(4, 18);
                        System.out.println(siretFromFileName);
                        cour0++;
                        int cour = 0;
                        //writeSiret(out,siret);
                        Scanner scanner = new Scanner(fileSiret);
                        while ((scanner.hasNextLine())) {
                            String line = scanner.nextLine();
                            cour++;
                            //on saute les en têtes sauf pour le premier fichier
                            if (cour == 1 && cour0 > 1) {
                                continue;
                            }
                            //premier fichier et premiere ligne d'en-tête
                            if (cour == 1 && cour0 == 1) {
                                out.write("NOM SOCIETE;SIRET" + ";" + line);
                                out.newLine();
                                continue;
                            }
                            String[] tab = line.split(";");
                            if (tab.length == 0) {
                                continue;
                            }
                            if (tab.length > 6) {
                                if (tab[6].length() > 16) {
                                    //System.out.println("hello"+tab[6].length());
                                    tab[6] = tab[6].substring(0, 13) + tab[6].substring(14, 16);
                                }
                                //System.out.println("nns"+tab[6]);
                                line = "";
                                for (String courVite : tab) {
                                    line = line + ";" + courVite;
                                }
                                line = line.substring(1);
                            }

                            boolean critereAdequat = siretFromFileName.substring(9).startsWith(tab[1]);
                            if (Constants.NIC_FILTER_ADEQUAT && critereAdequat) {
                                out.write(siret.getName() + ";" + siretFromFileName + ";" + line);
                                out.newLine();
                            } else {
                                out.write(siret.getName() + ";" + siretFromFileName + ";" + line);
                                out.newLine();
                            }
                        }
                        /* fichier VIDE : indiquer qu'il a été traité */
                        if (cour == 1) {
                            LOGGER.info("--> fichier à une ligne " + csvFile);
                            out.write(siret.getName() + ";" + siretFromFileName);
                            out.newLine();
                        }
                    }
                }
            }
        }

        LOGGER.info("compte-conso-total:" + nbSiretCheckes);
        out.close();
        CSVToExcelConverter.convertCSVToXLSRaw(outputFilenamePath, outputFilenamePath.replace("csv", "xls"), ";", false);
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package atmp.main.java.utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 * @author Lbedoucha Conseil
 */
public class UtilsVueFiltree {
    private static String vueFiltreFileName = "params\\vueFiltree.txt";

    private static String[] parseParamsFile(String alertesFile) throws FileNotFoundException, IOException {
        BufferedReader br = new BufferedReader(new FileReader(alertesFile));
        String line;
        int cour = 0;
        int countArgs = 0;
        String[] ret = new String[4];
        while ((line = br.readLine()) != null) {
            if (cour % 2 == 1) {
                ret[countArgs] = line;
                countArgs++;
            }
            cour++;
        }
        return ret;
    }

    private static void writeVueFiltreeReport(String date, String folder) throws IOException {
        String outputFilenamePath = folder + "\\" + "Vue-Filtree--" + date + ".csv";
        BufferedWriter out = new BufferedWriter(new FileWriter(outputFilenamePath));
        File[] files = new File(folder).listFiles();
        int cour0 = 0;
        for (File companyName : files) {
            if (companyName.isDirectory()) {
                //loop on SIRET
                File[] filesSiret = new File(companyName.toString()).listFiles();
                for (File fileSiret : filesSiret) {
                    //loop on THE csv file
                    String csvFile = fileSiret.getName();
                    String siret = fileSiret.getName().substring(4, 18);
                    if (csvFile.endsWith(".csv")) {
                        cour0++;
                        BufferedReader br = new BufferedReader(new FileReader(fileSiret));
                        String line;
                        int cour = 0;
                        //writeSiret(out,siret);
                        while ((line = br.readLine()) != null) {
                            cour++;
                            //on saute les en têtes sauf pour le premier fichier
                            if (cour == 1 && cour0 > 1) {
                                continue;
                            }
                            //marquage en tête
                            String criteriasHeader = "AT+45J - 60 derniers J;" +
                                    "AT+90J - 60 derniers J;" +
                                    "IPP notifiée - 60 derniers J;" +
                                    "MP notifiée - 60 derniers J";
                            if (cour == 1 && cour0 == 1) {
                                out.write("Nom Societe" + ";SIRET;" + criteriasHeader + ";" + line);
                                out.newLine();
                                continue;
                            }
                            String[] tab = line.split(";");
                            if (tab.length == 0) {
                                continue;
                            }
                            if (tab.length > 6) {
                                if (tab[6].length() > 16) {
                                    //System.out.println("hello"+tab[6].length());
                                    tab[6] = tab[6].substring(0, 13) + tab[6].substring(14, 16);
                                }
                                //System.out.println("nns"+tab[6]);
                                line = "";
                                for (String courVite : tab) {
                                    line = line + ";" + courVite;
                                }
                                line = line.substring(1);
                            }
                            //filtrer ici
                            String dateNotif = tab[11].replace("/", "-");
                            //System.out.println(tab.length+" "+line);
                            int nbJoursString = 0;
                            if (tab[12].length() > 0) {
                                nbJoursString = Integer.parseInt(tab[12]);
                            }
                            //IP number 16
                            int IPPercentage = 0;
                            if (tab[16].length() > 0 & !tab[16].startsWith("DCD"))
                                IPPercentage = Integer.parseInt(tab[16]);
                            if (tab[16].startsWith("DCD"))
                                IPPercentage = 101;
                            //System.out.println(IPPercentage+";"+UtilsGlobalPaul.daysBetween(dateNotif,date));
                            //Si au moins un des 4 critères se vérifie...
                            boolean critere1 = checkCriteria1(line, dateNotif, nbJoursString, IPPercentage, date);
                            boolean critere2 = checkCriteria2(line, dateNotif, nbJoursString, IPPercentage, date);
                            boolean critere3 = checkCriteria3(line, dateNotif, IPPercentage, date);
                            boolean critere4 = checkCriteria4(line, dateNotif, date);
                            boolean critere5 = siret.substring(9).startsWith(tab[1]);

                            if ((critere1 | critere2 | critere3 | critere4) && critere5) {
                                String criteriasLine = transformToFrench(critere1) + ";" + transformToFrench(critere2) + ";" +
                                        transformToFrench(critere3) + ";" + transformToFrench(critere4);
                                out.write(companyName.getName() + ";" + siret + ";" + criteriasLine + ";" + line);
                                out.newLine();
                            }
                        }
                    }
                }
            }
        }

        out.close();
        CSVToExcelConverter.CSVtoXLS(outputFilenamePath, outputFilenamePath.replace("csv", "xls"), ";", true);
    }

    private static String transformToFrench(boolean b) {
        return b ? "oui" : "";
    }

    public static void main(String[] args) throws IOException {
        try {
            writeVueFiltreeReport("07-11-2018", "C:\\Users\\Lbedoucha Conseil\\Documents\\NetBeansProjects\\net-entreprises_bot\\dossier sortie\\CompteEmployeurCourant\\29-1-2019");
            //test IPP
            System.out.println(checkCriteria3("Societe 2;2018;00021;01;151EC;;D;2641259122216 23 180215 592;LEGRAND;NATHALIE;MP;15/02/2018;25/07/2018;53;4;4152;;;;;;;;4152"
                    , "15-03-1989", 0, "15-04-2019"));
            //test MP distance
            System.out.println(checkCriteria4("Societe 2;2018;00021;01;151EC;;D;2641259122216 23 180215 592;LEGRAND;NATHALIE;MP;15/02/2018;25/07/2018;53;4;4152;;;;;;;;4152"
                    , "30-07-2019", "07-11-2019"));
        } catch (IOException ex) {
            System.out.println(ex);
        }
    }

    private static int getParam1ATLength(String alertesFile) throws IOException {
        return Integer.parseInt(parseParamsFile(alertesFile)[0].split(";")[0]);
    }

    private static int getParam1ATNotifDateDistance(String alertesFile) throws IOException {
        return Integer.parseInt(parseParamsFile(alertesFile)[0].split(";")[1]);
    }

    private static int getParam2LongATDuration(String alertesFile) throws IOException {
        return Integer.parseInt(parseParamsFile(alertesFile)[1].split(";")[0]);
    }

    private static int getParam2LongATDistance(String alertesFile) throws IOException {
        return Integer.parseInt(parseParamsFile(alertesFile)[1].split(";")[1]);
    }

    private static int getParam3IPPercentage(String alertesFile) throws IOException {
        return Integer.parseInt(parseParamsFile(alertesFile)[2].split(";")[0]);
    }

    private static int getParam3IPDistanceInDays(String alertesFile) throws IOException {
        return Integer.parseInt(parseParamsFile(alertesFile)[2].split(";")[1]);
    }

    private static int getParam4MPNotifDateDistance(String alertesFile) throws IOException {
        return Integer.parseInt(parseParamsFile(alertesFile)[3]);
    }

    //AT +30J sur les 60 derniers jours (date notif<60J)
    private static boolean checkCriteria1(String line, String dateNotif, int nbJoursString, int IPPercentage, String date) throws IOException {
        return
                //-90J
                (nbJoursString < getParam2LongATDuration(vueFiltreFileName)) &&
                        //+45J
                        (nbJoursString >= getParam1ATLength(vueFiltreFileName)) &&
                        //sur les 60 derniers jours
                        UtilsGlobalPaul.daysBetween(dateNotif, date) <= getParam1ATNotifDateDistance(vueFiltreFileName);
    }

    //AT + 150J
    private static boolean checkCriteria2(String line, String dateNotif, int nbJoursString, int IPPercentage, String date) throws IOException {
        return nbJoursString >= getParam2LongATDuration(vueFiltreFileName) && UtilsGlobalPaul.daysBetween(dateNotif, date) <= getParam2LongATDistance(vueFiltreFileName);
    }

    //%IP > 10% depuis XX jours
    private static boolean checkCriteria3(String line, String dateNotif, int IPPercentage, String date) throws IOException {
        return IPPercentage >= getParam3IPPercentage(vueFiltreFileName) && UtilsGlobalPaul.daysBetween(dateNotif, date) <= getParam3IPDistanceInDays(vueFiltreFileName);
    }

    //MP date notif <=35J
    private static boolean checkCriteria4(String line, String dateNotif, String date) throws IOException {
        boolean isMP = line.split(";")[9].startsWith("MP");
        return UtilsGlobalPaul.daysBetween(dateNotif, date) <= getParam4MPNotifDateDistance(vueFiltreFileName) && isMP;
    }
}

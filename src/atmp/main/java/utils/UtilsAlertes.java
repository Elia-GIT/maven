/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package atmp.main.java.utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;

/**
 * @author Lbedoucha Conseil
 */
public class UtilsAlertes {
    private static String alerteFileName = "alertes.txt";

    private static void createFile(String name) throws FileNotFoundException, IOException {
        File yourFile = new File(name);
        yourFile.createNewFile(); // if file already exists will do nothing
        FileOutputStream oFile = new FileOutputStream(yourFile, false);
    }

    public static int getLastXXDays(String[] tab) {
        return Integer.parseInt(tab[2]);
    }

    public static int getAtMpGrave(String[] tab) {
        return Integer.parseInt(tab[1]);
    }

    public static String getEmail(String[] tab) {
        return tab[0];
    }

    /*
    public static void writeAlertesReport(String date, String folder) throws IOException{
            String dir=System.getProperty("user.dir")+"\\"+alerteFileName;
            String[] parsedAlertesFile=parseAlertesFile(dir);
            int lastXXDays=getLastXXDays(parsedAlertesFile);
            int atMpGrave=getAtMpGrave(parsedAlertesFile);
            String email=getEmail(parsedAlertesFile);
            FileWriter fw = new FileWriter("Rapport--"+date+".txt");
            BufferedWriter out = new BufferedWriter(fw);
            File[] files = new File(folder).listFiles();
            int cour0=0;
            for (File siret : files) {
                if (siret.isDirectory()) {
                    //loop on SIRET
                    File[] filesSiret = new File(siret.toString()).listFiles();
                    for (File fileSiret : filesSiret) {
                        //loop on THE csv file
                        String csvFile=fileSiret.getName();
                        if(csvFile.endsWith(".csv")){
                            BufferedReader br = new BufferedReader(new FileReader(fileSiret));
                                String line;
                                int cour=0;
                                writeSiret(out,siret);
                                while ((line = br.readLine()) != null) {
                                    cour++;
                                    if(cour==1)
                                        continue;
                                    String[] tab=line.split(";");
                                    if(tab.length==0)
                                        continue;
                                    writeLineATMP(line,out,tab,lastXXDays,atMpGrave,date);
                                }
                                out.newLine();
                                out.newLine();
                        }
                    }
                }
            }
            out.close();
    }*/
    public static String[] parseAlertesFile(String alertesFile) throws FileNotFoundException, IOException {
        BufferedReader br = new BufferedReader(new FileReader(alertesFile));
        String line;
        int cour = 0;
        String[] ret = new String[3];
        while ((line = br.readLine()) != null) {
            if (cour == 1) {
                ret[0] = line;
            }
            if (cour == 3) {
                ret[1] = line;
            }
            if (cour == 5) {
                ret[2] = line;
            }
            if (cour == 7) {
                ret[3] = line;
            }
            cour++;
        }
        return ret;

    }

    public static boolean alertOn() {
        File f = new File(System.getProperty("user.dir") + "\\" + alerteFileName);
        if (f.exists() && !f.isDirectory()) {
            return true;
        }
        return false;
    }

    public static void main(String[] args) throws IOException {
        //writeAlertesReport("10-10-2018","C:\\Users\\Lbedoucha Conseil\\Documents\\NetBeansProjects\\net-entreprises_bot\\dossier sortie\\CompteEmployeurCourant\\10-10-2018");
    }

    private static void writeLineATMP(String line, BufferedWriter out, String[] tab, int lastXXDays, int atMpGrave, String date) throws IOException {
        String dateNotif = tab[11].replace("/", "-");
        String nbJoursString = tab[12];
        String nns = tab[6];
        String nom = tab[7];
        String prenom = tab[8];
        String type = tab[9];
        switch (nbJoursString.length()) {
            case 1:
                nbJoursString = nbJoursString + "  ";
            case 2:
                nbJoursString = nbJoursString + " ";
        }
        int nbJours = Integer.parseInt(nbJoursString.replace(" ", ""));
        if (nbJours >= atMpGrave && UtilsGlobalPaul.daysBetween(dateNotif, date) <= lastXXDays) {
            out.write("Date Notification : " + dateNotif);
            out.write("   |   Type : " + type);
            out.write("   |   Nombre Jours : " + nbJoursString);
            out.write("   |   NNS : " + nns);
            out.write("   |   Nom - Prénom : " + nom + " - " + prenom);
            out.newLine();
        }
    }

    private static void writeSiret(BufferedWriter out, File siret) throws IOException {
        out.write("======================");
        out.newLine();
        out.write("SIRET : " + siret.getName());
        out.newLine();
        out.write("======================");
        out.newLine();
    }
}

package atmp.main.java.utils;

import org.jsoup.Jsoup;

import java.io.IOException;

public class UtilsRapportRun {
    public static String getTypeCompte(int i, boolean isPluriel) {
        try {
            if (Jsoup.connect(Constants.URL_DEMO_PASSWD).execute().statusCode() != 200) {
                return "";
            }
        } catch (IOException e) {
            return "";
        }

        String compte = "Compte";
        if (isPluriel) {
            compte = compte + "s";
        }

        switch (i) {
            case -1:
                return compte + " en cours d'implémentation -- sc2";
            case 0:
                return compte + " avec connexion échouée";
            case 1:
                return compte + " avec Compte AT/MP introuvable";
            case 2:
                return compte + " avec Module AT/MP ok mais CEC introuvable";
            case 3:
                return compte + " avec erreur Net-Entreprises --> nous ne pouvons répondre à votre demande";
            case 4:
                return compte + " avec aucuns fichiers à télécharger";
            case 5:
                return compte + " avec accès AT/MP et téléchargements réussis";
            case 6:
                return compte+" avec infos à valider par mail";
        }

        return "";
    }
}

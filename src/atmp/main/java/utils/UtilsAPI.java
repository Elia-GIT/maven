package atmp.main.java.utils;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import atmp.main.java.bot.NetEntreprisesBot;
import atmp.main.java.model.InputData;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.*;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

public class UtilsAPI {
    public static class MyJob implements Callable<String> {

        @Override
        public String call() {
            final HttpHeaders headers = new HttpHeaders();
            headers.set("X-ApiKey", Constants.API_KEY);
            headers.set("X-ApiSecret", Constants.API_SECRET);

            final HttpEntity<Void> entity = new HttpEntity<>(headers);
            final ResponseEntity<JsonObject> out = new RestTemplate().exchange(
                    Constants.API_PREFIX + Constants.API_HOST
                            + ":" + Constants.API_PORT
                            + "/api/atmp/admin/logins",
                    HttpMethod.GET, entity, JsonObject.class);
            return out.getBody().toString();
        }
    }

    public static List<InputData> getLoginsList(Logger LOGGER) {
        Future<String> control
                = Executors.newSingleThreadExecutor().submit(new MyJob());
        String result = "";

        try {
            result = control.get(5, TimeUnit.SECONDS);
        } catch (TimeoutException ex) {
            LOGGER.info("--> timer except");
            control.cancel(true);
        } catch (InterruptedException ex) {
            LOGGER.info("réponse API getLoginsList Interr--> " + result);
            return new ArrayList<>();
        } catch (ExecutionException ex) {
            LOGGER.info("réponse API getLoginsList Exec --> Erreur clé API " + result);
            return new ArrayList<>();
        }

        JSONObject json;
        JSONArray arr = null;

        try {
            json = new JSONObject(result);
            arr = json.getJSONArray("logins");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        LOGGER.info("réponse API getLoginsList --> " + "200 OK");

        List<InputData> ret = new ArrayList<>();
        ret.add(new InputData(""));
        for (int i = 0; i < arr.length(); i++) {
            try {
                String siret = arr.getJSONObject(i).getString("siret");
                String nom = arr.getJSONObject(i).getString("nom");
                String prenom = arr.getJSONObject(i).getString("prenom");
                String motDePasse = arr.getJSONObject(i).getString("motDePasse");
                ret.add(new InputData(siret, nom, prenom, motDePasse, "", "", null));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return ret;
    }

    public static boolean submitCSVtoDB(String csvFile, String siret, Logger LOGGER) {
        boolean preprod = false;
        final RestTemplate restTemplate = new RestTemplate();
        final JsonArray atmps = new JsonArray();
        final JsonObject in = new JsonObject();
        List<String> nnsTab=new ArrayList<>();

        try (BufferedReader br = new BufferedReader(new FileReader(csvFile))) {
            int cour = -1;
            for (String line; (line = br.readLine()) != null; ) {
                cour++;
                if (cour == 0) {
                    continue;
                }
                String[] tab = line.split(";");
                //LOGGER.info("LINE= "+tab[6]+"--"+tab[7]);
                //sauter ligne 1
                final JsonObject atmp = new JsonObject();
                atmp.addProperty("exercice", tab[0]);
                atmp.addProperty("nic", tab[1]);
                atmp.addProperty("section", tab[2]);
                atmp.addProperty("risque", tab[3]);
                atmp.addProperty("bureau", tab[4]);
                atmp.addProperty("ctn", tab[5]);
                atmp.addProperty("nns", preprod ? UtilsMeth.getRandomHexString(10) : tab[6]);
                /*en préprod, anonymiser ces 3 champs*/
                atmp.addProperty("nom", preprod ? UtilsMeth.getRandomHexString(10) : tab[7]);
                atmp.addProperty("prenom", preprod ? UtilsMeth.getRandomHexString(10) : tab[8]);
                /* //fin */
                atmp.addProperty("type", tab[9]);
                atmp.addProperty("dateSinistre", UtilsMeth.getDateAsMillis(tab[10]));

                atmp.addProperty("dateNotification", UtilsMeth.getDateAsMillis(tab[11]));
                atmp.addProperty("nbJoursArret", Long.parseLong(preprod ? UtilsMeth.randInt(10, 150) : tab[12].length() == 0 ? "-1" : tab[12]));
                atmp.addProperty("niveauCcmIt", tab[13]);
                //System.out.print("VALEUR tarifCcmIt : "+tab[14]+"--");
                atmp.addProperty("tarifCcmIt", preprod ? "tarifCcmIt" : tab[14]);
                atmp.addProperty("dateNotificationIpDeces", UtilsMeth.getDateAsMillis(tab[15]));
                long pourcentIp = 0;
                if (tab[16].length() > 0) {
                    if (tab[16].contains("DCD")) {
                        pourcentIp = 100;
                    } else {
                        pourcentIp = Long.parseLong(tab[16]);
                    }
                }
                atmp.addProperty("pourcentIp", pourcentIp);
                atmp.addProperty("niveauCcmIp", tab[17]);
                atmp.addProperty("tarifCcmIp", tab[18]);
                atmp.addProperty("pourcentRctTiers", tab[19].length() == 0 ? 0 : Long.parseLong(tab[19]));
                atmp.addProperty("pourcentEtt", tab[20].length() == 0 ? 0 : Long.parseLong(tab[20]));
                atmp.addProperty("pourcentEut", tab[21].length() == 0 ? 0 : Long.parseLong(tab[21]));
                //System.out.println("VALEUR RISQUE : "+tab[22]);
                atmp.addProperty("valeurRisque", preprod ? "valeurRisque" : tab[22]);
                atmp.addProperty("siret", preprod ? siret : siret);
                if(!nnsTab.contains(tab[6])) {
                    nnsTab.add(tab[6]);
                    atmps.add(atmp);
                    in.add("atmps", atmps);
                }
            }
            // line is not visible here.
        } catch (IOException e) {
            LOGGER.info("");
        }
        LOGGER.info("[" + siret + "] NB ATMP à pusher --> " + atmps.size());
        if (atmps.size() == 0) {
            LOGGER.info("[" + siret + "] 0 ATMP à pusher !! --> STOPPING");
            return true;
        }
        //LOGGER.info(atmps.getAsString());
        final RequestEntity<String> entity = RequestEntity.post(null)
                .header("X-ApiKey", Constants.API_KEY)
                .header("X-ApiSecret", Constants.API_SECRET)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .body(in.toString());

        final JsonObject out =
                restTemplate.postForObject(Constants.API_PREFIX +
                        Constants.API_HOST +
                        ":" + Constants.API_PORT +
                        "/api/atmp/admin/atmps", entity, JsonObject.class);
        LOGGER.info("[" + siret + "] Réponse API pushCSVToDB" + out);
        return true;
    }

    public static void setLoginStatus(String siret, String statut, Logger LOGGER) {
        final RestTemplate restTemplate = new RestTemplate();

        final JsonObject in = new JsonObject();
        in.addProperty("siret", siret);
        in.addProperty("statusAtmp", statut);

        final RequestEntity<String> entity = RequestEntity
                .post(null)
                .header("X-ApiKey", Constants.API_KEY)
                .header("X-ApiSecret", Constants.API_SECRET)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .body(in.toString());

        final JsonObject out =
                restTemplate.postForObject(
                        Constants.API_PREFIX +
                                Constants.API_HOST +
                                ":" + Constants.API_PORT +
                                "/api/atmp/admin/logins/status", entity, JsonObject.class);
        LOGGER.info("[" + siret + "] Réponse API setLoginStatus " + out);
        //System.out.println(out);
    }

    public static boolean submitPDFtoDB(String pdfPath, String siret, Logger LOGGER) {
        final RestTemplate restTemplate = new RestTemplate();

        final HttpHeaders headers = new HttpHeaders();
        headers.set("X-ApiKey", Constants.API_KEY);
        headers.set("X-ApiSecret", Constants.API_SECRET);
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);

        final MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
        body.add("content", new FileSystemResource(pdfPath));
        body.add("siret", siret);

        final HttpEntity<MultiValueMap<String, Object>> entity = new HttpEntity<>(body, headers);

        final JsonObject out =
                restTemplate.postForObject(
                        Constants.API_PREFIX +
                                Constants.API_HOST +
                                ":" + Constants.API_PORT +
                                "/api/atmp/admin/pdfs", entity, JsonObject.class);
        LOGGER.info("[" + siret + "] Réponse API submit pushPDFtoDB " + out);
        return true;
    }

    public static void submitExtractionLogToDB(String date, String log, int siretFailureCount, int siretSuccessCount, String typeExtraction, Logger LOGGER) {
        boolean preprod = true;
        final RestTemplate restTemplate = new RestTemplate();
        final JsonArray logsJson = new JsonArray();
        final JsonObject in = new JsonObject();

        //LOGGER.info("LINE= "+tab[6]+"--"+tab[7]);
        //sauter ligne 1
        final JsonObject logJson = new JsonObject();
        logJson.addProperty("log", log);
        logJson.addProperty("date", date);
        logJson.addProperty("siretFailureCount", siretFailureCount);
        logJson.addProperty("siretSuccessCount", siretSuccessCount);
        logJson.addProperty("typeExtraction", typeExtraction);//UtilsMeth.getDateAsSeconds(dateBpij));
        logsJson.add(logJson);
        in.add("activityLogs", logsJson);
        // line is not visible here.
        //LOGGER.info(atmps.getAsString());
        final RequestEntity<String> entity = RequestEntity.post(null)
                .header("X-ApiKey", Constants.API_KEY)
                .header("X-ApiSecret", Constants.API_SECRET)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .body(in.toString());

        final JsonObject out =
                restTemplate.postForObject(Constants.API_PREFIX +
                        Constants.API_HOST +
                        ":" + Constants.API_PORT +
                        "/api/atmp/admin/activityLogs", entity, JsonObject.class);
        LOGGER.info("[ successCount=" + siretSuccessCount + " | failureCount=" + siretFailureCount + " ]  Réponse API --> " + out);
    }

    public static void main(final String[] args) {

        String logins2 = "1;IGIER;NATHALIE;1\n";/*+
                "2;clodic;yves;2\n" +
                "3;LES ROSIERS;LES ROSIERS;3\n" +
                "4;RAKOTOASITERA;CAPRICE;4\n" +
                "5;CAPSECUR;CAPSECUR;5\n" +
                "6;PRADEL;MICHEL;6\n" +
                "7;BASZYNSKI;MONIQUE;7";*/
        Logger LOGGER = Logger.getLogger(NetEntreprisesBot.class);
        String[] lines = logins2.split("\n");
        for (String line0 : lines) {
            String[] line = line0.split(";");
            //submitSecuLoginToDB(line[0],line[1],line[2],line[3],LOGGER);
        }
        //submitCSVtoDB("testFiles\\CEC-30077440300020-13-9-2019.csv","1",LOGGER);
        //submitCSVtoDB("testFiles\\CEC-30397252500032-13-9-2019.csv","2",LOGGER);
        //submitPDFtoDB("testFiles\\pdf-sample.pdf","3",LOGGER);
        getLoginsList(LOGGER);
        //setLoginStatus("1","OK",LOGGER);
        //submitExtractionLogToDB("27/09/2019","ICI LOG",1,1,"ATMP",LOGGER);
        //printSwaggerATMP();
        UtilsMeth.getDateAsMillis("10/04/1989");
        System.out.println("HashCode Generated by SHA-512 for: ");

        String s1 = "1f36ec912c453ab46bcbfee10895557257031adbef50bb4489382971ebf4";
        //System.out.println("\n" + s1 + " : " + UtilsMeth.encryptThisString(s1));
    }
}

package atmp.main.java.csv;

import atmp.main.java.model.InputData;
import atmp.main.java.utils.Constants;
import atmp.main.java.utils.UtilsAPI;
import atmp.main.java.utils.UtilsRapportRun;
import org.apache.log4j.Logger;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class CsvReaderWriter {

    private static final Logger LOGGER = Logger.getLogger(CsvReaderWriter.class);


    public List<InputData> readInputDataList(String csvPath) {
        if (Constants.IS_COMMAND_LINE_VERSION && Constants.IS_API_VERSION) {
            return UtilsAPI.getLoginsList(LOGGER);
        } else {
            int viviModifier = Constants.IS_PRAD ? 2 : 0;
            List<InputData> inputDataList = new ArrayList<>();
            File file = new File(csvPath);

            try (FileReader reader = new FileReader(file)) {
                char[] chars = new char[(int) file.length()];
                reader.read(chars);
                String[] lines = new String(chars).split("\\r?\\n");
                LOGGER.info("lines length CSV : " + lines.length);

                for (int k = 0; k < lines.length; k++) {
                    String currLine = lines[k];
                    if (k == 0) {
                        inputDataList.add(new InputData(currLine));
                    } else {
                        String[] cells = currLine.split(";");
                        String siret = cells[viviModifier];
                        String nom = cells[1 + viviModifier];
                        String prenom = cells[2 + viviModifier];
                        String password = cells[3 + viviModifier];
                        String companyName = siret;
                        String codeRisque = null;

                        if (cells.length > 4 + viviModifier) {
                            companyName = cells[4 + viviModifier];
                        }
                        if (cells.length > 5 + viviModifier) {
                            codeRisque = cells[5 + viviModifier];
                        }

                        inputDataList.add(new InputData(siret, nom, prenom, password, currLine, companyName, codeRisque));
                    }

                }
            } catch (Exception e) {
                LOGGER.error("ERROR - readInputDataList. path: " + csvPath, e);
            }

            return inputDataList;
        }
    }

    public void writeCSV(InputData inputData, String filename) {
        try (PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(filename, true)))) {
            String line = Constants.IS_PRAD ? ";;" : ""
                    + inputData.getCompanyCustomName()
                    + ";" + inputData.getCompanyID()
                    + ";" + inputData.getLastname()
                    + ";" + inputData.getFirstname()
                    + ";" + inputData.getPassword()
                    + ";" + UtilsRapportRun.getTypeCompte(inputData.getStatusLogin(), false);
            out.println(line);
        } catch (IOException e) {
            LOGGER.error("ERROR - writeCSV. filename: " + filename, e);
        }
    }

    public void writeHeaderLine(String line, String filename) {
        try (PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(filename, false)))) {
            out.println("Nom Société;" + line + ";" + "Status Connexion");
        } catch (IOException e) {
            LOGGER.error("ERROR - writeCSV. filename: " + filename, e);
        }
    }
}

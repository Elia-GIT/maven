package atmp.main.java.csv;

import atmp.main.java.model.LogPass;
import atmp.main.java.model.Table;
import atmp.main.java.model.TableLine;
import org.apache.log4j.Logger;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Class for reading and writing into .csv file (for table with encryption)
 */

public class CSVConnecter {
    //statuses for table
    public static final String STATUS_NON = "non"; // decrypted
    public static final String STATUS_OUI = "oui"; // encrypted

    private static final String DEFAULT_SPLIT_BY = ";"; // default splitting

    private static final Logger LOGGER = Logger.getLogger(CSVConnecter.class);

    private String splitBy;
    private File file;

    public CSVConnecter(String filePath) {
        this.splitBy = DEFAULT_SPLIT_BY;
        this.file = new File(filePath);
    }

    //BIG TABLE FUNCS

    //checking is passw encrypted
    private boolean isEncrypted() {
        String line;

        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            String[] lineVals;
            String status;

            if ((line = br.readLine()) != null) {
                lineVals = line.split(splitBy);
                status = lineVals[lineVals.length - 1];

                if (status.equals(STATUS_OUI)) { // encrypted
                    return true;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return false;
    }

    //get table from csv file (BIG TABLE)
    private Table getData() {
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            String line;
            ArrayList<String> headers;
            ArrayList<TableLine> tableLines = new ArrayList<>();

            if ((line = br.readLine()) != null) {
                headers = new ArrayList<>(Arrays.asList(line.split(splitBy)));

                while ((line = br.readLine()) != null) {
                    tableLines.add(new TableLine(line));
                }

                return new Table(headers, tableLines);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    //encrypting table
    public void enctypt() {
        if (!isEncrypted()) {
            Table table = getData();

            if (table != null) {
                table.encrypt();
            }

            try (PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(file, false)))) {
                if (table != null) {
                    out.println(table.toCSV());
                }
            } catch (IOException e) {
                LOGGER.error("ERROR - writeCSV. filename: " + file.getName(), e);
            }
        }
    }

    //decrypting table
    public void decrypt() {
        if (isEncrypted()) {
            Table table = getData();

            if (table != null) {
                table.decrypt();
            }

            try (PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(file, false)))) {
                if (table != null) {
                    out.println(table.toCSV());
                }
            } catch (IOException e) {
                LOGGER.error("ERROR - writeCSV. filename: " + file.getName(), e);
            }
        }
    }

    private void encryptLogPass() {
        if (!isEncrypted()) {
            LogPass logPass = getLogPass();

            if (logPass != null) {
                logPass.encrypt();
            }

            try (PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(file, false)))) {
                if (logPass != null) {
                    out.println(logPass.toCSV());
                }
            } catch (IOException e) {
                LOGGER.error("ERROR - writeCSV. filename: " + file.getName(), e);
            }
        }
    }

    private void decryptLogPass() {
        if (isEncrypted()) {
            LogPass logPass = getLogPass();

            if (logPass != null) {
                logPass.decrypt();
            }

            try (PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(file, false)))) {
                if (logPass != null) {
                    out.println(logPass.toCSV());
                }
            } catch (IOException e) {
                LOGGER.error("ERROR - writeCSV. filename: " + file.getName(), e);
            }
        }
    }

    private LogPass getLogPass() {
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            String line;

            if ((line = br.readLine()) != null) {
                String[] loginPassword = line.split(splitBy);
                return new LogPass(loginPassword[0], loginPassword[1], loginPassword[2]);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public LogPass getDecryptedLogPass() {
        if (isEncrypted()) {
            decryptLogPass();
        }

        LogPass logPass = getLogPass();
        encryptLogPass();

        return logPass;
    }


    public static void main(String[] args) {
        CSVConnecter connecter = new CSVConnecter("paramas\\access.csv");
        connecter.enctypt();
        System.out.println();
        connecter.decrypt();
    }

    // func for making string with splitted values
    public static String makeSplittedStringFromValues(String split, String... values) {
        StringBuilder str = new StringBuilder();

        for (String val : values) {
            str.append(val).append(split);
        }

        str.delete(str.length() - 1, str.length());
        return str.toString();
    }

    // func for making string with splitted values
    public static String makeSplittedStringFromValues(String split, List<String> arr) {
        StringBuilder str = new StringBuilder();

        for (String val : arr) {
            str.append(val).append(split);
        }

        str.delete(str.length() - 1, str.length());
        return str.toString();
    }
}
